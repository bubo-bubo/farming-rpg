event_inherited();

can_move = false;

spr_base = spr_bases_female_2;
spr_hair = spr_hair_female_ponytail_raven;
spr_legs = spr_legs_female_pants_magenta;
spr_torso = spr_torso_female_sleeveless_white;
spr_feet = spr_feet_female_boots_black;

portrait_index	= 6;
text = [
		"My name is Susan.",
		[
			"Hello Susan! Hello Susan! Hello Susan! Hello Susan! Hello Susan! Hello Susan! Hello Susan! Hello Susan! Hello Susan!",
			"Nice to meet you!"
		],
		"Hello hello hello!",
		"Nice to meet you too."
		];
name = "Susan";
voice = snd_voice1;
speakers = [id,obj_player,id, id];
next_line = [0, [2,3], -1, -1];
scripts = [
	[change_variable, id, "spr_torso",spr_torso_female_sleeveless_red],
	[
		[change_variable, id, "spr_torso",spr_torso_female_sleeveless_white],
		-1,
	],
	-1,
	-1,
];