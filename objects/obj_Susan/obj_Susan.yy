{
    "id": "3084428c-fa9a-45e4-90e8-02b13856699b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Susan",
    "eventList": [
        {
            "id": "2a1879dd-9321-4fdc-be81-b3dde001593c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3084428c-fa9a-45e4-90e8-02b13856699b"
        }
    ],
    "maskSpriteId": "a8ef778c-e1a3-4369-83cf-e6046e3ab6a0",
    "overriddenProperties": null,
    "parentObjectId": "02006da4-e10f-49c6-b0a0-4c447addc668",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a8ef778c-e1a3-4369-83cf-e6046e3ab6a0",
    "visible": false
}