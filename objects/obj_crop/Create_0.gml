/// @description Insert description here
event_inherited();
collideable = false;

frameWidth = 32;
frameHeight = 64;

cropType = 0;
name = "";
daysOld = 0;
growthStage = 0;  //0~4
growthStageDuration = 0;
maxGrowthStage = (sprite_get_width(spr_crops)/frameWidth-1);

fullyGrown = false;
sparkle = -1;

xx = x-frameWidth/2+2;
yy = y-frameHeight+6;

sx = x + random_range(-5,10);
sy = y + random_range(-15,-10);