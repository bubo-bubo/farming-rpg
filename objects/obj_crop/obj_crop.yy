{
    "id": "50bf6d23-6211-4283-9cbb-460ff1268d73",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crop",
    "eventList": [
        {
            "id": "b73c5c2f-8709-445b-9342-622bcacff3b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "50bf6d23-6211-4283-9cbb-460ff1268d73"
        },
        {
            "id": "8d152332-be37-4d12-bb6e-3856eec97fea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "50bf6d23-6211-4283-9cbb-460ff1268d73"
        },
        {
            "id": "a243e1ad-c2aa-43cb-93c6-e858cae351a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "50bf6d23-6211-4283-9cbb-460ff1268d73"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bbd278df-15c2-4c88-a29d-335b05b354f6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "688b272e-15d6-47ed-9e50-94f84441dae0",
    "visible": false
}