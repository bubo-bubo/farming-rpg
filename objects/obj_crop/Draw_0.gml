
//draw this crop
draw_sprite_part(
	spr_crops,0,growthStage*frameWidth,
	cropType*frameHeight,frameWidth,frameHeight,xx,yy
);

//draw sparkle
if(sparkle >= 0){
	draw_sprite(spr_crops_sparkle,sparkle,sx,sy);
	sparkle += 0.1;
	if (sparkle >= sprite_get_number(spr_crops_sparkle)){
		sparkle = -1;
		alarm[1] = random_range(4,5)*room_speed;
	}
}	