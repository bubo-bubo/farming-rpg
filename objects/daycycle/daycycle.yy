{
    "id": "81bf0dd4-f7a8-4851-a5ef-13067c8f15c2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "daycycle",
    "eventList": [
        {
            "id": "8d9877c5-9885-41a1-8801-a320ec6163ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "81bf0dd4-f7a8-4851-a5ef-13067c8f15c2"
        },
        {
            "id": "e7d5732d-a1a0-46cb-bb7f-2f0db595589e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "81bf0dd4-f7a8-4851-a5ef-13067c8f15c2"
        },
        {
            "id": "f2fb57cb-7cd2-4bff-a713-5ee127dbf79d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "81bf0dd4-f7a8-4851-a5ef-13067c8f15c2"
        },
        {
            "id": "f463247a-cd46-4dff-a57e-b35fbfcbadeb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "81bf0dd4-f7a8-4851-a5ef-13067c8f15c2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}