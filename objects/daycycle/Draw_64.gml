/// @description Insert description here

if (draw_daylight) {
	var c = light_colour;
	draw_set_alpha(darkness);
	draw_rectangle_color(0,0,guiWidth,guiHeight,c,c,c,c,false);
	draw_set_alpha(1);
}

draw_text(10,10,string(seconds));
draw_text(10,20,string(minutes));
draw_text(10,30,string(hours));
draw_text(10,40,string(day));
draw_text(10,50,string(season));
draw_text(10,60,string(year));
