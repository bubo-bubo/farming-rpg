/// @description Insert description here

//---------切换场景
if(doTransition){
	//handle black fade/room transition
	if(room != spawnRoom){
		blackAlpha += 0.1;
		if(blackAlpha >= 1){room_goto(spawnRoom)}//淡出
	} else {
		blackAlpha -= 0.1;
		if(blackAlpha <= 0){doTransition = false;}//淡入
	}
	//draw black fade
	draw_set_alpha(blackAlpha);
	draw_rectangle_colour(0,0,global.game_width,global.game_height,c_black,c_black,c_black,c_black,false);
	draw_set_alpha(1);
}