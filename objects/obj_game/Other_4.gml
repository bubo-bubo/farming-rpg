/// @description Insert description here
roomWidth = room_width;
roomHeight = room_height;

if(spawnRoom == -1) exit;   //游戏开始时不需要过场
obj_player.x = spawnX;
obj_player.y = spawnY;
obj_player.facing = spawnPlayerFacing

//转场时调整人物方向
with(obj_player){
	switch(facing){
		case dir.right:	y_frame = 11;break;
		case dir.left:	y_frame = 9;break;
		case dir.up:	y_frame = 8;break;
		case dir.down:	y_frame = 10;break;
		case -1:	x_frame = 0;
	}
}