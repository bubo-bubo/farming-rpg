//-----debug mode
if(!debug) {exit;}	

//------draw bbox
with(obj_collision){
	draw_rectangle(bbox_left,bbox_bottom,bbox_right,bbox_top,true);
}
//------draw grids
var cs = crops.cellSize;
draw_set_alpha(0.3);

var xx = 0;
var r = roomWidth div cs;
repeat(r){
	draw_line_color(xx,0,xx,roomHeight,c_silver,c_silver);
	xx += cs;
}

var yy = 0;
var r = roomHeight div cs;
repeat(r){
	draw_line_color(0,yy,roomWidth,yy,c_silver,c_silver);
	yy += cs;
}
draw_set_alpha(1);