{
    "id": "54332cde-ce9a-4892-9f0d-71baa42cabc5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Jack",
    "eventList": [
        {
            "id": "2b887cd9-9f2e-466e-a327-5c5d498a2482",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "54332cde-ce9a-4892-9f0d-71baa42cabc5"
        }
    ],
    "maskSpriteId": "a8ef778c-e1a3-4369-83cf-e6046e3ab6a0",
    "overriddenProperties": null,
    "parentObjectId": "02006da4-e10f-49c6-b0a0-4c447addc668",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a8ef778c-e1a3-4369-83cf-e6046e3ab6a0",
    "visible": false
}