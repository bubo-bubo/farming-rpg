event_inherited();

spr_base = spr_bases_male_1;
spr_torso = spr_torso_male_shirt_white;
spr_legs = spr_legs_male_pants_green;
spr_hair = spr_hair_male_bedhead_pink;
spr_feet = spr_feet_male_shoes_black;

portrait_index	= 5;
text = ["Hello, I'm Jack.","I'm a lumberjack and I'm OK! I sleep all night and I work all day! I'm a lumberjack and I'm OK! I sleep all night and I work all day!"];
name = "Jack";
voice = snd_voice3;
speakers = [id,id];