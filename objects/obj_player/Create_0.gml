/// @description Insert description here
event_inherited();

can_move = true;

n_spd = 2;
r_spd = 3;

x_frame = 1;
y_frame = 8;

x_offset = sprite_get_xoffset(mask_index);
y_offset = sprite_get_yoffset(mask_index);

spr_base = spr_bases_female_1;
spr_torso = spr_torso_female_sleeveless_red;
spr_legs = spr_legs_female_pants_magenta;
spr_hair = spr_hair_female_pixie_blonde;
spr_feet = spr_feet_female_boots_black;
spr_shadow = spr_character_shadow;

facing = 0;

portrait_index	= 4;
name = "Player";
voice = snd_voice1;

radius = 16;
active_textbox = noone;