{
    "id": "45996d50-3649-467a-a521-b3ecba8ba669",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "738e692d-2f94-42e1-adf3-03ae33448462",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "45996d50-3649-467a-a521-b3ecba8ba669"
        },
        {
            "id": "6315fcf2-6ffe-4db6-a63a-433bb41f654a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "45996d50-3649-467a-a521-b3ecba8ba669"
        },
        {
            "id": "9ff62175-11cb-46c4-ade6-3ed3f5550b09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "45996d50-3649-467a-a521-b3ecba8ba669"
        }
    ],
    "maskSpriteId": "a8ef778c-e1a3-4369-83cf-e6046e3ab6a0",
    "overriddenProperties": null,
    "parentObjectId": "bbd278df-15c2-4c88-a29d-335b05b354f6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a8ef778c-e1a3-4369-83cf-e6046e3ab6a0",
    "visible": false
}