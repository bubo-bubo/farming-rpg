#region Press buttons to get move_x and move_y
if(can_move) {
	//------receive input
	input_left = keyboard_check(vk_left);
	input_right = keyboard_check(vk_right);
	input_up = keyboard_check(vk_up);
	input_down = keyboard_check(vk_down);
	input_interact = keyboard_check_released(ord("E"));

	moveX = 0;
	moveY = 0;

	//-----check speed
	if(keyboard_check(vk_shift)){spd = r_spd;}
	else{spd = n_spd;}
	//-----get intended movement
	moveX = (input_right - input_left) * spd;
	moveY = (input_down - input_up) * spd;
}
#endregion

//get direction player is facing
if(moveX != 0){
	switch(sign(moveX)){
		case 1: facing = dir.right; break;
		case -1: facing = dir.left; break;
	}
}
else if(moveY != 0){
	switch(sign(moveY)){
		case 1: facing = dir.down; break;
		case -1: facing = dir.up; break;
	}
} else {
	facing = -1;
}

//-----collision check
if(moveX !=0){
	var collisionH = instance_place(x+moveX,y,obj_collision);
	if(collisionH != noone && collisionH.collideable = true){
		repeat(abs(moveX)){
			if(!place_meeting(x+sign(moveX),y,obj_collision)){x += sign(moveX);}
			else{break;}}
		moveX = 0;}
}
if(moveY !=0){
	var collisionV = instance_place(x,y+moveY,obj_collision);
	if(collisionV != noone && collisionV.collideable = true){
		repeat(abs(moveX)){
			if(!place_meeting(x,y+sign(moveY),obj_collision)){y += sign(moveY);}
			else{break;}}
		moveY = 0;}
}



//-----------进门
var inst = instance_place(x,y,obj_transition);
if(inst != noone and facing == inst.playerFacingBefore){ //只有碰到且面向门时才触发
	with(obj_game){
		if(!doTransition){  //向obj_game传递来自门的参数
			spawnRoom = inst.targetRoom;
			spawnX = inst.targetX;
			spawnY = inst.targetY;
			spawnPlayerFacing = inst.playerFacingAfter;
			doTransition = true;
		}
	}
}

//Textbox
if(input_interact){
	if(active_textbox == noone){
		var inst = collision_rectangle(x-radius,y-radius,x+radius,y+radius, par_NPC,false, false);
		if(inst != noone){
			with(inst){
				other.active_textbox = creat_textbox(text, speakers, next_line, scripts);
				can_move = false;
				other.can_move = false;
				moveX = 0; moveY = 0;
			}
		}
	}else{  //reset textbox
		if(!instance_exists(active_textbox)){
			active_textbox = noone;
		}
	}
}

//------move player
x += moveX;
y += moveY;