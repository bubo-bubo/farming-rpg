{
    "id": "4437c1d5-e2ab-49d7-bcd8-9262f0c7ab37",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_textbox",
    "eventList": [
        {
            "id": "c6cbac6e-1e42-4c6c-b8bc-2811e67e0fa0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "4437c1d5-e2ab-49d7-bcd8-9262f0c7ab37"
        },
        {
            "id": "2c8e1d5b-15ee-4139-9604-a37fef9cb238",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4437c1d5-e2ab-49d7-bcd8-9262f0c7ab37"
        },
        {
            "id": "7cd0a308-7ff3-4689-ae5c-92058529a535",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4437c1d5-e2ab-49d7-bcd8-9262f0c7ab37"
        },
        {
            "id": "fad8456b-e352-4d9b-a6fd-750cd8019827",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "4437c1d5-e2ab-49d7-bcd8-9262f0c7ab37"
        },
        {
            "id": "4e684ffe-bb71-4b17-b91c-91f2fe7b56d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "4437c1d5-e2ab-49d7-bcd8-9262f0c7ab37"
        },
        {
            "id": "cba6337a-3793-46b5-97e6-8fbc8534a9fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "4437c1d5-e2ab-49d7-bcd8-9262f0c7ab37"
        },
        {
            "id": "68406e4a-284e-4af6-a65d-76574560bab1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "4437c1d5-e2ab-49d7-bcd8-9262f0c7ab37"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}