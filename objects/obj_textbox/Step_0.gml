if(keyboard_check_released(interact_key)){
	//if this page is not end, skip to end
	if(!choice_dialogue and counter < str_len){ counter = str_len;} 
	//if end, jump to next page
	else if(page < array_length_1d(text) - 1){
		event_perform(ev_other, ev_user2);   //use scripts
		
		var line = next_line[page];
		if(choice_dialogue){ //line = [option1, option2, option3...]
			line = line[choice];
			page = line;
		}
		if(line == 0) page ++;
		else if(line == -1){instance_destroy(); exit; }
		event_perform(ev_other, ev_user1);
	//end of dialog
	}else{ //page = array_length
		instance_destroy();
	}
}

if(choice_dialogue){
	obj_player.can_move = false;
	choice += keyboard_check_pressed(vk_down) - keyboard_check_pressed(vk_up);
	if(choice > text_array_len - 1){choice = 0;}
	else if(choice < 0) {choice = text_array_len - 1}
}