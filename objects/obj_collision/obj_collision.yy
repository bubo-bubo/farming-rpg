{
    "id": "7b39afe0-ae4a-4a6b-b970-5521fc304e36",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_collision",
    "eventList": [
        {
            "id": "6bb0b2be-17b4-4972-b881-fd3d4a7653d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7b39afe0-ae4a-4a6b-b970-5521fc304e36"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fb975b94-64b3-468e-b865-ade8e5cebb26",
    "visible": true
}