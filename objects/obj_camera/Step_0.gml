moveCam = keyboard_check(ord("C"));

//按C时自由移动摄像头
if(moveCam){
	x += (keyboard_check(ord("D"))-keyboard_check(ord("A")))*6;
	y += (keyboard_check(ord("S"))-keyboard_check(ord("W")))*6;
}

//走出小边界时跟随玩家
else{
	x = clamp(x,following.x-h_border,following.x+h_border);
	y = clamp(y,following.y-v_border,following.y+v_border);
}