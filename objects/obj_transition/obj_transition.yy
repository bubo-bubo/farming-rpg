{
    "id": "3baca021-7c6c-46ee-954b-a7b976e2621e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_transition",
    "eventList": [
        {
            "id": "e564d8da-5183-47e8-a4ed-89873372c00d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3baca021-7c6c-46ee-954b-a7b976e2621e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fb975b94-64b3-468e-b865-ade8e5cebb26",
    "visible": true
}