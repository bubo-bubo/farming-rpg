{
    "id": "02006da4-e10f-49c6-b0a0-4c447addc668",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "par_NPC",
    "eventList": [
        {
            "id": "e038e5fc-7bff-41a4-b788-39ba924bfc57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "02006da4-e10f-49c6-b0a0-4c447addc668"
        },
        {
            "id": "134ceac9-7d55-4163-adc5-ae48e2558b34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "02006da4-e10f-49c6-b0a0-4c447addc668"
        },
        {
            "id": "ed134a8b-c625-41f9-b522-9b1e8d9a476f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "02006da4-e10f-49c6-b0a0-4c447addc668"
        },
        {
            "id": "b659a036-c3ef-4dd2-91b5-75596dbed7f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "02006da4-e10f-49c6-b0a0-4c447addc668"
        }
    ],
    "maskSpriteId": "a8ef778c-e1a3-4369-83cf-e6046e3ab6a0",
    "overriddenProperties": null,
    "parentObjectId": "bbd278df-15c2-4c88-a29d-335b05b354f6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a8ef778c-e1a3-4369-83cf-e6046e3ab6a0",
    "visible": false
}