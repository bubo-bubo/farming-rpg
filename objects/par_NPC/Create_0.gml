/// @description Insert description here
event_inherited();

w_spd = 1;
n_spd = 2;
r_spd = 3;

spd = w_spd;

x_frame = 1;
y_frame = 8;

x_offset = sprite_get_xoffset(mask_index);
y_offset = sprite_get_yoffset(mask_index);

spr_base = -1;
spr_torso = -1;
spr_legs = -1;
spr_hair = -1;
spr_feet = -1;
spr_shadow = spr_character_shadow;

moveX = 0;
moveY = 0;
can_move = true;
can_move_default = can_move;

alarm[1] = 1;

portrait_index	= 0;
text[0] = "This person has nothing to say";
name = "Anomynous";
voice = snd_voice1;
speakers = [id];
next_line = [0]; //0 = next line; -1 = end dialogue; >= 1 choices
scripts = -1;