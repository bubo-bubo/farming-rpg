
if(!can_move) exit;
//======\/\/= MOVE NPC =\/\/============
//-----collision check
if(moveX !=0){
	var collisionH = instance_place(x+moveX,y,obj_collision);
	if(collisionH != noone && collisionH.collideable = true){
		repeat(abs(moveX)){
			if(!place_meeting(x+sign(moveX),y,obj_collision)){x += sign(moveX);}
			else{break;}}
		moveX = 0;}
}
if(moveY !=0){
	var collisionV = instance_place(x,y+moveY,obj_collision);
	if(collisionV != noone && collisionV.collideable = true){
		repeat(abs(moveX)){
			if(!place_meeting(x,y+sign(moveY),obj_collision)){y += sign(moveY);}
			else{break;}}
		moveY = 0;}
}

//------move object
x += moveX;
y += moveY;