{
    "id": "d3826ad1-ddb9-40cc-ac58-5ee3398f5bec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_item",
    "eventList": [
        {
            "id": "90cf51f0-0af1-4f4c-935e-e9f4da889dd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d3826ad1-ddb9-40cc-ac58-5ee3398f5bec"
        },
        {
            "id": "c8f2898b-c1d1-4202-a6b6-7bf1c2f1ac4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d3826ad1-ddb9-40cc-ac58-5ee3398f5bec"
        },
        {
            "id": "f1eb4e18-d1cd-45f0-b90f-2f7f5dd4cda0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d3826ad1-ddb9-40cc-ac58-5ee3398f5bec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bbd278df-15c2-4c88-a29d-335b05b354f6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}