//draw notifications
var not_grid = ds_notifications;
var grid_height = ds_grid_height(not_grid);
var c = c_white;
var plx = obj_player.x - 16;
var ply = obj_player.y - 64;

var yy = 0; repeat(grid_height){
	var notesign = "";
	if(not_grid[# 0, yy] > 0) notesign = "+";
	draw_set_font(fnt_text_9);
	draw_text_color(
		plx, ply - yy * str_height, notesign + string(not_grid[# 0, yy]) +" "+ string(not_grid[# 1, yy]),c,c,c,c,not_alpha
	);
	yy++;
} 

//fade away
if(fade_away) not_alpha -= 0.1;
if(not_alpha <= 0) instance_destroy();