{
    "id": "9821c735-efc5-4e2e-a4d2-981b1629a706",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_notification",
    "eventList": [
        {
            "id": "fb605d28-0390-43a5-8da9-55a8682a0aa0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9821c735-efc5-4e2e-a4d2-981b1629a706"
        },
        {
            "id": "79344cf8-7392-4512-877e-df0b6d12ccea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9821c735-efc5-4e2e-a4d2-981b1629a706"
        },
        {
            "id": "8dad90d7-0f47-4f5a-ab55-0a79e3f6f918",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "9821c735-efc5-4e2e-a4d2-981b1629a706"
        },
        {
            "id": "1ea08540-48d4-4d03-a438-9f67867d583b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "9821c735-efc5-4e2e-a4d2-981b1629a706"
        },
        {
            "id": "0e915c43-4147-4e73-98df-980e94e847e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9821c735-efc5-4e2e-a4d2-981b1629a706"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}