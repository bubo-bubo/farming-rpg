if(keyboard_check_released(ord("I"))){show_inventory = !show_inventory;}

if(!show_inventory) exit;

//==========\/\/\/=IF SHOW INVENTORY=\/\/\/==========

#region Mouse on which Slot
mousex = device_mouse_x_to_gui(0);  //相对于屏幕的xy
mousey = device_mouse_y_to_gui(0);


var cell_xbuff = (cell_size+x_buffer)*scale;
var cell_ybuff = (cell_size+x_buffer)*scale;

var i_mousex = mousex - slots_x;
var i_mousey = mousey - slots_y;

var nx = i_mousex div cell_xbuff; //横向第几格 0~7
var ny = i_mousey div cell_ybuff; //0~2
var sx = i_mousex mod cell_xbuff;   // sx > cell_size*scale说明落在间隙里
var sy = i_mousey mod cell_ybuff;

if(i_mousex > 0 and nx < inv_slots_width and i_mousey > 0 and ny < inv_slots_height
	and sx <= cell_size*scale and sy <= cell_size*scale){
		
	m_slotx = nx;
	m_sloty = ny;
	selected_slot = m_sloty*inv_slots_width + m_slotx; //0 ~ inv_slots;
	
}
else selected_slot = -1;
#endregion

#region Pickup item 2

var inv_grid = ds_inventory;
var ss_item = inv_grid[# 0, selected_slot];

if(pickup_slot != -1){
	var pp_item = inv_grid[# 0, pickup_slot];
	var pp_num = inv_grid[# 1, pickup_slot];

	if(mouse_check_button_pressed(mb_left)){
		//点击相同物品(自己除外)，叠加
		if(ss_item == pp_item){
			if(selected_slot != pickup_slot){
				inv_grid[# 1, selected_slot] += pp_num;
			
				inv_grid[# 0, pickup_slot] = item.none;
				inv_grid[# 1, pickup_slot] = 0;
			}
			pickup_slot = -1;
		}
		//点击空格子或不同物品，交换
		else if(selected_slot != -1){
			inv_grid[# 0, pickup_slot] = ss_item;
			inv_grid[# 1, pickup_slot] = inv_grid[# 1, selected_slot];
			
			inv_grid[# 0, selected_slot] = pp_item;
			inv_grid[# 1, selected_slot] = pp_num;
			
			pickup_slot = -1;
		}
		//点击地图，扔下物品

		//点击其他部分，pickup slot复原
		else{
			pickup_slot = -1;
		}
	}
	else if(mouse_check_button_pressed(mb_right)){
		pickup_slot = -1;
	}
}
#endregion

#region Drop Item
else if(ss_item != item.none){
	//drop item into game world
	if(mouse_check_button_pressed(mb_middle)){
		inv_grid[# 1, selected_slot] -= 1;
		//destroy item in inventory if it was the last one
		if(inv_grid[# 1, selected_slot] == 0){
			inv_grid[# 0, selected_slot] = item.none;
			inv_grid[# 1, selected_slot] = 0;
		}
		//create item
		var inst = instance_create_layer(obj_player.x,obj_player.y,"Instances",obj_item);
		with(inst){
			item_num = ss_item;
			x_frame = ss_item mod other.spr_inv_items_columns;
			y_frame = ss_item div other.spr_inv_items_columns;
		}
		//CREATE notification
		if(!instance_exists(obj_notification)){
			instance_create_layer(0,0,"Instances",obj_notification);
		}
		with(obj_notification){
			var in = ss_item
			if(!ds_exists(ds_notifications,ds_type_grid)){
				ds_notifications = ds_grid_create(2,1);
				var not_grid  = ds_notifications;
				not_grid[# 0, 0] = -1;
				not_grid[# 1, 0] = inventory.ds_items_info[# 0, in];
			}else{//add to grid
				event_perform(ev_other, ev_user0);
							
				var not_grid  = ds_notifications;
				var grid_height = ds_grid_height(not_grid);
				var name = inventory.ds_items_info[# 0, in];
				var in_grid = false;
							
				//if in grid already, minus 1
				var yy= 0; repeat(grid_height){
					if(name == not_grid[# 1, yy]){
						not_grid[# 0, yy] -= 1;
						in_grid = true;
						break;
					}
								
					yy++;
				}
				//if not in grid, resize grid
				if(!in_grid){
					ds_grid_resize(not_grid, 2, grid_height+1);
					not_grid[# 0, grid_height] = -1;
					not_grid[# 1, grid_height] = inventory.ds_items_info[# 0, in];
				}	
			}
		}
	}
#endregion

	//pick up item 1
	if(mouse_check_button_pressed(mb_left)){
		pickup_slot = selected_slot; //pickup_slot = 捡起物品在第几格
	}
	
}
