var inv_grid = ds_inventory;


if(!show_inventory) exit;
//-----------draw inventory back
draw_sprite_part_ext(
	spr_inv_UI, 0,cell_size,0,inv_UI_width,inv_UI_height,
	inv_UI_x,inv_UI_y,scale,scale,c_white,1
);

//---------Player info
var info_grid = ds_player_info;

draw_set_font(fnt_text_18);
var c = c_black;
draw_text_color(info_x,info_y,info_grid[# 0, 3] + ": "+ info_grid[# 1, 3], c,c,c,c,1);

draw_set_font(fnt_smalldigits);
var yy = 0; repeat(3){
	draw_text_color((192+33*yy)*scale+info_x,info_y,string(info_grid[# 1, yy]),c,c,c,c,1);
	yy += 1;
}


#region DRAW INVENTORY ACCORDING TO DS_INVENTORY
//-------draw inventory
var ii, ix, iy, xx ,yy, sx, sy, iitem, inv_grid;
ii = 0; ix = 0; iy = 0; 

repeat(inv_slots){
	//x,y location for slot
	xx = ix*(cell_size + x_buffer)*scale + slots_x;
	yy = iy*(cell_size + y_buffer)*scale + slots_y;
	//item
	iitem = inv_grid[# 0, ii];
	sx = (iitem mod spr_inv_items_columns)*cell_size;
	sy = (iitem div spr_inv_items_columns)*cell_size;
	//draw slot
		draw_sprite_part_ext(spr_inv_UI, 0,0,0,cell_size,cell_size,xx,yy,scale,scale,c_white,1);
	if(iitem > 0){
		//draw item
		switch(ii){
			//left_clicked slot
			case pickup_slot:
				if(iitem > 0) draw_sprite_part_ext(spr_inv_items,0,sx,sy,cell_size,cell_size,xx,yy,scale,scale,c_white,0.2);
			break;
			//mouse on slot
			case selected_slot: 
				if(iitem > 0) draw_sprite_part_ext(spr_inv_items,0,sx,sy,cell_size,cell_size,xx,yy,scale,scale,c_white,1);
				gpu_set_blendmode(bm_add);
				draw_sprite_part_ext(spr_inv_UI,0,0,0,cell_size,cell_size,xx,yy,scale,scale,c_white,3);
				gpu_set_blendmode(bm_normal);
			break;
			//normal slot
			default: 
				if(iitem > 0) draw_sprite_part_ext(spr_inv_items,0,sx,sy,cell_size,cell_size,xx,yy,scale,scale,c_white,1);
			break;
		}
		//draw item number
		draw_text_color(xx,yy,string(inv_grid[# 1,ii]),c,c,c,c,1);
	}
	//increment
	ii += 1;
	ix = ii mod inv_slots_width;
	iy = ii div inv_slots_width;
}
#endregion

//-----------draw description
if(selected_slot != -1){
	var iinfo_grid = ds_items_info, description = "";
	var sitem = inv_grid[# 0, selected_slot];
	if(sitem > 0){
		draw_set_font(fnt_text_9);
		var iinfo = iinfo_grid[# 1, sitem];
		description = iinfo_grid[# 0, sitem] + ": " + iinfo + " is " + iinfo + " that is " + iinfo;
		draw_text_ext_color(desc_x,desc_y,description,string_height("M"),info_width,c,c,c,c,1);
	}
}
//-----------draw picked_up item and amount
if(pickup_slot != -1){
	//item
	iitem = inv_grid[# 0, pickup_slot];
	sx = (iitem mod spr_inv_items_columns)*cell_size;
	sy = (iitem div spr_inv_items_columns)*cell_size;
	draw_sprite_part_ext(spr_inv_items,0,sx,sy,cell_size,cell_size,mousex,mousey,scale,scale,c_white,1);
	//number
	var inum = string(inv_grid[# 1,pickup_slot]);
	draw_text_color(mousex,mousey,inum,c,c,c,c,1);

}