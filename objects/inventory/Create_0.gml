/// @description Insert description here
depth = -1;
scale = 1.5;
show_inventory = false;

inv_slots = 24; //背包总格数
inv_slots_width = 8; //横向有几格
inv_slots_height = 3;

selected_slot = -1;
pickup_slot = -1;
m_slotx = 0;
m_sloty = 0;

x_buffer = 2; //两格间的缝隙宽度
y_buffer = 3;

gui_width = display_get_gui_width();
gui_height = display_get_gui_height();

cell_size = 32;

inv_UI_width = 288;
inv_UI_height = 192;
info_width = (inv_UI_width - 2*x_buffer)*scale;

spr_inv_UI = spr_inventory_UI;
spr_inv_items = spr_inventory_items;

spr_inv_items_columns = sprite_get_width(spr_inv_items)/cell_size; //sprite一行有几格
spr_inv_items_rows = sprite_get_height(spr_inv_items)/cell_size;

inv_UI_x = (gui_width * 0.5)-(inv_UI_width*0.5*scale);//背包ui的左上角
inv_UI_y = (gui_height * 0.5)-(inv_UI_height*0.5*scale);

info_x = inv_UI_x + 9*scale;
info_y = inv_UI_y + 9*scale;

slots_x = info_x;   //背包第一格的坐标
slots_y = inv_UI_y + (40 * scale);

desc_x = info_x;
desc_y = inv_UI_y + (156*scale);

//---------Player Info

ds_player_info = ds_grid_create(2,4);
ds_player_info[# 0,0] = "Gold";
ds_player_info[# 0,1] = "Silver";
ds_player_info[# 0,2] = "Copper";
ds_player_info[# 0,3] = "Name";

ds_player_info[# 1,0] = irandom_range(0,99);
ds_player_info[# 1,1] = 10;
ds_player_info[# 1,2] = 10;
ds_player_info[# 1,3] = "Player";

//--------Inventory
ds_inventory = ds_grid_create(2, inv_slots);
//--------items
enum item {
	none		= 0,
	tomato		= 1,
	potato		= 2,
	carrot		= 3,
	artichoke	= 4,
	chilli		= 5,
	gourd		= 6,
	corn		= 7,
	wood		= 8,
	stone		= 9,
	bucket		= 10,
	chair		= 11,
	picture		= 12,
	axe			= 13,
	potion		= 14,
	starfish	= 15,
	mushroom	= 16,
	height		= 17,
}

#region create items info grid
ds_items_info = ds_grid_create(2, item.height);

//------item name
var z= 0, i = 0;
ds_items_info[# z, i++] = "Nothing";
ds_items_info[# z, i++] = "tomato";
ds_items_info[# z, i++] = "potato";
ds_items_info[# z, i++] = "carrot";
ds_items_info[# z, i++] = "artichoke";
ds_items_info[# z, i++] = "chilli";
ds_items_info[# z, i++] = "gourd";
ds_items_info[# z, i++] = "corn";
ds_items_info[# z, i++] = "wood";
ds_items_info[# z, i++] = "stone";
ds_items_info[# z, i++] = "bucket";
ds_items_info[# z, i++] = "chair";
ds_items_info[# z, i++] = "picture";
ds_items_info[# z, i++] = "axe";
ds_items_info[# z, i++] = "potion";
ds_items_info[# z, i++] = "starfish";
ds_items_info[# z, i++] = "mushroom";
//------item info
var z= 1, i = 0;
ds_items_info[# z, i++] = "Nothing";
ds_items_info[# z, i++] = "a tomato";
ds_items_info[# z, i++] = "a potato";
ds_items_info[# z, i++] = "a carrot";
ds_items_info[# z, i++] = "an artichoke";
ds_items_info[# z, i++] = "a chilli";
ds_items_info[# z, i++] = "a gourd";
ds_items_info[# z, i++] = "a corn";
ds_items_info[# z, i++] = "a wood";
ds_items_info[# z, i++] = "a stone";
ds_items_info[# z, i++] = "a bucket";
ds_items_info[# z, i++] = "a chair";
ds_items_info[# z, i++] = "a picture";
ds_items_info[# z, i++] = "an axe";
ds_items_info[# z, i++] = "a potion";
ds_items_info[# z, i++] = "a starfish";
ds_items_info[# z, i++] = "a mushroom";
#endregion

var yy = 0; repeat(inv_slots){
	if(irandom(100)>10){
		ds_inventory[# 0, yy] = irandom(item.height-2)+1;
		ds_inventory[# 1, yy] = irandom(10)+1;
		yy +=1;
	}
}

