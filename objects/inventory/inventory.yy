{
    "id": "7ace241f-0587-4af6-affc-47230eb3dfb7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "inventory",
    "eventList": [
        {
            "id": "153ff5ff-55f2-48ed-87c1-3727d840dc70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7ace241f-0587-4af6-affc-47230eb3dfb7"
        },
        {
            "id": "9db306b7-9ff5-49e1-94b3-f06d7a1d504c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "7ace241f-0587-4af6-affc-47230eb3dfb7"
        },
        {
            "id": "4d6810aa-86b8-42d2-bbb0-e9317daecf1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7ace241f-0587-4af6-affc-47230eb3dfb7"
        },
        {
            "id": "2a083a77-8b15-4efd-bd1c-7936a3285f66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "7ace241f-0587-4af6-affc-47230eb3dfb7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}