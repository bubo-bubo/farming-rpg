{
    "id": "3f79eae4-f511-4a56-8d79-932fbb671714",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "depthsorter",
    "eventList": [
        {
            "id": "f7ff1715-95fa-4aa3-a311-6384c7347339",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3f79eae4-f511-4a56-8d79-932fbb671714"
        },
        {
            "id": "8fd1a439-2de3-410c-90cb-ca36a035cdaa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "3f79eae4-f511-4a56-8d79-932fbb671714"
        },
        {
            "id": "05868718-1bbf-4b6d-ac59-25543021a0cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3f79eae4-f511-4a56-8d79-932fbb671714"
        },
        {
            "id": "433ac5e2-8d8a-42f9-82f9-7e9e0965db3a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "3f79eae4-f511-4a56-8d79-932fbb671714"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}