/// @description Insert description here
if(!planting) exit;

var cs = cellSize;
var gx = (mx div cs);
var gy = (my div cs);

//stick to grids
xx = gx*cs;
yy = gy*cs;

//what is in the grid
var cell = ds_crops_instances[# gx, gy];

//check for soil
var lay_id = layer_get_id("T_Soil");
var map_id = layer_tilemap_get_id(lay_id);
var data = tilemap_get_at_pixel(map_id,mx,my);

//if no soil or already planted, draw a warning box
if(data == 0 or cell != 0){
	draw_set_alpha(0.3);
	draw_rectangle_color(xx,yy,xx+cs,yy+cs,c_red,c_red,c_red,c_red,false);
	draw_set_alpha(1);
}

//draw the crop to be planted
draw_sprite(spr_crops_picked, selectCrop,xx + cs/2, yy + cs/2);
