{
    "id": "12c10297-3247-4157-b98d-3ef664bb6634",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "crops",
    "eventList": [
        {
            "id": "4f3b81f4-83da-44f3-b5d1-fa662b7c092b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "12c10297-3247-4157-b98d-3ef664bb6634"
        },
        {
            "id": "34c8a65a-446a-401a-980d-781757b7f300",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "12c10297-3247-4157-b98d-3ef664bb6634"
        },
        {
            "id": "60bd56a4-6833-4cfd-9d5a-63df1039e9ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "12c10297-3247-4157-b98d-3ef664bb6634"
        },
        {
            "id": "6091a0e7-09a4-4bd4-831e-86fdbc449b8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "12c10297-3247-4157-b98d-3ef664bb6634"
        },
        {
            "id": "cc9d0afc-73d8-472c-aefd-e6c8cbb7b58d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "12c10297-3247-4157-b98d-3ef664bb6634"
        },
        {
            "id": "14e2462f-0293-45f5-ab9a-99c5e1f7343f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "12c10297-3247-4157-b98d-3ef664bb6634"
        },
        {
            "id": "0e290ddc-99c1-465f-9102-3e38ee419bf9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "12c10297-3247-4157-b98d-3ef664bb6634"
        },
        {
            "id": "9f5864cf-0164-48d8-b30b-e896d9395e3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "12c10297-3247-4157-b98d-3ef664bb6634"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}