/// @description Insert description here
#region PLANTING
if(room != rm_farm) {planting = false; exit;}
if(keyboard_check_released(ord("P"))){planting = !planting;}
if(planting){
	mx = mouse_x; my = mouse_y;
	
//select crop	
	if(mouse_wheel_up()) selectCrop += 1;
	if(mouse_wheel_down()) selectCrop -= 1;
	
	if(selectCrop > sprite_get_number(spr_crops_picked) - 1) selectCrop = 0;
	else if(selectCrop < 0) selectCrop = sprite_get_number(spr_crops_picked) - 1;
	
//create crop
	if(mouse_check_button_released(mb_left)){
		instance_create_crop(mx,my,selectCrop);
	}
}
#endregion


//Press G to grow a day
if(instance_exists(obj_crop)) and keyboard_check_released(ord("G")){
	with(obj_crop){
		if(growthStage < maxGrowthStage){
			var firstGrow = 0;
			if (daysOld >= 1) firstGrow = 1;
			daysOld += 1;
			growthStage = firstGrow + (daysOld div growthStageDuration);
		}
		else{
			fullyGrown = true;
			alarm[1] = 1;
		}
	}
}