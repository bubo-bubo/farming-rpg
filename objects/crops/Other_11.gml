//grow everyday
if(room == rm_farm){
	if(instance_exists(obj_crop)){
		with(obj_crop){
			if(growthStage < maxGrowthStage){
				var firstGrow = 0;
				if (daysOld >= 1) firstGrow = 1;
				daysOld += 1;
				growthStage = firstGrow + (daysOld div growthStageDuration);
			}
			else{
				fullyGrown = true;
				alarm[1] = 1;
			}
		}
	}
}
else if(ds_crops_data[# 0, 0] != -1){
	var inst_num = ds_grid_height(ds_crops_data);
	var yy = 0; repeat(inst_num){
		ds_crops_data[# 3,yy] += 1;
		yy += 1;
	}
}