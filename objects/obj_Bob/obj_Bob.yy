{
    "id": "c72b792b-9684-4503-9bd8-182a871d8202",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Bob",
    "eventList": [
        {
            "id": "fe725f43-c4bc-416c-8d78-3c2109d22c8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c72b792b-9684-4503-9bd8-182a871d8202"
        }
    ],
    "maskSpriteId": "a8ef778c-e1a3-4369-83cf-e6046e3ab6a0",
    "overriddenProperties": null,
    "parentObjectId": "02006da4-e10f-49c6-b0a0-4c447addc668",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a8ef778c-e1a3-4369-83cf-e6046e3ab6a0",
    "visible": false
}