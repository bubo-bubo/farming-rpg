event_inherited();

spr_base = spr_bases_male_2;
spr_torso = spr_torso_male_shirt_white;
spr_legs = spr_legs_male_pants_green;
spr_hair = spr_hair_male_messy_raven;
spr_feet = spr_feet_male_shoes_black;

portrait_index	= 7;
text = ["Hey there, I'm Bob.","How's your farm?"];
name = "Bob";
voice = snd_voice4;
speakers = [id,id];