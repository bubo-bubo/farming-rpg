{
    "id": "c8f90ba8-2ba5-4822-980d-93045b1168c9",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_text_24",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Calibri",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "42f4b6ab-8f7f-4333-83a8-189748d82ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 39,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8209de5b-5dd3-4f9a-a8e1-1ea079f00825",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 39,
                "offset": 4,
                "shift": 10,
                "w": 4,
                "x": 13,
                "y": 166
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "434760f3-677d-4d8f-97d6-7335fb88a1b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 9,
                "x": 2,
                "y": 166
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cffdc997-f05e-4b31-9324-bac3fc43d56f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 39,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 238,
                "y": 125
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "30423022-1d72-45cb-9b98-1b85094c9037",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 222,
                "y": 125
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2ca71750-2dd1-4fac-bf53-8f949ad42d31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 39,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 199,
                "y": 125
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "753fd4f8-4498-47e4-af0f-5752dbdbf110",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 39,
                "offset": 0,
                "shift": 23,
                "w": 21,
                "x": 176,
                "y": 125
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "759165c9-61e7-44a4-9236-df31432a32bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 39,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 170,
                "y": 125
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "98bbfc8c-3c93-4c0b-b7ae-86840bea8239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 39,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 162,
                "y": 125
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "547d192f-1bdd-43c4-8268-7a6cab3d4981",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 39,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 154,
                "y": 125
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8907829a-5558-448d-92ad-e0a22e1b7164",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 39,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 19,
                "y": 166
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "9937ee40-338f-4a5c-80f7-06c447e364de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 138,
                "y": 125
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5707fade-2390-4550-b075-5b3b4cc7ff87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 39,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 114,
                "y": 125
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "fe258409-9445-48f5-ba68-22022782d46a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 39,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 104,
                "y": 125
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "38dec5b8-dde8-4978-9c41-fa6462321a48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 39,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 97,
                "y": 125
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b047b8f4-ae45-4d81-a9c4-23728a8638c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 39,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 82,
                "y": 125
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "5559e4cf-2495-4f70-bbd3-b181eba13e0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 125
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d9c0dd57-edd9-40f4-b4df-3547eb63130a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 39,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 51,
                "y": 125
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8baa4803-8cc6-412b-9c10-6f3cf97d5507",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 35,
                "y": 125
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "650ca0d4-6b36-4978-b773-ea63e955d742",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 19,
                "y": 125
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "df798fb8-8480-4d57-9f2f-942e8661b878",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 2,
                "y": 125
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "70d4f00e-0269-453e-a32f-53b1ff196858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 39,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 123,
                "y": 125
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "da4d4269-82c5-4faf-83e4-91881a8d5be2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 52,
                "y": 166
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8fd5554a-201e-42cc-a0b6-73d39e35fc2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 214,
                "y": 166
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2e3212ae-f455-419a-a512-d37a56914a48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 68,
                "y": 166
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d462b2f9-681a-4056-924e-0ae344837789",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 126,
                "y": 207
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b7426cd5-ccbc-4c37-a816-a8c4f5bd4d78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 39,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 120,
                "y": 207
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a45da66c-4870-4c21-80e7-4f5b6ecb7d91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 39,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 112,
                "y": 207
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "35f952bc-98f0-42b6-8d9d-3201290962d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 97,
                "y": 207
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "97583da7-1b56-40ba-966b-13f39afb6f74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 81,
                "y": 207
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7c601ba1-e464-4634-a21e-281faf089e2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 39,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 66,
                "y": 207
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f7169970-f095-4e71-abca-6ccfb3699318",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 52,
                "y": 207
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c559a8fb-43c1-485f-9c36-9a25d1327369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 39,
                "offset": 0,
                "shift": 29,
                "w": 27,
                "x": 23,
                "y": 207
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d01d07e2-2ae4-49f1-9da5-9c1041db2f9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 39,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 207
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "26193821-9971-4f4b-a347-98794d4ab8a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 39,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 142,
                "y": 207
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f33f5cd1-f62e-48a0-80a3-b9f88ab94b71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 39,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 230,
                "y": 166
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "eb19159e-aad8-4e00-bd9c-5b69072b8152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 195,
                "y": 166
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a884eae1-5994-4ff5-b0c2-1b21b6ff813e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 39,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 181,
                "y": 166
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6936dc6e-3b27-4902-922c-d9397833d306",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 167,
                "y": 166
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "8d478c81-f4d6-45bf-95e4-5efe01136b62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 148,
                "y": 166
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "fc9b7663-8e58-4d1b-8825-1ac4cd9f911f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 130,
                "y": 166
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3aefd509-4d94-4b34-8b46-2901fa9c10fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 39,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 124,
                "y": 166
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "7f656cf3-e617-49ce-a772-e29740044d61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 39,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 114,
                "y": 166
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ffb06483-533a-4f0d-921f-f011760c44eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 39,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 97,
                "y": 166
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "59937504-cdf8-4298-bb2e-8a9c1a65a1ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 84,
                "y": 166
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5cb496a5-2355-4950-91dd-e01177017a97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 39,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 227,
                "y": 84
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f7d69d16-cdcf-4cf9-b197-7943ccf26161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 39,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 33,
                "y": 166
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a8ab1ea4-6e94-4ba1-b613-d2ffcafa5e8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 39,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 206,
                "y": 84
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c57132d8-66a9-422d-8147-9b1312cdc7b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 39,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 110,
                "y": 43
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "90b2218b-b811-4b03-8bd5-a265aa7e480e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 39,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 71,
                "y": 43
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c113ce0b-c653-4b7c-addf-1df3e7416be7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 39,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 54,
                "y": 43
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "855b390e-fd6c-41c2-a521-0296db845d70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 39,
                "y": 43
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "02379fcc-6bbb-4307-a543-98ea4b984bb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 39,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 21,
                "y": 43
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c41a6620-1136-450c-9c5c-14afd6d3374d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 39,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 2,
                "y": 43
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "886a2f19-9cbd-4e32-9411-d5b8ef30e7ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 39,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ae819859-1cea-4c2b-9ded-816489762217",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 39,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0d49fa94-ed6b-4a9f-8eef-8069aa3d2533",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 39,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9262c5dc-a4ad-48c7-bb23-7061ca1a4224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 39,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c1ffbcfd-e680-4b96-bba9-6f628653c795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 94,
                "y": 43
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "2e372d23-d11f-4eec-b014-1cce4404abe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 39,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c851f85f-2979-4750-a8f3-74e2ec565109",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 39,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ff9167a6-ad73-4116-bbfa-94d6da9a8dc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 39,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b308e34c-800a-4e0b-8c4b-cd9b775db4cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 39,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e266a1a5-dada-498a-9f64-e6ab57789171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 39,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7a40a0d8-1a2d-493c-8716-1757e04ca477",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 39,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "63d4787f-cdfe-4f94-bd13-78b352331b42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "69c50825-d07c-45f7-8daf-0baed61fc676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 39,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "94f2abd2-8300-426d-a756-528857a4143e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "581c4279-8b9d-4384-b6af-5cd6839a0a23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 39,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a75bef39-d6c3-449d-8087-9967d45d9dd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "523f3d2c-4cd2-433e-bea6-60c9ec462428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 39,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 126,
                "y": 43
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "74d09c83-1daf-41dc-bb56-c3307873ebb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 39,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 29,
                "y": 84
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "61e1297f-4e6b-4a42-acb4-aa332e7d568d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 39,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 138,
                "y": 43
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "6ea664c8-d1e2-4f2e-b558-fb088909f273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 39,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 178,
                "y": 84
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "55759cda-473d-4852-879b-7194ed9ff0ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 39,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 168,
                "y": 84
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8a3a53c0-6558-4bb1-89d7-b81774c5c185",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 153,
                "y": 84
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "0d5ac0dc-2fce-40cd-b870-15e5aeab7888",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 39,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 147,
                "y": 84
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d35dec85-0ae9-4012-bfa1-de7aa4dbbf8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 39,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 123,
                "y": 84
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ce973660-021f-4c72-84cf-24bbf750de06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 39,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 108,
                "y": 84
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "355a487b-6ead-42ce-9f00-d09543758312",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 39,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 91,
                "y": 84
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6353d8c0-e30f-4cf3-9dca-01ea293823b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 39,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 75,
                "y": 84
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "772702d1-5d2a-457f-b0b6-24c7936331da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 39,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 59,
                "y": 84
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "400a661d-f7ef-4433-adc4-4418be287dca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 39,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 184,
                "y": 84
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "7ea5d006-db29-4fcd-a9a3-4ed1bf12fa73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 46,
                "y": 84
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e2dbbe34-c361-4fcd-82d1-bed5dd98cd54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 39,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 17,
                "y": 84
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f5cf3eb4-b9ac-4332-87a5-089adae82629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 39,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 2,
                "y": 84
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3f0b0d15-779e-4de8-ae17-1e0e7bee72f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 39,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 238,
                "y": 43
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5e01271d-6e95-4758-b076-83f74dd6b051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 39,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 214,
                "y": 43
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "3058041a-747f-477c-9f2a-a7d0c6a4ed31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 199,
                "y": 43
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "bbdddc38-1329-464b-8f33-856d9e48ec26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 39,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 182,
                "y": 43
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "85e1e438-fdfe-46cc-8602-669a92876049",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 169,
                "y": 43
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0163a586-3c56-470e-8ee1-d64ec52d900c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 158,
                "y": 43
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "bf698a9c-09bb-4974-82ed-db8f52922f7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 39,
                "offset": 6,
                "shift": 15,
                "w": 3,
                "x": 153,
                "y": 43
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "52a8b530-366a-4775-9d74-2308cbd3af94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 39,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 195,
                "y": 84
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4efd3a28-f0ab-4b71-bf95-fe032c0a202a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 159,
                "y": 207
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "39f0e74c-e242-4aed-9bc5-455eda4498a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 63
        },
        {
            "id": "37ab8ad4-ea88-42a6-bbe3-25232a93003f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "9176f2fe-eb32-4342-a2a5-54531611e71d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 85
        },
        {
            "id": "2fea6e9a-c489-4cc1-86cb-5a03a64b1274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "d52e969f-8f0b-4a5f-8926-4276dc955be4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "e19e0351-019e-416b-a847-3fc4e7eb0ab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 89
        },
        {
            "id": "83671156-d39c-46f0-a15a-6545bdb61dd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 116
        },
        {
            "id": "21c006e8-ea21-4fb2-ba68-3e73b4bbbe9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "f15b2011-a4dd-4b8a-99df-fe526639710d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "06488aaa-0f9d-4603-9dd5-ada34d395df0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 217
        },
        {
            "id": "12bc00e1-1375-4e62-977a-eb29d016817e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 218
        },
        {
            "id": "c61fa4e7-d06b-431b-ba5d-41828392320f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 219
        },
        {
            "id": "36caa0b7-1f12-45fb-8e4b-2bd0f30c81fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 220
        },
        {
            "id": "e235c5f8-3063-431d-abf9-6614b67c8349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 221
        },
        {
            "id": "3779a988-be18-430e-851c-8eff21867129",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 253
        },
        {
            "id": "1264d1ed-f3e7-4610-a575-bc6f51dd96f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 255
        },
        {
            "id": "646237ea-cb1f-4b49-acbc-6f97f38e73d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 356
        },
        {
            "id": "76a5f85d-9e56-4d55-9462-fc644e233480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 357
        },
        {
            "id": "a39c3e53-628d-4e96-bc8f-24c4ca1902bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 360
        },
        {
            "id": "5448d50b-3ec1-4eec-ba57-3fe83026fc64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 362
        },
        {
            "id": "509e0495-b62a-41cf-86ba-6ccbf49d4f60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 364
        },
        {
            "id": "20d1dd7b-39ba-42f8-9bb4-4a4c7ac7ce72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 366
        },
        {
            "id": "b5d6c653-1284-4e34-bd8f-2ac54706126c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 368
        },
        {
            "id": "ad4db74f-7611-4db7-a19c-f02f459569f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 370
        },
        {
            "id": "71f4b88b-2412-41f8-91d9-3adf5675ff4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 372
        },
        {
            "id": "a1cce0b4-2022-47f1-93a8-1e59c4bd9511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 374
        },
        {
            "id": "6e65dc44-0dd9-4d7f-bac9-1b49a537cae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 375
        },
        {
            "id": "4a644add-7e59-4242-97fe-c716304d2d64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 376
        },
        {
            "id": "4c2169e5-4269-4957-a723-1b1b03ce3f51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 431
        },
        {
            "id": "d3885e32-12ed-4d5c-91a0-87199e126920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 538
        },
        {
            "id": "38bc7055-f5db-4fc0-9ca3-0bc58f5fd50c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 539
        },
        {
            "id": "eda02b26-ef75-4e0f-92d6-d410d618bdbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7808
        },
        {
            "id": "84ca7a80-7c61-40ea-886f-2dbcf29aefd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7810
        },
        {
            "id": "99d250f6-26ea-4d3d-ad45-c2e2239c7381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7812
        },
        {
            "id": "d59224ed-2a2c-4b06-b008-48e03c006914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7908
        },
        {
            "id": "2c25559b-b653-447f-b620-3bc0da2169fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7910
        },
        {
            "id": "c1d8aade-4244-4da5-bbfa-c074e54cf712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7912
        },
        {
            "id": "e56f2df6-f789-4986-b9b5-cdfbf065370b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7914
        },
        {
            "id": "0938abd1-a6a4-4f40-8d66-872c4d5a3d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7916
        },
        {
            "id": "5823c810-356c-45cc-9480-1de255be18cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7918
        },
        {
            "id": "bba95666-f0bc-489b-8178-cb50dd78e119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7920
        },
        {
            "id": "38640a9f-fbd0-4ec5-9ad1-1c5acd7c6a92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7922
        },
        {
            "id": "d6358e3e-d256-4731-b40e-5dd2b3180bda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7923
        },
        {
            "id": "99e79f5a-c7fb-4f3c-b1d2-ccce465ff67c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7924
        },
        {
            "id": "252bafcd-c35e-43ea-a380-11d2f9b9eada",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7925
        },
        {
            "id": "15c8604c-7a07-4abf-beaf-98f82c77066c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7926
        },
        {
            "id": "ab4547cd-73e5-46d2-bf1c-91c3e2329c33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7927
        },
        {
            "id": "cac97ba9-0fbc-45c1-8ace-ce67475f380b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7928
        },
        {
            "id": "1186c0cd-a1e1-4a43-b46d-d7c50569f414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7929
        },
        {
            "id": "e166227f-b765-4f5c-b4b0-1dc9e14a1759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8216
        },
        {
            "id": "6476328f-2abf-4bd6-9091-73cadde786d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "931bbb01-d29f-453a-af1c-2bdc0e0898e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8220
        },
        {
            "id": "97c0a451-9e63-4bca-9380-f6e5ff5a688a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "e483d8ef-3d4a-43a2-9f4e-dcaaf18c8147",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "c7ed4046-2dc9-44d5-b6b2-6192a2fa0e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 86
        },
        {
            "id": "97f37e4a-d372-45de-bc01-361f44b2566a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 88
        },
        {
            "id": "98a349ad-5a83-4f9b-a9fb-2054cf35d1dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "d54cacd5-ade5-413c-aedf-c574c803a5de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 221
        },
        {
            "id": "f5343fe4-7f46-4ff8-9e7e-4d2111cee721",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 356
        },
        {
            "id": "fb914422-a940-43a3-bb15-5751c91eab40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 374
        },
        {
            "id": "fe043f73-a7a3-46c3-bf14-3a7e9cd61a7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 376
        },
        {
            "id": "e8143d52-f6b3-4026-bbf5-5a838cc02cf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 538
        },
        {
            "id": "30d449ab-51d4-4532-86f1-1ac4cf897a55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7922
        },
        {
            "id": "952aedc9-dae1-4b93-ab82-73959141735b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7924
        },
        {
            "id": "cc47a7ef-e662-496c-badc-6c32ed7ddb25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7926
        },
        {
            "id": "49d906b9-8b0c-4dab-9755-c80cd9b9bd7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7928
        },
        {
            "id": "2c6a304c-8ae6-43cd-a4f4-3fd1f166acfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 8217
        },
        {
            "id": "2b36aa12-5636-40eb-aa3e-a48947b9fad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 8221
        },
        {
            "id": "2491daee-accd-4747-b70c-42ef8af6222f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "337ef607-7352-46f1-a9a2-1c0959bb43e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 46
        },
        {
            "id": "f5129d71-45d8-4b57-9c9f-e6eeae780fd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "45c9b3d9-5996-49f1-844b-57471b719024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "7d051d79-4326-4b43-a36d-3efd2cc9a47d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "a0f45cd7-9122-4b4b-bc18-5188781aa95b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 192
        },
        {
            "id": "3debc91b-7102-42eb-ae45-f8ad1fbd5ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 193
        },
        {
            "id": "9eea7e82-e809-43b5-b40c-026e5435372e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 194
        },
        {
            "id": "bf40446d-4bc6-411b-a952-e85f6f662c91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 195
        },
        {
            "id": "56b9c842-53c2-4845-85ae-b9feee860f8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 196
        },
        {
            "id": "8b945cef-23c8-4116-a09d-cbd9f7b7b061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 197
        },
        {
            "id": "b9bf4891-e528-4054-8d9f-5dd2e6998d35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 198
        },
        {
            "id": "ec5ed36b-6327-4d47-84d2-abdde2f272b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 221
        },
        {
            "id": "db8a95b1-22a4-4e27-98ba-06a517ac794c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 256
        },
        {
            "id": "780d0aa7-3e62-4bf7-9a35-b7ec2d8f8df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 258
        },
        {
            "id": "ca1da89f-ac86-462e-983e-0f5f0fb1825f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 260
        },
        {
            "id": "a5b4747c-9020-4f16-9286-e81b611f3ced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 374
        },
        {
            "id": "c18de544-fd92-468a-beba-721da2c1e89d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 376
        },
        {
            "id": "f9172931-040a-4278-a274-bd57a942eb5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 506
        },
        {
            "id": "9ba49aaa-bb76-4dbf-9a4a-56ec3d6f2311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7840
        },
        {
            "id": "e6c6a9f0-2674-4642-8910-0e5463610dbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7842
        },
        {
            "id": "2c1246d5-a17c-4b10-a3bc-909b0cb64af4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7844
        },
        {
            "id": "6e75eefc-7ffa-4393-b0a0-82f64ce0f289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7846
        },
        {
            "id": "f713b70f-e120-479a-ae09-6c8b1e793f10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7848
        },
        {
            "id": "6954b445-dac4-4319-99be-fbb465469a88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7850
        },
        {
            "id": "07c68b56-4a5d-4143-b305-72edb51f14bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7852
        },
        {
            "id": "7ec63578-1c02-492b-8e7b-48d7055ddd73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7854
        },
        {
            "id": "00a128b4-7988-44eb-b139-4f1d9f530479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7856
        },
        {
            "id": "7f83660b-caf9-4a57-a7da-28b484ba5dbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7858
        },
        {
            "id": "371954d4-6689-4bc6-99ac-6807e7100e4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7860
        },
        {
            "id": "ac72f99a-2a12-4459-83c8-296fc2c524f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7862
        },
        {
            "id": "67d9a175-0c96-437e-821c-f36e7bc09ea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7922
        },
        {
            "id": "45524277-2b3d-440d-a0fb-0db56af2af16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7924
        },
        {
            "id": "733871c6-7c42-4f36-9818-fd5eb70d2190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7926
        },
        {
            "id": "b79aff16-040a-4e9d-8842-42e6a7e22488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7928
        },
        {
            "id": "ab764499-5f1f-41c2-9eb2-994207af78a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 45
        },
        {
            "id": "60b09f82-0bbf-455f-abfd-4a22bca67397",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 79
        },
        {
            "id": "27b81fe7-690c-4bef-86d4-258eaae06ec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 81
        },
        {
            "id": "45c3d999-83a9-4082-9763-340d7cd1b466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 101
        },
        {
            "id": "1d4fa03e-a034-4999-869c-6a1bf77565a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 102
        },
        {
            "id": "fc2daaaa-7c10-4093-b131-f9a28ac8121c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 111
        },
        {
            "id": "e0b763f5-4c55-4896-8ee8-dde07ddf0145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 118
        },
        {
            "id": "d8a75f15-8fc4-419e-821b-2c82f9e972ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 119
        },
        {
            "id": "4e4b2d02-6969-4e50-b6a4-79241c1c41ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 121
        },
        {
            "id": "bcde0541-f17a-4441-9023-9f99f1a6edd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 173
        },
        {
            "id": "ed9c3457-262d-4465-a742-3c05b34c397a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 210
        },
        {
            "id": "ea219d43-87eb-41e6-8a84-30a103840258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 211
        },
        {
            "id": "9dbb1db9-5e76-41eb-988a-482facc564c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 212
        },
        {
            "id": "d12d0592-2a2f-4e30-bf27-e690b03f7757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 213
        },
        {
            "id": "6904bbe6-81f8-4c06-bcdb-ba188e044ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 214
        },
        {
            "id": "13eb0eef-6b2c-4cd8-a908-4d602c6b2aaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 232
        },
        {
            "id": "6b9346df-30a2-4948-977c-e1745c67b14d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 233
        },
        {
            "id": "2b8eefe0-75d1-4f6e-9fc8-f7cc3320be1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 234
        },
        {
            "id": "3ed24bdc-db0a-474d-acfe-c2c88c727e0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 235
        },
        {
            "id": "45774630-ca57-42bf-a895-64cff392eee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 242
        },
        {
            "id": "947dfa26-d4b8-4d30-b599-50b7e0f2a51a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 243
        },
        {
            "id": "0d1e6d40-675d-4371-83ac-9abd29419660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 244
        },
        {
            "id": "f70ac4dd-54a4-45b3-b31f-4d3c220c0808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 245
        },
        {
            "id": "9d5d7af1-ee00-43a1-a653-9badc5be78c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 246
        },
        {
            "id": "6e2d4960-2982-45e9-aa34-4543a3710b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 248
        },
        {
            "id": "70a2c396-19a0-4155-909d-b41103b9ef68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 253
        },
        {
            "id": "38830b87-ca12-4384-a150-1cad482661de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 255
        },
        {
            "id": "015d4d2e-49a1-4126-b2b6-13234cafc920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 275
        },
        {
            "id": "a1c28f23-bdf8-4a61-a892-a51d9703e298",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 277
        },
        {
            "id": "708f8db7-2cb7-4017-a2f9-49b7f56b065b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 279
        },
        {
            "id": "8f636fc5-0fcc-437b-aa30-769afdf5d420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 281
        },
        {
            "id": "f9831852-4df8-4ed0-92d9-762821277c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 283
        },
        {
            "id": "69eba63d-21b8-4a98-980e-b3383bec32fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 332
        },
        {
            "id": "6de973bd-524e-42da-b4e9-536a587a2617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 333
        },
        {
            "id": "63b00031-115e-4965-b4e6-8f42496b6647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 334
        },
        {
            "id": "cc933cb9-0a4a-4301-a9bd-a20c23093ed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 335
        },
        {
            "id": "2ec5d018-3849-4b41-90eb-b22f7461c0ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 336
        },
        {
            "id": "dfe7d046-e3d1-4ffa-8a8c-d69b34938f26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 337
        },
        {
            "id": "cb04e370-7fbe-479e-bb25-fc65ae35484c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 338
        },
        {
            "id": "0eed817e-a0f3-4674-9a8f-88c5f4afef7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 339
        },
        {
            "id": "f357f26e-8bc4-47a4-b445-56dc4ede2df0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 373
        },
        {
            "id": "3b7cb7fe-548f-479c-9d64-741c9a107916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 375
        },
        {
            "id": "06f4abc9-c17e-42db-be5c-d2af88a4f0ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 416
        },
        {
            "id": "31fed82d-2888-4b23-8346-d73b1a3e85fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 417
        },
        {
            "id": "bad28ce1-a25d-4a5a-a1d7-8eb48275ebca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 511
        },
        {
            "id": "81a1c895-970d-4cbf-8715-efcb7adc461a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7809
        },
        {
            "id": "d9a39df1-3302-4ffb-be18-bd6f8bede5da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7811
        },
        {
            "id": "ffd2c650-8b34-4b5b-bd4f-a1543954cbc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7813
        },
        {
            "id": "89877c98-d07a-4ffe-83e3-3ac4e584092e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7865
        },
        {
            "id": "067a1b41-0614-4e45-add9-a7ff6b76ddea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7867
        },
        {
            "id": "ba3106de-6a0f-4fb8-8cd2-f373ee315e8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7869
        },
        {
            "id": "c0073aad-9159-46a5-b0d3-529dad917e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7871
        },
        {
            "id": "b1664f56-872d-434e-822c-6b74a7f150a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7873
        },
        {
            "id": "f3321277-407c-4f9b-9c68-d445a6470d71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7875
        },
        {
            "id": "6535eef3-fd9d-4367-aa85-3427e2f34e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7877
        },
        {
            "id": "63a80884-8271-4e17-9b73-066dcf7a89d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7879
        },
        {
            "id": "fb554061-e3a1-402c-bf14-3e63daab5c9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7884
        },
        {
            "id": "25f0c3c4-4ca8-4246-8be6-3f7d06842ccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7885
        },
        {
            "id": "0284d588-d3cb-4c44-92ad-1b1be8649722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7886
        },
        {
            "id": "a6d3d3da-56b6-4906-b1f7-b142ca9e1792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7887
        },
        {
            "id": "aea4c9e3-67b3-412e-ba17-67cb28033461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7888
        },
        {
            "id": "58e9c86a-4b2c-4b3b-8779-22c9e7dff4f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7889
        },
        {
            "id": "d63bfd37-730c-4b9f-bb1b-8e76cd3be370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7890
        },
        {
            "id": "98059288-3548-4f7a-a0e1-85aedf545217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7891
        },
        {
            "id": "f30abfbc-eb2c-45d2-9e29-e66fbd4bf187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7892
        },
        {
            "id": "446674af-c50e-4398-8f4a-ad58f21df167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7893
        },
        {
            "id": "ce7b0f35-4758-4f36-9268-bd9d9f19b99c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7894
        },
        {
            "id": "f4f6cd82-a1e8-48b7-b19b-ca7529ea8136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7895
        },
        {
            "id": "9d4d0e7a-3a59-4b5c-b308-4ff913303c7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7896
        },
        {
            "id": "e850811c-a443-4c92-8d53-12d9456df626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7897
        },
        {
            "id": "5622dc0b-d7df-4d4a-95c0-1fcfedc9fd8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7898
        },
        {
            "id": "fb23cb18-598f-4747-aeef-399effcabf63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7899
        },
        {
            "id": "dd236eff-50e3-4c2d-ba71-17380155926a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7900
        },
        {
            "id": "1219de0e-b7b1-442b-b0e8-eaa1503ba683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7901
        },
        {
            "id": "e85b0ecf-2d12-4342-bb32-bfebbd443803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7902
        },
        {
            "id": "19d92113-e42c-428a-80e1-b352874053fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7903
        },
        {
            "id": "f09929ec-898b-4c8d-8d78-94b5e106d8a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7904
        },
        {
            "id": "a84df5c7-1268-4838-a0ef-91629b5e8d93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7905
        },
        {
            "id": "dd044ec0-d796-4f34-b472-da4873d8e994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7906
        },
        {
            "id": "c298b7eb-4675-406f-a758-47237fe23852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7907
        },
        {
            "id": "1cace1f9-abe6-4c6d-8991-cb10c477be9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7923
        },
        {
            "id": "53f23a6f-9dd0-44ff-85ff-0698f0d222dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7925
        },
        {
            "id": "e270abf9-43be-48c2-bcd3-b5d4d3e1e59d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7927
        },
        {
            "id": "ce5ee143-504c-421e-b52e-2f2eaeadf12e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7929
        },
        {
            "id": "b4e3a908-072c-46a4-bd48-2f3637eb5ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 8208
        },
        {
            "id": "7150dbda-f783-45a5-9cc1-a75e7b8e7a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64256
        },
        {
            "id": "f5da3917-deaa-44a8-8af2-191458363b90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64257
        },
        {
            "id": "919ef970-02dd-47a5-9086-deb3f5493903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64258
        },
        {
            "id": "65be5b7d-8c03-411f-b4dd-16e6acd396fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64259
        },
        {
            "id": "26b172ca-274f-4800-8e93-efe56fbabfdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64260
        },
        {
            "id": "7d76765d-7bb1-49d7-9d9c-7378f275af56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "d2e0bd31-1382-4ef6-9313-87a4049e8728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "0e9ef05f-6465-490f-838b-ebc467e655b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 47
        },
        {
            "id": "2db15beb-2b64-4a91-a6d2-de3032b27456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "baa01203-da5e-42aa-b706-83df76641086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 74
        },
        {
            "id": "84057eb4-ee12-423c-8744-0cc6e565af80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "b1729830-28bf-4642-8ba7-f331b5b09239",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 192
        },
        {
            "id": "8f2fa8a9-f45b-4867-b05b-309289621687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 193
        },
        {
            "id": "dc69b6f7-4d52-4439-87e8-ecf11d88caaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 194
        },
        {
            "id": "f3e3e80d-a71b-4f67-b679-8920b51000ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 195
        },
        {
            "id": "ee839821-db32-4edf-a16f-31338470b33c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 196
        },
        {
            "id": "6799a625-33f2-4a67-94d9-6d216d0d6de7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 197
        },
        {
            "id": "610abc3c-f08d-4f07-8a97-9c1acef35ad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 198
        },
        {
            "id": "d256e358-a249-4eba-88c4-0daf90f3ea19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 224
        },
        {
            "id": "109a6894-c4ed-4606-8b5c-415183a68695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 225
        },
        {
            "id": "6f913479-21b4-41c4-b809-eb1941c3fdf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 226
        },
        {
            "id": "2add3091-e8b4-4923-a2ec-10ab3c458168",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 227
        },
        {
            "id": "d26ba34c-7268-4b4a-885a-0138314d6812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 228
        },
        {
            "id": "e1dd67d7-fd9b-4a90-b041-654b956474c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 229
        },
        {
            "id": "71651602-7b99-467f-86b4-55fb49e3c224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 230
        },
        {
            "id": "8a65a868-6a24-460d-a52f-42b2efd257d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 238
        },
        {
            "id": "de0191dc-2aff-4546-bc50-c7f22a4f9408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 239
        },
        {
            "id": "eec8491b-23ef-4408-985d-302e576b1ac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 256
        },
        {
            "id": "218ec046-9a97-4d9c-9caf-303ba09a2b00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 257
        },
        {
            "id": "15eb87b7-02f7-4e04-8524-3fb6b123178b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 258
        },
        {
            "id": "bbbc99e5-bf53-48a9-8564-3493a6ff0126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 259
        },
        {
            "id": "321ffe64-fd70-4927-b01a-7039b388c738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 260
        },
        {
            "id": "0901994a-d560-459e-872f-8ac04699bb29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 297
        },
        {
            "id": "1e97a816-4f66-47b8-8f3b-a985f484d6e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 299
        },
        {
            "id": "d842b529-4983-4f54-97ec-c405458af61f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 308
        },
        {
            "id": "a32dc4b4-b5cf-4f52-8215-03d28f552c6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 506
        },
        {
            "id": "081a1410-7023-4a9b-8608-ba139631659a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 507
        },
        {
            "id": "5bb78ac4-c852-496e-88a3-d78a62eaf7d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7840
        },
        {
            "id": "3e45c305-ad40-4ecf-acc8-e19ac4559e29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7841
        },
        {
            "id": "30528297-08da-4585-85f2-283a706d6682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7842
        },
        {
            "id": "97d3e176-09fa-410c-a096-784c6b1e6222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7843
        },
        {
            "id": "52dfa87d-833b-42ce-b074-777efea20f25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7844
        },
        {
            "id": "27ccd446-5b91-4e60-abe8-7b1740c79724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7845
        },
        {
            "id": "65e70dcf-bd19-4614-bac9-ba7b1920b7f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7846
        },
        {
            "id": "9c530eb2-dd94-4890-aae9-2dba1f083c57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7847
        },
        {
            "id": "8be196af-d9ce-4898-b0be-61ab9a976972",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7848
        },
        {
            "id": "07472b7c-8720-4c97-94b3-3cd8f72b304c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7849
        },
        {
            "id": "626c84a2-3477-457a-a4c3-b8e4982b1f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7850
        },
        {
            "id": "e32fa763-1818-4c25-ab65-83437f5e4f33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7851
        },
        {
            "id": "daf386d1-a0f7-4c2d-8e7f-75e1cdf56b47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7852
        },
        {
            "id": "faeee605-88fb-4095-a309-e1ef22b2e3f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7853
        },
        {
            "id": "54b3a0df-45fb-4128-acdd-99e47637864c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7854
        },
        {
            "id": "d4a801fd-db7d-4eee-a1ee-c8b86dc7dc36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7855
        },
        {
            "id": "5f430725-e3ff-4391-ab1e-051d4cbf13c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7856
        },
        {
            "id": "01323a79-8cff-4e3e-881b-01578aad6922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7857
        },
        {
            "id": "ea026a90-55a1-43a2-af8e-a87c64ac586d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7858
        },
        {
            "id": "aa1dee36-48c3-4153-8d10-7df77884460d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7859
        },
        {
            "id": "10261de1-bbad-43fe-8b8a-336c5fbb7789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7860
        },
        {
            "id": "146df83d-aae8-4820-ab4e-f70d35583f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7861
        },
        {
            "id": "882cb174-01cb-4a05-a513-81fd6e53be2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7862
        },
        {
            "id": "ddffb464-f980-4ce8-b46b-a03105c7faf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7863
        },
        {
            "id": "eab6177e-3b9d-4551-ac85-ff9a9037bf8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "b1e96ac4-5274-4620-bf5a-d88cb6842a31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 192
        },
        {
            "id": "a77a5b0f-4583-40f1-a588-25a84b2bc02c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 193
        },
        {
            "id": "66385ca7-26f2-422e-a9b1-40875cd748f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 194
        },
        {
            "id": "7fe14460-a79e-4fb6-9d2f-9ced5caa7dc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 195
        },
        {
            "id": "ab91c1d9-e81b-4059-a87c-c79ded000c50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 196
        },
        {
            "id": "e274d9a0-e9a5-4d34-aee6-60fea31a9e60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 197
        },
        {
            "id": "59494b97-ca49-4167-9184-25a9b2e9743e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 198
        },
        {
            "id": "f2576054-3100-4816-a654-e1355aee3ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 256
        },
        {
            "id": "2334376d-8adc-45ba-819f-b6e986c6a5a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 258
        },
        {
            "id": "3b3cc601-70a7-4bff-920c-2fbfb1f10044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 260
        },
        {
            "id": "f746bc8a-bd4c-4e2c-9610-9fa76204005e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 506
        },
        {
            "id": "b9ee773e-21b1-4830-ba8e-d0dacd1536c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7840
        },
        {
            "id": "47fa7621-3324-4864-b0d1-1adc50c12535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7842
        },
        {
            "id": "8e71a89d-70e4-460b-a04c-d8617a6781d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7844
        },
        {
            "id": "caadf9db-319a-472b-81ad-a6ab4f8a4170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7846
        },
        {
            "id": "c9830016-e22f-4394-94c2-2c83c3020f72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7848
        },
        {
            "id": "0c74c740-6d80-43bf-8957-801c51ab4661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7850
        },
        {
            "id": "27e248a8-e17f-4b73-acc8-be41f61bc65f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7852
        },
        {
            "id": "fee07edd-f4f0-4e4d-930c-ecab1c2abde3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7854
        },
        {
            "id": "29e7eacb-4f0b-44b5-8cee-d76c95a48486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7856
        },
        {
            "id": "cef1779c-d81b-440a-b673-1e201b7d374d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7858
        },
        {
            "id": "7acc967d-3f1d-4bd5-8361-c9465f0afcee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7860
        },
        {
            "id": "0328db26-e85a-4cd3-92f3-56319dcee510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7862
        },
        {
            "id": "c6680973-b030-4521-b693-3944b74c115d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 45
        },
        {
            "id": "3d8c4012-adbb-49c1-9e30-81aea5450013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "87a9c7cf-5197-4339-a53c-1547a63a3697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "236abc14-ebc6-4a0b-a96d-26ab53cce59b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "cd0fbbf0-ce0c-4281-9aea-9525e7b4e778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "f5924b8d-d1ec-4ed1-8121-01bb08d7b0ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 99
        },
        {
            "id": "5f7ee7b2-9db4-407a-aeb1-0bce87f7adf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 100
        },
        {
            "id": "210d5521-9676-4956-8c58-b46c5e45aa81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 101
        },
        {
            "id": "e3fc03ca-df9c-4f51-b44c-5cb8582c2d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 102
        },
        {
            "id": "7522d973-a360-48d9-97f5-51b24d99aee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 111
        },
        {
            "id": "6f181ad6-ae25-45b9-80af-82141371e263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 113
        },
        {
            "id": "810647bd-c9b6-44dc-a5cf-7129b7271126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 116
        },
        {
            "id": "6b72143a-4640-4d56-bd7c-aaa548d2ddc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "0ad5dc77-37cf-4969-9284-0e85967cf37e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "405f9c0f-5e07-44ae-848c-aea6f5de0528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "756686d7-da2b-429c-b1eb-8113d9c6c890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 173
        },
        {
            "id": "2bbe5794-3abe-4b6a-9a4e-3637b85e12e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 199
        },
        {
            "id": "4a048937-cab8-4204-be60-e1a125be0d14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 210
        },
        {
            "id": "d79dd471-f034-4d0d-8b8d-3a8a26e28bf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 211
        },
        {
            "id": "f193cb32-7ae0-4b3d-aef4-fdf010adc2d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 212
        },
        {
            "id": "3894f9d2-531c-46a4-97ed-5e16d2177421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 213
        },
        {
            "id": "caa2295f-bc1e-4918-b5ae-b4b1e60ff722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "622f97db-64e6-4492-84d1-6bd90d41aa0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 231
        },
        {
            "id": "269ae67c-6ff2-4fc7-98ff-f626825be9ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 232
        },
        {
            "id": "d973e0ff-a70c-42b1-b964-14c86cf3c427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 233
        },
        {
            "id": "41206571-c135-4c60-a44d-e6afaaae5af6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 234
        },
        {
            "id": "0753e894-b757-4f63-996e-c8c4a4090896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 235
        },
        {
            "id": "44ff3fce-df46-4ee7-956e-bee109f2646e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 238
        },
        {
            "id": "949524ff-7f2e-44dd-9d6a-f100e054c31c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 239
        },
        {
            "id": "6bf9c6d6-48fe-4628-97bf-6ae773a3c2dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 242
        },
        {
            "id": "b36760bf-39f0-4a61-a275-36b50d0dae83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 243
        },
        {
            "id": "1590e955-4c90-4a54-9d9f-ad7693f98a27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 244
        },
        {
            "id": "f3c09d71-ad21-4a9e-9a8d-bb6771629676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 245
        },
        {
            "id": "3f3945a6-2653-47f1-9452-a970da156ba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 246
        },
        {
            "id": "e0b79f03-1a0e-4435-8f97-902a30c68792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 248
        },
        {
            "id": "f5684541-e31a-49da-bc31-23bb979a074f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 253
        },
        {
            "id": "ef8c8832-9461-48c4-addf-3c80ce6bafcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 255
        },
        {
            "id": "3cf95ae3-04a6-47a4-8da2-443cd90e4973",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 262
        },
        {
            "id": "c27f3d97-444c-42df-9b3b-45e6b3382a07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 263
        },
        {
            "id": "d004e40c-9984-4ddb-93ef-4ceaa8479113",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 264
        },
        {
            "id": "1b31b0b1-a9e5-4ef2-a2b8-195e6dc112b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 265
        },
        {
            "id": "c8591b31-52cc-453f-95fb-c296f28700e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 266
        },
        {
            "id": "1fdb3110-321f-428d-922c-3cc9cf5ab186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 267
        },
        {
            "id": "cb504db9-10c8-4849-9644-ad7c763503c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 268
        },
        {
            "id": "aacaafef-9541-4feb-a39f-7cdeafd6dd6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 269
        },
        {
            "id": "2326b338-efd7-4304-8490-910f03aa33b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 275
        },
        {
            "id": "9d363f77-2800-4c7a-bcd3-55b9a85fa1e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 277
        },
        {
            "id": "cf6ecb7a-1c69-40d6-91e7-d9083e18a050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 279
        },
        {
            "id": "c6817257-2f95-4e80-b3d4-ffb0386abef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 281
        },
        {
            "id": "6dcd14c9-a09e-4666-ae1c-9309ada11ab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 283
        },
        {
            "id": "a6fc9128-f7c2-4650-bdd3-11e21365265f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 284
        },
        {
            "id": "4e287db7-1cd1-4f0a-896a-91a22a041485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 286
        },
        {
            "id": "10129f5b-894e-442b-b81b-087601f95c5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 288
        },
        {
            "id": "66f1a669-adf2-44cc-9f74-c4ae29b1728f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 290
        },
        {
            "id": "80c0897a-5904-4670-aa38-605269fa9653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 297
        },
        {
            "id": "e717021f-06bd-4611-92a5-48335a7a3f46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 299
        },
        {
            "id": "1030cf2e-1bce-4104-88a3-cbd89faa5aff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 301
        },
        {
            "id": "7c693628-0390-4efa-8d43-874569628cff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 309
        },
        {
            "id": "2a5815fa-c409-4fef-9709-4901de67cbb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 332
        },
        {
            "id": "5f2e3e6f-b618-46d8-b542-fcd8976959db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 333
        },
        {
            "id": "2efeb9e7-c6e4-4a9a-85ce-ea7b313eb600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 334
        },
        {
            "id": "245dc3a8-6f61-4165-8b8f-02508257fdf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 335
        },
        {
            "id": "b7c5ca6c-cfba-415f-9c15-a2520d176f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 336
        },
        {
            "id": "40190215-412c-4806-8025-efd6d3e4c3ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 337
        },
        {
            "id": "fca8c09a-0827-4dcf-8230-f3a4c28af2fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 338
        },
        {
            "id": "6fb135ab-0f1e-49df-995e-db1e007a8f8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 339
        },
        {
            "id": "270c4251-3b12-4178-8169-201bebf1a68d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 357
        },
        {
            "id": "2f21c2c0-03e5-4110-954a-2506ffae434b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 373
        },
        {
            "id": "745d74bd-b5d4-4f02-9c06-dc453748e336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 375
        },
        {
            "id": "bd427410-b799-40bd-bb10-b799ae3fc1da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 416
        },
        {
            "id": "78b70e2f-9b6c-418a-8ca2-073a5531ae70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 417
        },
        {
            "id": "de0a7553-401f-430e-9df6-98498dcf8eba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 511
        },
        {
            "id": "b61f946c-1b4a-46c0-a5ce-32dc1b0f6ff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 539
        },
        {
            "id": "4c798f3e-a070-46cb-b5c2-02ca7cb46499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7809
        },
        {
            "id": "c632fc53-db3a-4acb-a716-d11d37f55a4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7811
        },
        {
            "id": "1b8ee598-c341-4e74-beed-3a80b410892c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7813
        },
        {
            "id": "47409164-a84c-4427-90eb-2f073d888fe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7865
        },
        {
            "id": "cecf06ea-342c-4b6b-a8a0-a02e0d6ddc9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7867
        },
        {
            "id": "9228e7ae-a1a3-4c89-8587-67e06e005834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7869
        },
        {
            "id": "e264eef4-72e4-4ed2-a0e1-50b8eba8d990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7871
        },
        {
            "id": "3da0920e-28e2-41df-b169-8ae7a5939528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7873
        },
        {
            "id": "83a21532-6e14-4aee-b7c0-c23e4f1f5dc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7875
        },
        {
            "id": "4b0722fb-dbe5-477f-ab65-ab1c1589ee90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7877
        },
        {
            "id": "aff95078-ec78-4073-8f6c-17a9fa7ec9b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7879
        },
        {
            "id": "68cad1f2-5d39-4c6f-b211-fec8d22659af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7884
        },
        {
            "id": "c201632e-db05-4f80-9d37-3dae3dd8d335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7885
        },
        {
            "id": "59ff3bb1-8161-4169-bb3b-3c3bb6461fad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7886
        },
        {
            "id": "18de42f1-93bb-47c3-8729-6e8f4ab6c06d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7887
        },
        {
            "id": "3dd292af-fe35-48b6-90c9-8f972992b54b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7888
        },
        {
            "id": "395074ed-2451-4d32-a6ab-422a6f0f2e5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7889
        },
        {
            "id": "dfcfe0bb-04dc-4c2c-8eea-32ebf90516bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7890
        },
        {
            "id": "def5a87e-1fcf-4e77-91ff-98891f84b599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7891
        },
        {
            "id": "44c899d6-7d91-4368-91ed-db6b08198125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7892
        },
        {
            "id": "889afae6-b0e7-4e81-9ffa-9af2fe666d18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7893
        },
        {
            "id": "070719d8-191f-42a3-85b3-018096295db6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7894
        },
        {
            "id": "7b158b80-7ce7-4b98-aba7-8f39c643a255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7895
        },
        {
            "id": "ef16a159-d247-4ac2-bdc3-eae1835292f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7896
        },
        {
            "id": "6bb4cc95-b0db-4919-9276-05bbde4b12b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7897
        },
        {
            "id": "f07ad6b3-4d30-4bff-9326-4d4ac6d4d750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7898
        },
        {
            "id": "fed36d59-d049-400c-b998-e71bc13fc445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7899
        },
        {
            "id": "6cd9c4ab-795c-45b0-aa34-2e4570654cf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7900
        },
        {
            "id": "4e92de9b-80c0-4081-bdb7-abd909d9b39d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7901
        },
        {
            "id": "21fe5209-cc7c-4e40-8580-01662870cc3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7902
        },
        {
            "id": "69549852-6f1b-466b-910d-48105f2227dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7903
        },
        {
            "id": "92e1f601-2b14-4fc2-af4f-4d4f3dabbe8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7904
        },
        {
            "id": "b9838fe0-2bca-4d86-8487-48e6a6e1f59f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7905
        },
        {
            "id": "197d90c5-3aaa-42f1-a8df-793a477a253b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7906
        },
        {
            "id": "39ca775d-4ecb-4034-966f-87a99025feb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7907
        },
        {
            "id": "09db7dbd-7cd8-4a69-b9f6-dd9d02c4d2c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7923
        },
        {
            "id": "082133a6-c972-4f03-b357-bd07097948a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7925
        },
        {
            "id": "097516c0-2820-414f-8b2a-41bc4334bbb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7927
        },
        {
            "id": "3fbed787-d9df-4176-b64f-373006e24e76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7929
        },
        {
            "id": "73e22eb2-c3b1-438e-af97-cf2fa715b9c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8208
        },
        {
            "id": "8042ea86-f554-48e4-b7c4-6e7bb15cbf16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 64256
        },
        {
            "id": "808791ab-69e2-4b3d-af3a-6f883fca9934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 64257
        },
        {
            "id": "261a60cb-ea7e-41c4-b4f9-90df203639fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 64258
        },
        {
            "id": "a7fab2f1-a808-43e9-ad9c-0df2b79eec32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 64259
        },
        {
            "id": "c4038242-1aff-4497-9cd5-02dff77c54c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 64260
        },
        {
            "id": "0a150348-a6f4-47a3-add5-6c9710ce9f90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "413ebb92-c5f1-4395-9786-02d5c4181b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 74
        },
        {
            "id": "3f0dc823-55a9-487a-a19b-2cfe8b9da70e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "c7897041-9135-4dcf-9b4c-f3b530451362",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "73e16723-8652-42db-9723-d90e9fb3accb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "88c12061-23b1-4bb0-ac1d-6d5c8a0f8a28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "03ebcc06-c68e-4bb5-a3b3-763cc1d2c3b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "c1be45b8-c611-407c-b7bf-f827009696f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "15e13971-21e5-4473-aad0-9a3a30acb731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "e66a37fe-c184-48de-b26a-052b596961ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 116
        },
        {
            "id": "74ee10b0-210f-464e-b167-6156dbbf4cc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "7cc42948-43ff-4edf-9c92-05af84922b3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "b47b00b5-616b-4f3c-b9db-eb05a6758202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "4aa85387-7822-40db-ae4c-49a5d3f36258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 183
        },
        {
            "id": "84375d1b-9978-461a-a61a-90fa27f49b0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 198
        },
        {
            "id": "df99b1d5-218c-42ac-8880-6a34d80de61a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 210
        },
        {
            "id": "96deda23-4a7a-4c09-9af3-ba7379268000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 211
        },
        {
            "id": "60d1c947-8843-46d2-95e7-512e5e7ca5c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 212
        },
        {
            "id": "5bc3baec-b83c-4d51-9987-39171a2b10bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 213
        },
        {
            "id": "29b0f5d0-2d63-4b30-b484-7ee5ffee9f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 214
        },
        {
            "id": "508894cc-d4d6-4329-895a-6b323131351b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 217
        },
        {
            "id": "7b7c26f9-0ac4-42ff-babd-7cddd479ff78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 218
        },
        {
            "id": "5012be49-7aff-4a82-8b69-98e42aaab319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 219
        },
        {
            "id": "03b995d8-a3ae-4974-af14-bcb59e6b3079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 220
        },
        {
            "id": "9d02abfa-5eda-4552-b183-7674c774173e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 221
        },
        {
            "id": "c7b52f5d-cb42-4489-8183-55ca0a9ddab3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 253
        },
        {
            "id": "fb544c2d-3b88-4b8c-9d02-9fe9e33fbe64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 255
        },
        {
            "id": "f1a5b868-6416-412b-a0c6-2e1660762ce4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 284
        },
        {
            "id": "2a7431cb-5f36-4744-b722-7033db9023f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 286
        },
        {
            "id": "00cd6072-ac9c-43d7-8c26-d13c809680a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 288
        },
        {
            "id": "a25ae6fe-95bd-4a97-a33c-b903879baadc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 290
        },
        {
            "id": "c2aa0ee8-3d0e-4909-9447-3510f6600b06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 308
        },
        {
            "id": "e00a95f3-b3bf-44e7-b26a-c3970297c25b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 332
        },
        {
            "id": "59d2329d-baf3-4885-b4a2-4127ed7c8400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 334
        },
        {
            "id": "31abdfde-456b-4a65-bc8c-429d01822e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 336
        },
        {
            "id": "02a23c01-2513-4240-b32f-0d31c06f42c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 338
        },
        {
            "id": "25cbf66f-868c-4731-a3c6-963757ad3162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 356
        },
        {
            "id": "f28c7dc5-6d47-44f4-ac9b-341b199600e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 357
        },
        {
            "id": "854010d8-44db-4579-a6ec-a1d9f9daa905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 360
        },
        {
            "id": "449c7f2d-52a0-45c0-a517-8a2fb18caddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 362
        },
        {
            "id": "8bc94fd5-e258-4c39-93a8-670527282d85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 364
        },
        {
            "id": "f26c8e51-7782-4bf1-b510-fee892452485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 366
        },
        {
            "id": "cd75cd00-04e7-4361-9412-27daadeb8b80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 368
        },
        {
            "id": "7cca0b57-b82b-471c-b292-3981cb0ae76d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 370
        },
        {
            "id": "e3bf8d98-cf07-4268-b9a5-2aa4e2a1b085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 372
        },
        {
            "id": "ed4caf90-4881-40b8-94c0-b8754662b211",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 373
        },
        {
            "id": "380d5283-afbc-4e69-9c43-51df720e7d93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 374
        },
        {
            "id": "39523cff-8071-4fdd-bcd7-3b4f33320c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 375
        },
        {
            "id": "f768e1ea-c389-4fe1-b0fb-15c0228c758a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 376
        },
        {
            "id": "45c9bbc0-4637-4d25-99fc-9f524fc14e06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 416
        },
        {
            "id": "d6f9b563-dd84-4978-97da-4c3ecd973d83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 431
        },
        {
            "id": "67407e9c-33a4-483e-88e1-b76011744d60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 538
        },
        {
            "id": "286d1008-0cd2-4ee3-9b25-0663ab18cfad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 539
        },
        {
            "id": "25b20d77-02f9-49f6-a5e9-a982a4fa3c3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7808
        },
        {
            "id": "f744e777-a48d-49a5-bcc2-fc05616b8ca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7809
        },
        {
            "id": "be7252c2-f825-4ee3-93f0-c034ac78c99d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7810
        },
        {
            "id": "19647723-0447-4787-ab98-449964422ad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7811
        },
        {
            "id": "48cb52d6-f676-4045-8e88-da4bc6ac7ecd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7812
        },
        {
            "id": "c127ae80-a164-47c3-8c63-39efe08207ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7813
        },
        {
            "id": "954ac05f-0708-4701-ae6d-fd5b0ca605d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7884
        },
        {
            "id": "d1fe3ba1-e025-4828-a970-c947641f342c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7886
        },
        {
            "id": "beee668f-e14c-4f98-b9e7-c1f9e6bdd86d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7888
        },
        {
            "id": "cbf3b094-17c7-4347-a447-ad222cd0ac0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7890
        },
        {
            "id": "e83ecf82-e6c1-4444-b7a3-0c841e8af347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7892
        },
        {
            "id": "c56b5238-6586-461a-b83a-c689b4075a53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7894
        },
        {
            "id": "09f702b7-0e7a-41a1-84a5-401ffcb7affa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7896
        },
        {
            "id": "944fc6c0-7eb7-4987-ae2a-9a100b32692a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7898
        },
        {
            "id": "eb7596b4-b7a0-4b5b-9e6d-4c1d4a05a2f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7900
        },
        {
            "id": "b02c9ed2-eeb2-43ac-a645-e5144c83ae2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7902
        },
        {
            "id": "540f089c-7bdb-49b6-bcee-a6b7244a6dbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7904
        },
        {
            "id": "38eee554-5153-4d19-8e00-d78be67273bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7906
        },
        {
            "id": "568d3775-f9b9-484a-893c-6221f2e63371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7908
        },
        {
            "id": "84c57578-bd22-4a24-85ee-ca2361b290fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7910
        },
        {
            "id": "1662fb9e-7d95-4484-8ba0-931fdade6495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7912
        },
        {
            "id": "c59bf61f-378e-43f7-9b66-ec3d74612dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7914
        },
        {
            "id": "32bb1561-276a-44c4-b99c-d6c530e60057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7916
        },
        {
            "id": "3514329e-b44b-452e-b9a0-24ea93d6d08c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7918
        },
        {
            "id": "ac56b89e-995c-4033-8c20-84f2fcaadc5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7920
        },
        {
            "id": "bbbe5a41-8517-42ca-8b28-d8fa8b6ece77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7922
        },
        {
            "id": "e2b677fd-f3a0-476a-8527-c7619e361a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7923
        },
        {
            "id": "39f9c90a-bbee-4499-9d66-7582158949fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7924
        },
        {
            "id": "9f33cb8b-68a5-4e9f-97ca-6ddf32dc63b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7925
        },
        {
            "id": "0ef22243-4fff-4acd-8530-b8d6d9f8d66e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7926
        },
        {
            "id": "cd208798-54b5-4b6d-b1bc-12e79d6b18c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7927
        },
        {
            "id": "68b08c07-7911-47cb-ba45-6e5a63ec863c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7928
        },
        {
            "id": "059b1e0d-3313-43d4-b0e2-860a3ee8b57e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7929
        },
        {
            "id": "cc583418-7fc2-4843-bad0-3ebcfc42b558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8216
        },
        {
            "id": "e4b2b6e2-fce2-4ae0-829a-396c4d90d7be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "77fe4cfa-f531-4ae0-acdd-5fc19589e31f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8220
        },
        {
            "id": "93a3e3fe-ee7b-4bed-814d-08598da4cea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8221
        },
        {
            "id": "8973c228-bdaa-41a4-9ed6-8c570aea8169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8729
        },
        {
            "id": "b4039b11-6d9c-4e93-b4cf-6fd1dd48d406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "8b2cd7ef-462f-42b6-9115-4deb97cf6427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "e8fdeae9-d216-4a45-9475-98393a4fc7bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 86
        },
        {
            "id": "a78c35dd-fe25-4ba8-b85f-dff63d12e5c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "6dafe0eb-39a5-4d28-97be-1a85cf1266aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "5fa43835-63ac-48cb-9357-286e3250d56e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 90
        },
        {
            "id": "ddd5be8a-fd91-4e38-90b9-55ec6bfdb32c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 198
        },
        {
            "id": "b4ff7a9d-beb3-450e-b2a9-cbb074966ae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 221
        },
        {
            "id": "3736f447-6317-48cd-ba1c-c91f86029bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 356
        },
        {
            "id": "e3a7f1dd-6672-4727-abbd-646de9fa6db5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 374
        },
        {
            "id": "51cf6ea3-664c-4d42-9804-b3cc1d7b0860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 376
        },
        {
            "id": "351cf9ec-e55d-4c86-a62e-088d062a62f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 377
        },
        {
            "id": "8f2f5544-e9fb-488e-ba4c-5a3f7fbccaa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 379
        },
        {
            "id": "7e377a32-21de-4d9b-93fc-de322dece303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 381
        },
        {
            "id": "6876488a-6a87-4248-b48f-78ab9e36d0f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 538
        },
        {
            "id": "e20f41ad-8524-4d9c-ac5c-c90a40c619d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7922
        },
        {
            "id": "7c69eecc-c0e1-4a19-a2c3-4ced7dfd9af6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7924
        },
        {
            "id": "97b0885b-6607-49cf-afab-1a0148d246c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7926
        },
        {
            "id": "bddac846-ea4e-426b-a1e3-3e4ca6389a26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7928
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}