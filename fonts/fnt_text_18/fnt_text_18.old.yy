{
    "id": "c8f90ba8-2ba5-4822-980d-93045b1168c9",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_text_18",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Calibri",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "1b4e11d5-338b-4ba5-8cd9-1d7320c17755",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3e3c35fa-cbba-480c-9687-06c83c3bba9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 79,
                "y": 46
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "74f728d1-d180-48f6-beb5-1f67f9e8cae7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 72,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6cfef5a4-ac7c-4a1d-b2eb-33bcb97a13d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 62,
                "y": 46
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "cc251331-c976-4f2b-b271-b4526562d93a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 53,
                "y": 46
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c243601b-aec1-47b2-8960-e741aba25e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 40,
                "y": 46
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8a792216-b089-4ea5-8ed4-130392766301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 28,
                "y": 46
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ead61a6a-25bd-4522-bea2-0b11450d0b89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 24,
                "y": 46
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "99d56256-8c89-45b4-a43a-b397c0cf0a44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 18,
                "y": 46
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4e734a4e-d3fb-419e-b471-cdf332ad52e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 12,
                "y": 46
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "260bbf3b-a6f8-4780-bed0-b104f41e9c10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 83,
                "y": 46
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c0e083e3-75d7-43ec-a8fd-8eecee94e613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a22f7651-284b-4c22-ad04-0526576cf56b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 236,
                "y": 24
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "929f07ed-5c96-4795-b280-5c70a60b24a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 231,
                "y": 24
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2c2fa1ae-875c-4b81-b0b6-42999339b7da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 227,
                "y": 24
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "56865109-746b-4225-a16b-45de38ae1830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 218,
                "y": 24
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c624219c-cf4b-47b7-9c68-0da39c89d091",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 209,
                "y": 24
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ec5ef6b3-4d32-4b26-9729-a778afa3fe17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 201,
                "y": 24
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "265a5d6b-c2b6-4aa9-afe9-61ca9f1c4a90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 192,
                "y": 24
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9c9dbb41-218f-4d72-9d39-564b4c17755b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 183,
                "y": 24
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "78c58bd1-0ac4-4322-81e7-79a026fd0aa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 174,
                "y": 24
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e2f7f409-90c9-450c-91ae-de5cbc24f2da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 241,
                "y": 24
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "38b7c407-a7c2-4a83-8390-8caa4dcca107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 101,
                "y": 46
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ec2913dd-f27a-44f1-aab4-12f357836d25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 193,
                "y": 46
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ef0a0da5-2b21-4f6a-93d3-db932ad78217",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 110,
                "y": 46
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "552d2f15-5ca6-48ce-8c69-e9f14a1b699c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 30,
                "y": 68
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c7bdcfdf-57f6-426c-8fc0-ad570e092986",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 26,
                "y": 68
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b90d8018-5628-4241-ab63-eb8ec65b0dc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 21,
                "y": 68
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3cf40409-feb1-476c-914e-b6effb0e66e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 12,
                "y": 68
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "22391c48-21cc-4a8a-9d2c-8d355503abfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4781feb0-f83a-42fe-94e2-083772f224a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 245,
                "y": 46
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c926bd45-f6b9-4d3d-88ef-9cf8c219f05c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 237,
                "y": 46
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ca83719c-f5d3-4012-b88a-ae4259d06a14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 223,
                "y": 46
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7a5ee46a-a8b3-4e08-ac39-9b05d85aa7d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 211,
                "y": 46
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "fa8cba49-aa6a-4a23-af81-bbfe86224658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 39,
                "y": 68
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3bd2505f-f52a-4e11-977f-2397de880aa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 202,
                "y": 46
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d6552aad-fd7a-44aa-aed6-0d999c360f69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 183,
                "y": 46
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4d7625c4-7a17-4f6f-95bb-d305540dbbe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 175,
                "y": 46
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "36899350-6691-405d-a0cd-82a94f5b2d0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 167,
                "y": 46
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "28518da3-9148-43c8-a00a-736227dbff8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 157,
                "y": 46
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d69d2379-e5ef-44f8-a279-4ca2ba8a766c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 147,
                "y": 46
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e3bada1d-93d1-4e8a-8be4-2b9565d7f79d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 143,
                "y": 46
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "72cf5e0c-ae40-486a-ba08-0270681b3aca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 137,
                "y": 46
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "22aedc58-2315-4a0e-b4ce-c969f977d0b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 127,
                "y": 46
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ea650de8-745a-4b40-a9ae-38cf09b94e28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 119,
                "y": 46
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b14c1016-8fac-4957-9482-df539bb01e2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 20,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 160,
                "y": 24
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2b81408e-3761-4196-8a35-a7dfcac01aa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 90,
                "y": 46
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "db674480-0d16-40b0-a136-05182ede52ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 149,
                "y": 24
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "34efa9d6-1548-4f01-be4d-bca85f41813e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f82e6e70-1868-433b-934f-cc3572532ca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "3710158f-e2f0-41a8-86f2-07093024da4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "44125594-96a4-4503-bd92-f574a4dea5c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1f59ab3c-2620-4b35-aaca-a6b5e5dbab80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d6dd070a-0b5b-4f08-b8b2-8acd4d5639d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "70a84ef5-e7df-4de2-9d51-9f3743a51a92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a283c8c8-02b4-4604-9c2b-777e3b07f3e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 20,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "899effc3-f0aa-4691-ae79-f8a742002abc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a86eb9e0-f6e0-4367-903c-a9453231bc83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "dd87d719-405e-439c-b27c-0898b331e7d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ff5be429-7816-4c5f-ab05-a43f79c9d95d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1b2b22ad-bf7a-48bf-bf19-c6c5d9b552bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "77f2c7f6-f2cf-4a17-b06e-9410f11a334a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f549c84f-1eb5-4b91-931d-b15fc4a82877",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c5aa3b1b-14c2-42e6-b8b9-dc6bd9dfb91e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a5177dd7-aa9a-4e0f-89fe-051e9f0a8b76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "dd000f19-81d5-401f-b15f-f3404ad28282",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "72cc9e00-0212-4f7a-b242-9aef818beead",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a7c5f764-3766-4536-af2a-483cc0f0872f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d6bcdbce-3fd6-4f9c-893b-9be35b4ef096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1e640d5c-c6fa-43c0-ae8b-ca53bfa9cbab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "07ab982f-2703-43bb-aad5-762063edfe68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "565e9da6-7bb5-4647-972a-2d1d210b17f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 51,
                "y": 24
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f6c7dafd-9bd6-4052-a21b-949e7e1812e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "52ee61bc-50b9-4cff-b049-79b5ef1aac15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 132,
                "y": 24
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "850d60a9-28b3-4679-9412-4212db4e281f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 127,
                "y": 24
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "100f9681-a195-45da-8108-bf0472a1ce1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 118,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1bbd8404-f9b3-4ca6-80e7-f7b246cc2deb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 114,
                "y": 24
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f063e82f-3013-4706-852c-2928706869f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 102,
                "y": 24
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "97b77ca6-787f-45cf-bc05-89d29ce95d7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 93,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "6e96b89e-4aa4-4334-9bd8-e255e5ce1618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 84,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7234b6f9-93f6-4538-b5bd-872ad7f82bf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 75,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "780f55c2-2f03-4e20-9d32-a9abfbce5445",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 66,
                "y": 24
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d8f93e42-dcfa-480f-96ed-dcb352af14b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 136,
                "y": 24
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a72baccf-9d22-4c64-b42e-dbcdc61348e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 60,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9d61368d-b8c2-4dd3-a20b-1714b4de26f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 24
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "79eadc66-f7c8-49c7-9074-c79390f82339",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 35,
                "y": 24
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ce41d61c-c7a8-4aed-ae8c-3f57615cd851",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 25,
                "y": 24
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5374754d-81b3-4756-8698-716fffacf6ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 11,
                "y": 24
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4c7de25b-9e49-4a70-bdc1-fca6bbfeaa9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4ef228ac-2ee8-4385-ab66-9ff3cc68af8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "556d90db-aaa0-4998-93aa-f33531ab2708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "cd54a6af-27fd-4085-8b04-6eb803b127e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f4190fb4-7a86-4388-95af-fb2bbe04fa31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 20,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "53e385c0-925e-4d88-a89e-8ba3ed729397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 143,
                "y": 24
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "3d2bc243-78be-4547-8aae-b5091c08249c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 48,
                "y": 68
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "0d29fa91-d198-42a2-a801-71d393806193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "79e73fa2-96e9-4ce2-985f-5b2b6c84da27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "a221c920-10aa-4ab7-bbff-56d72a66247c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "5114f7f4-caeb-4479-8eed-0b04c62a6ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 221
        },
        {
            "id": "2cde75ae-1f96-4b5c-b747-c876f46144c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 356
        },
        {
            "id": "fdd9335a-3fc6-476f-8c59-723e102d4ba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 374
        },
        {
            "id": "d38ab823-00e2-4d7c-8046-d890ab1a16b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 376
        },
        {
            "id": "2231ad3d-be9f-4619-a346-909f2454a526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 538
        },
        {
            "id": "de5aaee6-2a3d-48a2-a212-89f347450fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7922
        },
        {
            "id": "a35f54bb-c7f5-4b06-9726-05ea568daff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7924
        },
        {
            "id": "6a888d82-cf75-4ee4-b7ab-1fd15ba1c7d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7926
        },
        {
            "id": "2a072c59-4d99-4b02-bbac-2d744a8193bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7928
        },
        {
            "id": "02f952ae-38e7-4ba5-a617-3e4ce1929a9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8216
        },
        {
            "id": "fbe239be-fc26-4e96-b0aa-15adbab20be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8220
        },
        {
            "id": "ce751443-ff42-4829-981f-e07d70747455",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "aa3622ce-5300-42c1-be79-69c2c712d9c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "9ab55de0-c231-4a44-ab34-49307a64f8e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "3677791b-a349-4854-92da-99ca9ffd5e60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "1e047081-3b5f-45c1-967c-535c0f8524c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "3805ad4c-85d8-4d72-8a1d-7f045361d6f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "843dfebd-67e6-4f1d-a35c-b1a1054f9aa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "13ac5534-b44b-4878-9df9-e08a4c88052f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "d0204f5f-2eef-49ba-acc6-b184ead00c53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "45800d58-bc22-40fa-926c-6df57d68be3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "d8fdc46c-8e2d-49e5-822f-23d77f9c0052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "6c696688-f032-4e49-ae5d-024428c2d77d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "32908d01-69f7-4223-b1e6-a1139d2c2b0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "d8c3f2eb-9dcf-408e-aec4-e398b0f14700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "fdcef1f3-4918-4fa3-b45c-9b660b24f2a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 308
        },
        {
            "id": "c73e6a51-182c-4bd2-ac84-f2f9613b73e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 506
        },
        {
            "id": "f520745a-d14e-42e6-ac1a-4f0f534a5549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7840
        },
        {
            "id": "4e26872e-9db7-4842-a9a8-20ba6f3d14ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7842
        },
        {
            "id": "9c0af348-f765-412a-8891-033c2fe50d5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7844
        },
        {
            "id": "c892970f-710f-4929-b9e4-5aeb6ae86bf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7846
        },
        {
            "id": "7c9f98f8-c259-4934-b6f3-feba3ee24f61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7848
        },
        {
            "id": "08817b12-a52d-4485-8d4b-f9c3c9afdef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7850
        },
        {
            "id": "eca0a38e-6405-4266-91eb-11207a3fc896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7852
        },
        {
            "id": "1b44448a-806b-4393-b04d-8fba477eabf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7854
        },
        {
            "id": "4cd30c1d-b6f7-4cd1-8a58-34a9d80c84e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7856
        },
        {
            "id": "241f514f-2aad-4f5a-8654-90ac5673e245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7858
        },
        {
            "id": "b606ea8e-3e65-416f-9402-0b85c5acfc48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7860
        },
        {
            "id": "e43535f0-7e6d-4d98-9033-9baddf383cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7862
        },
        {
            "id": "dc111ca8-c791-4c6a-87f6-f01de2dd17fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "09035176-763b-4d13-9b2d-a170765b8b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "58d1d933-2ed6-4a5a-9636-88cdf0c8490c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "7167c188-8297-41f9-a434-bf8f029dbe37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "8b9c73b0-fa0f-43d7-ae53-38c96c9109b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 221
        },
        {
            "id": "613b02d2-b607-455c-81f9-74884a1fcf98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 356
        },
        {
            "id": "3f514cab-3c1f-461f-8780-be8b15ce642b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "b2717b4e-e2c3-4a32-afbe-90bd45770696",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 374
        },
        {
            "id": "3f3591c8-4183-47bd-9223-6f7f14f2dacc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 376
        },
        {
            "id": "2514fbb5-5a8f-4b56-b8ef-cc64f5d97e53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 538
        },
        {
            "id": "e87089f1-7b57-4236-880b-72bd8a6c736f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "2625f8d5-fd95-4c96-9b06-7b2469fa51ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "90e6ef4f-c5aa-444e-8740-a69e33b8fc9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "e4abd4a3-d727-4cbb-91b2-461f20b0a829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7922
        },
        {
            "id": "0ce98020-5c1f-431d-a6a1-0a5663a49be6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7924
        },
        {
            "id": "7c927528-8d9b-4241-94c1-02e4c15b169a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7926
        },
        {
            "id": "06220712-9815-4b67-843b-fc1da813a1fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7928
        },
        {
            "id": "378dace8-331c-4333-90a3-10a59e3ea638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8216
        },
        {
            "id": "eb3caa6e-636f-4c0c-b161-17cf1cb8dac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "ea659cc8-1ff6-464e-94c5-248f28051bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8220
        },
        {
            "id": "83bc523e-2a9f-4cdb-9a8f-289041927139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}