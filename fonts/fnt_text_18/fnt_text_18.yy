{
    "id": "c8f90ba8-2ba5-4822-980d-93045b1168c9",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_text_18",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Calibri",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "91686530-ce3b-4eb4-aa02-51e66970ff15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 29,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3fa51c1f-2945-42d8-ada1-6097fc266575",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 29,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 74,
                "y": 95
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5e21dfd2-a4dd-4ab0-975c-0dda2a9015ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 64,
                "y": 95
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "17a653dd-0e27-4e7b-a083-84f9d4373239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 50,
                "y": 95
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8c5e447e-6a95-4eaa-8971-f86de0ecfd63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 95
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a7f52520-58bd-421c-bc6a-e51209dce94c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 29,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 95
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "0ec7e9b6-af9c-4fc8-90a6-be924d226a6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 29,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 95
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9f17f0f3-6f10-4d02-8cf2-738d19d86373",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 237,
                "y": 64
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "01446a45-0fcc-4141-8a90-d6f615771ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 29,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 230,
                "y": 64
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f3ab90a5-7df8-4caa-bad0-36983996d355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 29,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 223,
                "y": 64
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "27d520a2-baff-4926-9827-84df618ec168",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 29,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 79,
                "y": 95
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "bc2a1d65-4884-42e2-b451-990cdf2612cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 211,
                "y": 64
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "010613b0-3400-4065-bd77-5ba487ada20b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 192,
                "y": 64
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "057131d5-2bb4-49c2-a33a-452a5fdf49d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 29,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 184,
                "y": 64
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d7af6253-69ef-4170-aea2-a2013e67dab3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 178,
                "y": 64
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c78b637e-e819-4993-b559-db1f59329475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 166,
                "y": 64
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "5d142b02-13b9-4c93-9b85-5917a46da0c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 153,
                "y": 64
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "796049d7-251a-4e25-b90d-d893ebf6253b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 29,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 142,
                "y": 64
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b818fadd-d8c2-48d0-800e-6e146211f3c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 130,
                "y": 64
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a04f59a0-99b8-490a-852e-f75119458647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 118,
                "y": 64
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7b7a0397-b611-459b-887d-8b38d3c07fca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 104,
                "y": 64
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "caaadfaa-8c08-4490-a819-2fcf15c971d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 199,
                "y": 64
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "24c6c7b9-c29d-44d3-bb96-259afedf08db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 103,
                "y": 95
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5c64640a-5cf0-4f8a-80f9-d863de7881e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 226,
                "y": 95
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "edbf2c06-495e-4fc6-a383-72034c8653eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 115,
                "y": 95
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8048d78a-8841-45aa-9e24-db505667b772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 98,
                "y": 126
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6693a908-33f0-491d-ab62-5d0136950dcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 29,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 93,
                "y": 126
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e2cff849-377e-4828-a62c-d70d6f567517",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 29,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 87,
                "y": 126
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "98b1446e-dedf-4a0e-9f4f-51b16e3b4213",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 75,
                "y": 126
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "4b7a1ad6-4330-4fa7-9c7d-658e4a5dae04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 63,
                "y": 126
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "17577d93-565d-478d-97a1-eaa711d50880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 51,
                "y": 126
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c32435eb-274a-4479-9b2c-ca006ff9da43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 40,
                "y": 126
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8e3631bf-e108-4e4b-b8bf-80387a04a837",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 29,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 18,
                "y": 126
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "65c5a7be-e52c-44d3-99e4-59d9ab43a288",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 29,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 126
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7f12ec44-dcd2-4478-9db7-16542a571e6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 29,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 110,
                "y": 126
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e33bbdaf-8308-4571-a423-d01f654e22a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 238,
                "y": 95
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d56eecb6-481e-435c-97f3-e1ca7609864e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 29,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 212,
                "y": 95
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e03e669a-ae1d-4398-a823-b73c74a01045",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 29,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 201,
                "y": 95
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9319eb4f-1bdc-4613-99c1-71b3361cba91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 29,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 191,
                "y": 95
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3971e175-d7cc-46b5-84b5-8e1827b9271b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 176,
                "y": 95
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "547490cc-b2e3-4871-8d3e-fcd0465006f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 29,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 163,
                "y": 95
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "72273bab-d397-4041-8f2a-3d2f81fe7223",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 29,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 158,
                "y": 95
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e31201df-9e52-4a8a-a777-d4f2aa7af5e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 150,
                "y": 95
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "dd265e4a-f922-45e5-b169-056d6cac73de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 29,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 137,
                "y": 95
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "8f80f1f2-286e-4796-9370-464c4062eaf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 29,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 127,
                "y": 95
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e8dfac1b-3d0f-4675-bee6-18b50a5ab731",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 29,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 85,
                "y": 64
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7a6842d0-e26b-4962-ac15-496b18ddaecd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 29,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 89,
                "y": 95
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d3e2ecd6-564d-4a2b-9d45-29fc8bdf4a57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 69,
                "y": 64
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "abaa8ed0-cb36-4180-854b-54b7f4fa8fdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 29,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 32,
                "y": 33
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "0bd513f9-685a-47ed-ba8f-7a59f2f1b5f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 33
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fde9a121-37b9-434f-b03f-24634b3f482b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 29,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f65b8102-c212-4cbf-9147-64e54cc808d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "fe40f312-4e61-4196-8b00-7712b6a95387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1bbe4390-1c05-47a4-b9ae-a5799a2d8b14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 29,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "309c0faf-e456-45b5-a23c-fc7ed5db6465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5c15316d-0610-408c-a79b-fc08e61903d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 29,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f3f4307e-48c2-4cfc-9ef3-3a373551674b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "1d026975-7080-4972-8615-52b578724ef9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7abed304-d170-470c-b206-84d5e68b90f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 20,
                "y": 33
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f7031c91-deac-47df-83ab-bb55c0249eb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 29,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ed62db0c-a41a-477f-86e9-7e67c7d19daf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "380fa302-d06d-411c-aea6-1112895ba658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 29,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "3dab2ef9-b5ce-43dd-b4f4-f4926a08a6cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "bc499055-4e24-434c-b186-0c28b375b29f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "33909fbe-c762-4180-8140-dc5d59555975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1eaa7f51-2304-4303-a33e-d3dbc42e241a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "762fffe6-fb2b-4518-a0b3-41a97639c3eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "99988a7b-86ac-4dc5-9b85-ef670d47e1b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "c357c7a9-cad7-4ffb-9659-bc1e2b0b62a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "34043408-78f6-420e-a300-98602b54c4eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "2dab8919-04d4-4294-a899-da66dff5b52b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 44,
                "y": 33
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9cfe129f-0139-49cd-aecf-cdf29b680cf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 169,
                "y": 33
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "8ff22aa8-acd3-4708-af0a-451d864b3420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 54,
                "y": 33
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ad062f99-9dc7-4c77-842c-6a629fc3f9c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 45,
                "y": 64
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b837e144-2869-4c37-af02-3dae1f659860",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 38,
                "y": 64
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3d3cec7f-9a16-4629-bb1e-327fdd2f0115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 64
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "636a60f6-0a46-4a7f-ac89-fc93fc3734f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 21,
                "y": 64
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "16cda1b0-b0a2-4f80-942e-72fdec35f122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 29,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 2,
                "y": 64
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "bd6fee0c-516d-4016-9563-979e50821b4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 230,
                "y": 33
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "82d0b115-2fc1-48cf-bc40-d82701b579dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 217,
                "y": 33
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7712936e-feb0-4350-9cb3-5626d43272e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 204,
                "y": 33
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "57cc09cc-753c-449e-aca5-38eebf055084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 192,
                "y": 33
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e19e5bc0-ea04-4e29-aef1-85c1ae0d576f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 51,
                "y": 64
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "348620a8-4053-4220-95a7-a6e3090d0424",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 182,
                "y": 33
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e06beb35-bd7e-490d-96ef-f459c632030c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 159,
                "y": 33
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "345cd8fd-eb69-46e9-85f6-6e4a77049917",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 147,
                "y": 33
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "000aebe1-de85-4d3c-b134-9df1f369caef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 134,
                "y": 33
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f078951a-b343-4cb3-bef3-a6d7ebf7b71c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 29,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 115,
                "y": 33
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f4873d8b-5d5c-41d9-a334-f635f3edd11b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 102,
                "y": 33
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ba16a228-3c9a-450e-bba2-38dbf6bf3a6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 89,
                "y": 33
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e155ef1d-dd3a-4562-9183-aa5725ae0cfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 79,
                "y": 33
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d055c629-1f51-48c2-aaf5-e1e14cfc3759",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 29,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 71,
                "y": 33
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f7222c89-fdd7-42b2-a504-cbe271f85cf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 29,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 66,
                "y": 33
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9b0dfa55-7269-4695-a85d-9aa0db7cb5cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 29,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 60,
                "y": 64
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "65e14443-1c75-4a9b-80ff-d682ece92069",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 123,
                "y": 126
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "ce5624da-e682-408a-b6e0-cafedcf6c03f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 63
        },
        {
            "id": "93a79276-b19b-4516-86a7-d7c965bd9eb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "b51daf06-ce3a-4c90-8277-c8903ee6bad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "6f0f2056-a105-4081-93ca-3c990d23bdde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "fc4cf052-bcc1-4761-bbd5-fc59a8b1d4e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "7c19619e-841b-461c-a148-22ebee4b90d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 116
        },
        {
            "id": "523b874c-2153-4452-af3b-453786e99cff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "4c5c0c2a-70b9-440a-8652-cd89136d0a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "753eba1c-3f98-42ed-b9cb-0994b89780dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 221
        },
        {
            "id": "87b8fa3f-4f86-4be0-9604-57399eade81e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 253
        },
        {
            "id": "230dbfb4-2beb-497d-a51d-cb426170f65d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 255
        },
        {
            "id": "1a7c9fca-0caf-484a-88d4-b37b45616ebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 356
        },
        {
            "id": "bdc0c007-4dc9-4b7e-80ff-61443545e1c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 357
        },
        {
            "id": "24901b33-6507-4908-8beb-30abe862648b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 372
        },
        {
            "id": "5312649a-543c-4688-83da-a595523a76b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 374
        },
        {
            "id": "a5003f2b-b6e4-41c1-9e19-30ac95d46797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 375
        },
        {
            "id": "32d87c3f-b259-47a0-95df-140bbdf26cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 376
        },
        {
            "id": "a8dec6b0-562f-4341-b636-ce9de128b50a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 538
        },
        {
            "id": "0272b0b8-28d1-40ec-9e9c-e6c5fd71bc33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 539
        },
        {
            "id": "9dc73a7e-32db-4579-8c56-6e927edd0b51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7808
        },
        {
            "id": "608bb155-b0ad-4283-9456-cb133cc6bc89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7810
        },
        {
            "id": "ae9b495d-bb1a-48ab-80ee-d18cefa6cf33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7812
        },
        {
            "id": "60d997e3-f4ae-4e8c-9c72-6cde0d751277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7922
        },
        {
            "id": "da628b5e-23d8-4ec0-8fe1-7f8008bb644f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7923
        },
        {
            "id": "ea45427e-13dc-4870-b4b2-0d9e8cc0c2bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7924
        },
        {
            "id": "7a674a94-43c0-4c42-9ead-f2299992e926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7925
        },
        {
            "id": "fef02f7e-54fa-47b1-9dbd-4d9a98a38f16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7926
        },
        {
            "id": "9637fcb1-f2b5-45b4-a2aa-26b7bb2080ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7927
        },
        {
            "id": "b8de584a-f420-40d0-805a-3c9d4d2de302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7928
        },
        {
            "id": "8d90ef95-e4e6-4a31-913b-61dfb2c1a729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7929
        },
        {
            "id": "28099319-cb33-4c0b-a36a-1fcad28fc4cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8216
        },
        {
            "id": "23019004-21d6-4b2f-b92e-0750863ea4ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "5b777442-34e6-4144-9ee0-2e62dc146d52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8220
        },
        {
            "id": "c50319ce-2380-4408-827a-876db67b87d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "debb21fa-00d5-462c-ad2f-e8e10f3a6f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "5f7b349b-a26d-4275-a06a-be101cfbac43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 88
        },
        {
            "id": "6810006a-c912-4b07-82c4-fb279d9f2432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "0f450c40-1ed1-4402-8e85-7c8b17f1c9e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 221
        },
        {
            "id": "effe79c7-9c48-4c7c-aeac-82474109b53a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 356
        },
        {
            "id": "98c6e8dd-79b9-421e-9f6e-d71af4508351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 374
        },
        {
            "id": "664fd58e-554c-45bd-b81a-616488506507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 376
        },
        {
            "id": "268a15c2-0349-4505-8bf3-5db44ad91187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 538
        },
        {
            "id": "d70ed8af-4aa0-4c50-8d85-de56c35e34fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7922
        },
        {
            "id": "949b4055-c910-4167-b771-1482656d9483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7924
        },
        {
            "id": "5865c9a9-66b8-43ed-ba04-069e3257f39a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7926
        },
        {
            "id": "ccd14edd-4fc6-4600-b0a3-2b02cc7566cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7928
        },
        {
            "id": "96e4d565-8d84-474b-9ef7-ac28dbf5bcc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 8217
        },
        {
            "id": "3de1d489-6b10-462f-ba8c-6be94ff95998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 8221
        },
        {
            "id": "8ad48c40-03e0-41f7-9431-3331f196320f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "1b1d5502-96c3-4ff0-818d-9cd4d69c1db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 46
        },
        {
            "id": "0365054d-45ae-4315-b029-3b7c27415ba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "9f2717d0-64ce-4172-bc68-480b1aeabea6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "f4a3269b-1022-421e-8b22-703166db936f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "308ce0db-0427-4ee4-8475-72ac2a3529a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 192
        },
        {
            "id": "d7f9cbca-24b5-4058-92f4-5c41eb18ed0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 193
        },
        {
            "id": "6ea8f00e-27c6-42f1-98ec-e55a73844059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 194
        },
        {
            "id": "e757eab7-da9b-4842-a7d2-8720a960f6d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 195
        },
        {
            "id": "6f0c499d-b1c8-45ce-a4b9-9e13ff2999cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 196
        },
        {
            "id": "c28bef8e-20d9-450b-a080-d80423e0f8c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 197
        },
        {
            "id": "5843a77b-7a12-4f40-a3f6-ee4fa655fc74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 221
        },
        {
            "id": "6b5e07f0-300e-4d17-9f9b-a265980b6600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 256
        },
        {
            "id": "6a48dc77-3c14-4148-a699-ea781a9ade2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 258
        },
        {
            "id": "10181aab-c9fe-4021-83c8-8504d78d0da4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 260
        },
        {
            "id": "192727c5-9328-4fd0-b472-524026adc4aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 374
        },
        {
            "id": "ea290a49-6989-41f1-8c06-4600485a3142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 376
        },
        {
            "id": "8d08e9ba-256a-450a-8484-7949f52361d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 506
        },
        {
            "id": "97c50307-deb1-4a66-be76-3166723c381e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7840
        },
        {
            "id": "093ef729-83a5-4a21-be81-03eca7322467",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7842
        },
        {
            "id": "680de1f7-bd80-429f-9254-b3b584c823e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7844
        },
        {
            "id": "ef3556ff-1c23-44c2-986b-5999e64186d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7846
        },
        {
            "id": "a726ccdd-e960-48df-9c12-199eacbe6260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7848
        },
        {
            "id": "895e886f-f4a1-4083-8821-6bd32c47d201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7850
        },
        {
            "id": "535c16ca-d7de-43df-9dba-1e5029fa188a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7852
        },
        {
            "id": "daf392d6-d900-48e8-bc04-b3215ec35cf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7854
        },
        {
            "id": "b30cceb3-3a9a-428b-84bc-80363cfefec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7856
        },
        {
            "id": "cd48263f-6bef-4522-ab93-0b4eedb92a2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7858
        },
        {
            "id": "e48fbd3a-c9e0-4a1a-ab01-969f564969f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7860
        },
        {
            "id": "e80e454f-f9c5-4cc7-bf15-855aa9614e81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7862
        },
        {
            "id": "e74cd0bf-73ce-4c2c-8fe4-aa9fb4b5d9f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7922
        },
        {
            "id": "4a5f1f1e-4f5f-4473-a4ae-251336eb4640",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7924
        },
        {
            "id": "62526c26-c924-44bb-b708-c3f20fa26b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7926
        },
        {
            "id": "8b4484e9-7b47-4a6f-b7b2-b177180694ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7928
        },
        {
            "id": "66cb066b-cf6f-48fc-a931-7f72ef09390b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 102
        },
        {
            "id": "2b18c8ef-5a02-4fd7-851e-2905d8a6d7fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 118
        },
        {
            "id": "88c67c13-256c-420b-b344-206cbfc7594b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 121
        },
        {
            "id": "b3b0d766-950a-42a4-875a-2252a0e1c49e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 253
        },
        {
            "id": "9dff45ff-19c2-42ec-a5c7-b85faba54e2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 255
        },
        {
            "id": "0759ec98-d72a-4820-bd3e-e25afe282863",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 375
        },
        {
            "id": "54a96e43-b293-49c4-a548-d99c1bbc9c16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7923
        },
        {
            "id": "6e1d213a-65ad-49f8-8d38-a52263ff27bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7925
        },
        {
            "id": "a9353afb-448a-4830-86e9-f3c1a00a4f09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7927
        },
        {
            "id": "81c3c266-a459-4e23-81de-67b1e4b0c04c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7929
        },
        {
            "id": "1fddbb8e-3ce6-4547-b0c7-3ac832ccd4ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64256
        },
        {
            "id": "3f181f79-844a-4d4b-9550-b159026e3683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64257
        },
        {
            "id": "2d10b8f3-6347-41ed-810e-2637f900513c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64258
        },
        {
            "id": "346b893b-db57-4a9f-a33b-b760b968d09a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64259
        },
        {
            "id": "fee7aac7-f7d9-498e-8497-8ccc9f2d5c21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64260
        },
        {
            "id": "6d675549-aad2-493d-b6a6-94756630b723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "da6348c5-6fb4-490c-8a38-68b33fcac206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "69606951-b5a0-4b71-b49c-76fd78904cac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 47
        },
        {
            "id": "be4e35be-4524-4205-bf13-f28a12fcabc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "e9e07a76-9212-4f7d-9c6a-b3aafe075994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "375eb0c6-1f47-4c14-bfc5-45f9f289ed17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "6e28fbb2-471b-4e55-8655-f550da907b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "dbaa2090-919e-40a3-b0da-1e1cf09365fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "356e0b2e-d2e4-4b52-a970-c4e108657270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "bf6577f9-b93f-4409-b147-0a0d5a826857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "d2502ac1-fb47-4e80-b132-d80868e20fd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "21cc7285-16d9-4b29-a9ff-b12407e98a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "f2c72c13-6d0f-4c6e-901f-33fe7900fdee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "8a8ab627-9ffd-4794-8ea6-e46baf462b24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 224
        },
        {
            "id": "291f8ae9-0678-4061-9af4-a5a88869f646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 225
        },
        {
            "id": "ff006679-d9ec-4191-a86d-b42c15297619",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 226
        },
        {
            "id": "a9d4d6ca-783e-499b-8307-f947603d5052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 227
        },
        {
            "id": "6029b0e0-3b3e-49a8-bb62-4cad8d6ca7b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 228
        },
        {
            "id": "01a8e3c0-5c20-4d60-833b-90f04fd87e57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 229
        },
        {
            "id": "65771eba-7d54-4d68-bb56-7a77cae99b48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 230
        },
        {
            "id": "e410251e-1ea6-4425-acad-d9052a1dd3d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "4efe6117-1685-4aa1-8441-398c40b34d6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 257
        },
        {
            "id": "902d4514-f800-4e51-abf2-83b24fad2632",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "278f83ba-d7fd-466b-b816-d33de3f1b546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 259
        },
        {
            "id": "f40d7aae-305a-432d-aafd-317075db62b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "4bc20c55-1aab-427c-83d9-e23bdc38acc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 297
        },
        {
            "id": "1804df3b-d7ab-41b7-ac86-bfa46bb3dce5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 308
        },
        {
            "id": "403ff106-06b1-4cdb-ba76-d00ffe49f050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 506
        },
        {
            "id": "258c7763-3b58-4c61-bb35-78ce1143b228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 507
        },
        {
            "id": "f8ca7270-93df-4945-8d3e-68153c1247a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7840
        },
        {
            "id": "54373109-2862-4fa6-b00d-77e00888a751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7841
        },
        {
            "id": "34c8b60d-7352-420b-b4ba-02afe4f78caf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7842
        },
        {
            "id": "c56b88ac-43d4-4287-bd14-c9787c0b660e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7843
        },
        {
            "id": "8029e278-a287-46b7-b425-8e266bdca976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7844
        },
        {
            "id": "874ece5d-09ff-439c-906f-6367fe03aca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7845
        },
        {
            "id": "8e1d0e24-9b5a-478e-b443-689112e42a7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7846
        },
        {
            "id": "7f98f94c-e407-4432-afd0-9a1bbf3d0318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7847
        },
        {
            "id": "2bab61dd-e9a0-4bc0-934b-94f6a1a282ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7848
        },
        {
            "id": "00e2adcb-25d2-4211-a84f-8c0d3876a15d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7849
        },
        {
            "id": "6946772e-703d-4fa8-b599-6bbfa05759f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7850
        },
        {
            "id": "fb8564ab-531f-4f67-9224-3c71af267fd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7851
        },
        {
            "id": "caae3dd9-bc17-475f-a1f1-1d2557b733f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7852
        },
        {
            "id": "1414d98d-ccf9-4347-a419-693670213aa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7853
        },
        {
            "id": "b8f0ee95-0a3b-493a-9f57-d2d5075b9477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7854
        },
        {
            "id": "3976ce05-12bb-4c52-8d2e-2b07862c3b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7855
        },
        {
            "id": "18737fe4-edee-4f67-8aea-25185f8d8a00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7856
        },
        {
            "id": "69642550-7841-4d77-9668-ab32aa8cdcaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7857
        },
        {
            "id": "de34c5f8-c588-4f5a-9c1b-7c2c514a531d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7858
        },
        {
            "id": "632657b7-dca6-4c41-a1a4-a4163c1cea0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7859
        },
        {
            "id": "0968635c-065c-43c7-af9d-fed14c3c055d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7860
        },
        {
            "id": "11b74ef8-1a40-45e3-ac4f-4736d33f7366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7861
        },
        {
            "id": "83c991db-6109-4421-b18c-39b3f34e12c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7862
        },
        {
            "id": "0571430d-dac3-4587-a213-035cd101f60b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7863
        },
        {
            "id": "6f05390f-f7bf-4b3f-8ee3-172f3bfb0fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "a0172cf5-7ad1-43ab-bb73-abf016361c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 192
        },
        {
            "id": "cec51ae3-7d26-412e-933d-bb602d340284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 193
        },
        {
            "id": "03743654-b62e-4b3b-8da7-bf6d139cadfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 194
        },
        {
            "id": "35793bdf-a0f0-4050-bc18-de351d73422f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 195
        },
        {
            "id": "862f1816-08ec-4ea1-a829-0ce720d2c7d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 196
        },
        {
            "id": "0d7c0130-f0a2-4895-a598-17786ba91170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 197
        },
        {
            "id": "6fadb5e1-2424-43ae-a43c-982e1d83d418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 256
        },
        {
            "id": "f8839fbc-0e2e-4deb-81e4-8a1cb52ff0ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 258
        },
        {
            "id": "7055e324-6966-424f-9c4b-513197dc42d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 260
        },
        {
            "id": "f6968766-6499-4af5-8a51-f3126aa170d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 506
        },
        {
            "id": "21cf50e7-2e2b-4760-806d-f50821a7b338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7840
        },
        {
            "id": "74516c57-cdda-4d21-a9c5-bff46588455c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7842
        },
        {
            "id": "d8e16bc0-595f-4990-a432-2539671c7bc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7844
        },
        {
            "id": "7d461c04-e7d5-4ee0-93e6-f86d291b3cc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7846
        },
        {
            "id": "9a979bf9-ea79-45f5-8535-1d075489f4ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7848
        },
        {
            "id": "ae0f59bd-1a52-4216-b0b0-1a845227f59d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7850
        },
        {
            "id": "4f465505-ccb7-4bce-b2dc-4045d00bbc25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7852
        },
        {
            "id": "0e877d3c-6a89-4606-ba8f-fd750b290a8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7854
        },
        {
            "id": "8d2b59b0-b877-4d68-84b3-d75dcdad4561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7856
        },
        {
            "id": "b2b0e006-b129-4450-94fb-adbc0ecc449a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7858
        },
        {
            "id": "a4291efa-c5e3-417f-8bb1-d1f08d7314b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7860
        },
        {
            "id": "8de08784-1a31-4f95-ba0c-d83f304810d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7862
        },
        {
            "id": "a27434d2-ffbc-4d83-b26f-d9f50d243143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "44f735f1-3d36-4267-a92b-e9d840ac3e68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "d3b8c6fc-3616-4828-8117-5e76d528c6f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "5ef5ba80-4019-4b75-8486-1fb9bf3fbaa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "a8de2a40-68b9-4e50-9e64-490a5131fd4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 116
        },
        {
            "id": "a577253a-81af-4a05-9760-61eb48ce5278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "7d101127-c0e5-49db-881d-916113c7b8ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "49a60cf4-0af1-4928-9af3-59fa22fb6d28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "c2104565-7cef-4c0a-b1a3-3dc7f4bfe6cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 199
        },
        {
            "id": "e19c2e9a-570d-46cb-b127-4c55db67bfc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 210
        },
        {
            "id": "5e00d2ab-ed34-4c99-b572-b7d6c6edfefb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 211
        },
        {
            "id": "62d3c6d0-6ce8-49e9-a3e1-fc6a64479e4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 212
        },
        {
            "id": "ef0ec2e0-130a-4c8b-92c4-3eff2e257607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 213
        },
        {
            "id": "9d5cbd86-149a-4196-a9d3-3a498d3665ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "0986fc52-44f3-4245-8cf9-8f684d96f961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 238
        },
        {
            "id": "c4905e9b-2be7-4cbc-9dcc-f69fcffb9468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 239
        },
        {
            "id": "dc0a95a7-78e8-4fea-8836-581e03f0dd80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 253
        },
        {
            "id": "e9c88ea5-1389-486d-916d-9f4b84b81178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 255
        },
        {
            "id": "b55f8e1d-0a87-45c9-95fa-def1df569bed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 262
        },
        {
            "id": "053858eb-7f49-4683-a748-4af639b8d4c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 264
        },
        {
            "id": "e4509c81-414f-4a87-a5d4-31dddec7e38a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 266
        },
        {
            "id": "bfd7db55-ab3a-4af8-b68d-47b38a7ad540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 268
        },
        {
            "id": "1a877339-259e-47bd-ad35-cc95244474d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 284
        },
        {
            "id": "fbfe85b8-7778-404a-bee1-09f53494c588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 286
        },
        {
            "id": "1df5feeb-5448-4d75-a73a-050850b9f00d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 288
        },
        {
            "id": "27de9a8f-0bee-49d0-b120-8dabd453c284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 290
        },
        {
            "id": "c11f00e3-806b-4ba9-9d2d-e703c3c8bf58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 297
        },
        {
            "id": "71d0da30-e78b-403f-bd95-1a9d11f62dd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 299
        },
        {
            "id": "f0a39c7f-5a79-487e-ad23-51b5635acd2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 301
        },
        {
            "id": "e43d7751-96a6-42eb-917a-70988edf31bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 332
        },
        {
            "id": "0e05ba3b-5ec4-4d4d-8a14-fbab8aa4e989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 334
        },
        {
            "id": "c48a9e4e-d194-4eed-9ec8-468ae47c8087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 336
        },
        {
            "id": "5cffc3e0-c44f-482b-a8cc-516f7f7d3994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 338
        },
        {
            "id": "363d20cb-a380-4cc0-a786-ef7c8ccd8455",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 357
        },
        {
            "id": "15de35eb-e0c2-4055-aa3d-3a140dbf1243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 373
        },
        {
            "id": "8eb64df8-c2ea-499b-8dd5-edc97ec753f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 375
        },
        {
            "id": "7c5e9077-9381-45b0-a175-66faac236504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 416
        },
        {
            "id": "cc0eb0c7-a423-46c0-a262-f8faf3fb1d9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 539
        },
        {
            "id": "e61d7fb8-c057-4a5e-bf8b-0815c8c1b216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7809
        },
        {
            "id": "61afffd7-ccf2-40f6-b131-ba50a318a6d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7811
        },
        {
            "id": "24b11919-78a5-41cc-9cdd-b968156936d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7813
        },
        {
            "id": "6826be44-e381-4fdf-bf1b-90880d647b59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7884
        },
        {
            "id": "a75f3c4c-d3ba-43ec-9a62-ccc82d55cc55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7886
        },
        {
            "id": "32ac98d5-7e88-4ac2-b14c-4c69ac96f1ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7888
        },
        {
            "id": "c1a0d471-7641-4199-bc5d-3ac0da86045d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7890
        },
        {
            "id": "6b76c1f3-2694-4d9d-8baf-eed7455458b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7892
        },
        {
            "id": "58a974a5-b29c-40ec-9e1a-1d874b6c7a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7894
        },
        {
            "id": "02f29b7c-2036-4612-ad5b-bb63e1ff95f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7896
        },
        {
            "id": "09df2032-6e38-45f4-9313-5eb85cd90914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7898
        },
        {
            "id": "5b3b1135-68ff-4a26-bec0-0377662d1e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7900
        },
        {
            "id": "23f0e6ce-aba3-46f8-9719-7736b5d95298",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7902
        },
        {
            "id": "dddcff55-fb8a-4063-bb1b-12b8cb36bf6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7904
        },
        {
            "id": "ff38abe0-f3b5-46f6-a98f-12c3f6f6e71e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7906
        },
        {
            "id": "03a9d654-b75d-405c-a3c6-91dcd7feab2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7923
        },
        {
            "id": "2e3242e9-d627-4d8e-9179-da1ae720f272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7925
        },
        {
            "id": "9bc07802-1ecc-46a8-93a8-6f1b25bfec53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7927
        },
        {
            "id": "92a676ca-a930-4d04-b233-409aeda06c87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7929
        },
        {
            "id": "db9fb801-6f7e-49a3-a13a-f231e2bbe0cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "5513a875-3d4d-40c6-9ac9-af12bb8a304c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "01ea2e38-6387-4ee2-9d5e-98ad1513c792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "4f17cbf1-40a5-4f6a-94ca-b2bb36a38d06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "3c06fa95-7faf-406c-af0f-669d531a371a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "a5cdab07-7200-4b81-a1b6-5d4dcc1344a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "e42ddb32-54c2-489e-8e9c-d847555df854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "e0a968d8-28bb-441b-be28-6cebbfad8565",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "ec1065c2-1b01-413d-aa7e-5bf51870ac28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 183
        },
        {
            "id": "015023ab-bd35-404c-91f4-5e3c481e8ee4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 198
        },
        {
            "id": "1052f638-cf34-4eec-816c-8c55047cc633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 217
        },
        {
            "id": "35f2b0e7-c16a-49e5-a6a6-5e6b064e24b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 218
        },
        {
            "id": "d1b8d6f2-9f43-437d-a547-bc73eb77daa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 219
        },
        {
            "id": "44fd50c5-5a28-40f9-931e-4388fca88562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 220
        },
        {
            "id": "4c55087c-f571-4316-b722-49b6be14124b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 221
        },
        {
            "id": "68fccacf-74dd-4c43-b14a-75a7b8e875cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 253
        },
        {
            "id": "e52a8765-dc45-4552-b4b2-080a761367bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 255
        },
        {
            "id": "75749161-0a88-4fa2-9f95-f543f6793c8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 356
        },
        {
            "id": "6f02f26f-77b7-477c-8a29-1114dc6ed98b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 360
        },
        {
            "id": "ba997a09-1550-408f-ae95-5669364f4bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 362
        },
        {
            "id": "0ec37018-1b02-488b-ac98-5178cc9ce927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 364
        },
        {
            "id": "55431332-bc12-4a5a-b194-99df8615ce53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 366
        },
        {
            "id": "ba48fa76-89e7-4c01-9ee7-7829a486c252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 368
        },
        {
            "id": "8b1094d4-d580-4cfc-83b7-5a1e374079e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 370
        },
        {
            "id": "37222178-cdd4-430c-a3de-d11e4ce1c0c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "cc94520c-4559-4860-a091-9bea184cf4fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 373
        },
        {
            "id": "be5badcc-9de6-43e5-8616-baa3e547908a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 374
        },
        {
            "id": "a93b0016-ab18-4652-bd74-971efb04f7aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 375
        },
        {
            "id": "0f70efc1-3e20-4a40-b7d9-123ad79e49d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 376
        },
        {
            "id": "ba5dc436-2acd-4735-82f7-1c72ae7e7bef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 431
        },
        {
            "id": "e7a9684e-1e6a-4cd9-a8a5-14931b50fafe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 538
        },
        {
            "id": "3e044e79-207b-49b7-a383-ffb2a909849c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "d3027d2d-d8ad-407d-ac45-746b24f740f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7809
        },
        {
            "id": "739f71d6-43e8-47b3-9a13-cfe541dc2e09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "4e3caee9-9225-4929-af34-977ae4cef459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7811
        },
        {
            "id": "08ae9c4f-0c83-49d6-b669-d5864b9bce39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "1ca2884c-86fb-4d0f-8d2f-13d91a60f2de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7813
        },
        {
            "id": "86da7858-bb03-49c6-82d1-4c89817b6e8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7908
        },
        {
            "id": "32a55206-49e8-4307-b1d5-4e5e34d37fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7910
        },
        {
            "id": "a9034545-01b8-4b13-a4cc-eae6a079b86c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7912
        },
        {
            "id": "5fd63425-408a-492b-b6a1-4cf7f42ac683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7914
        },
        {
            "id": "663756b4-2b12-4255-b24f-d6e77852bf39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7916
        },
        {
            "id": "ed8926aa-5686-42f3-9c38-545c09e073e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7918
        },
        {
            "id": "0c58d3e5-8522-4e78-b080-9b83cb4fd116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7920
        },
        {
            "id": "939558fd-0592-47e5-b99d-8905abf2d641",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7922
        },
        {
            "id": "647d81b5-c9e9-48d4-a9f9-a1300332e0d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7923
        },
        {
            "id": "1e97bafa-cdea-4910-ae03-ffabc242b1cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7924
        },
        {
            "id": "50e0399f-7262-4a1d-a516-97f091e0630e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7925
        },
        {
            "id": "553426d9-043f-4c16-b2d6-fbf4aae6dd7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7926
        },
        {
            "id": "37df153e-5e84-415f-852a-ce151abb90a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7927
        },
        {
            "id": "aa39bdaf-32c4-4e59-92e8-5e13960f6025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7928
        },
        {
            "id": "feb8b932-fbb7-4ecc-bdfd-465016ce8a40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7929
        },
        {
            "id": "4f4dd16b-5298-4329-bb11-fd0a64566d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8216
        },
        {
            "id": "fa6c9c3d-0023-4e9e-90df-154d0cf3b1ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "90e94299-75cb-41c5-8695-c56407d02146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8220
        },
        {
            "id": "6b9a5ca3-85b7-4dbe-9473-91a92028b4f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8221
        },
        {
            "id": "10d056e7-be65-490a-962c-c0ffeb781430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8729
        },
        {
            "id": "9f9d700a-bc9f-461e-81be-ab0260130b85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "047f2ff8-f328-4038-9714-289057ddf977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "43930cc4-4310-4e48-8a30-0f37dce2b3c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "fef5c439-9b10-477f-a9ab-09509021297b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "d63c05ac-00b4-4ea8-ba65-f6f2a7dda52f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 198
        },
        {
            "id": "e753f61b-dc64-4611-8ca8-b81adcc1751f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 221
        },
        {
            "id": "ffc16e44-e3f5-4d06-a9a5-c24fe62fb9ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 356
        },
        {
            "id": "979c9dd0-eb48-49a8-bc19-0dfff10c684e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 374
        },
        {
            "id": "2209101f-81bb-4f47-a97a-8dc18df14c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 376
        },
        {
            "id": "e7fcbe0c-750f-4936-83fa-9ad651f9c2a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 538
        },
        {
            "id": "49edf7c2-a890-4783-b992-5e365065b92f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7922
        },
        {
            "id": "ef35e54c-5233-43c3-8157-fcc8a9b16a08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7924
        },
        {
            "id": "561b415f-977f-43ff-a664-9dda43ce9b8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7926
        },
        {
            "id": "58917781-a53e-4c92-88d4-303ce7a8e8ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7928
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 18,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}