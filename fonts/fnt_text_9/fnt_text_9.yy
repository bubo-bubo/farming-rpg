{
    "id": "5b09e272-e0d9-4f6c-9f92-2568593255e5",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_text_9",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Calibri",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "236e9246-df3f-4b6b-a0af-da39ce1e4aae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e7da8328-0635-4f23-bcec-680257447bec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 105,
                "y": 53
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "68386154-3c24-4e69-a51e-1ca967eb26e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 100,
                "y": 53
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b1d228c2-c015-449e-8f93-105f43185544",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 92,
                "y": 53
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9483d66b-8c0c-4b92-a15d-cbda05ff2c94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 85,
                "y": 53
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c3b67f75-8c90-431d-bb45-88c603988de0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 75,
                "y": 53
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c4f2f27c-93a3-4cd2-9cfa-7ed90f0a291e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 65,
                "y": 53
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4c6fab42-bc93-455b-b17a-f0a8a79094fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 62,
                "y": 53
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c29b0698-d12d-4300-8bb5-2596150223bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 57,
                "y": 53
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6fb9b646-ec2b-43c0-9d04-104f0a05e85e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 52,
                "y": 53
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a2868a10-b861-4f18-a9dc-8bccea0e3671",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 109,
                "y": 53
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e2964f70-6ebb-407d-b7a3-7ba25db0321a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 45,
                "y": 53
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "62ebb1c6-32b5-4410-8676-0bc311c5b664",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 33,
                "y": 53
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "277a80dc-5d21-4e98-987a-bf846398e6ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 28,
                "y": 53
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f386fb1c-4a63-410c-a976-abeecadc7605",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 24,
                "y": 53
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9d2ae930-a655-487c-bf0f-7cb72d05b371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 17,
                "y": 53
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "489d55df-59ae-494d-b1fd-ff6a95fbfba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 10,
                "y": 53
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "1ce39859-a82f-4e77-8de0-f4d29b121451",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b7b014b3-001b-48f2-9476-8d6967a3db26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 113,
                "y": 36
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b6cc20e3-60a6-40c3-b579-76e99645dd04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 106,
                "y": 36
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a6695e93-1626-4970-bf5f-c66553faf819",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 98,
                "y": 36
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "43fe25e6-1c48-4ad9-9426-e3af0d672a5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 38,
                "y": 53
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "64ad2fa0-08e2-4ab3-980e-58d7d5a80995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 116,
                "y": 53
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0d74c9ef-09df-43a2-b987-917d49e56ff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "982ec8ed-2743-426c-9565-729ed6a2c791",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 70
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "bdac1cdc-ec89-43c8-a786-551cb1dd57f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 39,
                "y": 87
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "63972b9a-2957-45f5-8553-dda52b388a24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 35,
                "y": 87
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c5b7a0b3-22f2-4710-936b-ca82682d9240",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 15,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 30,
                "y": 87
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ebe23cda-6417-45df-a555-f6223e83576f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 87
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a0903bbb-d063-4ce0-83ee-fb91df2bef5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 87
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b2684b0c-33f8-477f-856e-d4df77d19415",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 87
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "5bfa86f0-76bb-4968-9add-dc132e075feb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 87
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "040e89b9-4571-4880-ab10-4976ce963d87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 115,
                "y": 70
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "949f9400-5a49-4780-9e08-ac17154ff455",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 106,
                "y": 70
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ffec587f-9224-4dc6-a5a3-64b4d58ff5d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 98,
                "y": 70
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "faec0021-ee0f-4180-b8ad-4eeed06b361d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 91,
                "y": 70
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "3b5ba00e-74c6-44b9-892b-adfc26dcc911",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 82,
                "y": 70
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2e221818-a7dc-4a4a-95f6-d41d920da2b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 75,
                "y": 70
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "35ba530d-46fc-4dc2-8a53-bad9643a19b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 68,
                "y": 70
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "dbce0a83-4e83-43f8-ae0a-080827c5c6dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 59,
                "y": 70
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e74c8990-4018-424f-a8e5-58e5656b34e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 50,
                "y": 70
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a1c6d54c-23b8-4a3e-8def-3108cc87f062",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 46,
                "y": 70
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "004ba8b8-d052-4d92-a846-a1670c6278ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 41,
                "y": 70
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "89047466-bfb7-4d98-8b7c-bcffa3253d0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 33,
                "y": 70
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a23d62a6-1f4f-4b63-be8a-d8a5ccb04721",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 27,
                "y": 70
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "23ffb540-dfcc-4390-bb7f-015aadac2bb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 16,
                "y": 70
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "fffd8ca0-c0d0-4cf8-904a-081b1b76b74c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 89,
                "y": 36
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "204073a9-d60e-4624-bf85-7231c77e8fb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 80,
                "y": 36
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "44d87870-88ad-4cc0-a02a-21cb41aadd16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 73,
                "y": 36
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d53f8b65-ce51-42ba-8ec7-a8578ef47ee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 39,
                "y": 19
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7b57f7a6-ecf8-4327-adae-04962bdc8107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 26,
                "y": 19
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e5f81c49-4dd5-4f54-8032-c57f561da2d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 19,
                "y": 19
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "03eb2596-6c51-4718-9822-8d2177b2b108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 11,
                "y": 19
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "98ac4e35-f098-4514-9654-f53f9a00540f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b600bd4a-dac0-4dde-88a1-ff02e82097f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "878b4f73-c422-416a-a211-704d5c7f3c43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "33019cf3-7153-4940-88b1-ee06fa7a6fcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "3eb25a79-3f01-42e3-aab4-c51c4eefc99e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "e6d9e56a-d95e-465d-a323-1e3f327f5819",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ed018902-53d0-4e31-ba8d-3436c9af54ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 34,
                "y": 19
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a6907942-40d3-46bd-a0c6-1370990d67d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1df691f5-8e72-40f7-9d0e-fe2c76c05f33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e07dabc3-bce7-42da-bc27-5e7e22981338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e825895e-b6cc-4805-87b1-be9a269ca41c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7717e20d-3f16-4956-a011-ccc1186aa633",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "5ceac837-962e-454b-a9a1-48c6d842baec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e3867bb8-1e8a-4f86-b653-087189baf3e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e3f9ac68-2701-4d61-abcb-3939c1670f57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e9cd841f-4df5-462b-8039-7269da29d83b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a9ed100a-6ab2-458a-a12d-1d67cdc3c313",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a076df4b-c754-4dd0-9387-60b57547beaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3781f729-edef-4e26-bda3-35a8f4b2471b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 49,
                "y": 19
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "aefa7825-5d14-42b9-94d0-bcf131fc30ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 120,
                "y": 19
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e73e61fd-1743-4872-b1be-de59e456e8fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 57,
                "y": 19
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d9a8121d-7b94-4223-8c21-597777861d7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 15,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 62,
                "y": 36
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "db3e25d5-32c3-49dd-8009-ace304ebf95f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 55,
                "y": 36
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1077fc74-f114-430b-97e6-8b2c371e330b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 51,
                "y": 36
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e0433194-4210-469a-87ff-e167ae60e182",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 41,
                "y": 36
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "64376ecb-4952-403a-88bf-d0527600c384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 34,
                "y": 36
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "863c811a-d260-4529-9bb2-14764384d87c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 27,
                "y": 36
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "89a5890f-2b4e-4039-8090-971be023996d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 20,
                "y": 36
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a427c1a1-dd91-4af3-91fe-f2c8a2ba9820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 13,
                "y": 36
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "45fa54e0-7769-4a8c-9b7d-8f9be2e963d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 8,
                "y": 36
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "6be0bc70-2daf-4d42-a6a9-4152185b9a9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 67,
                "y": 36
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "72808096-462f-4299-a3bd-49a340dfc12c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a309379d-f6da-44d9-962d-b5ecece58856",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 113,
                "y": 19
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d358fa7b-826f-4d76-921f-506efd3bb3cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 106,
                "y": 19
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b919a675-f58c-412e-8936-54db95be861e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 96,
                "y": 19
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "62ba3b35-bfa5-41c6-8fef-2dd9d2784e2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 89,
                "y": 19
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "361080f5-0e92-4b08-8f9c-4bac12dc60e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 82,
                "y": 19
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2daf9eac-4055-4118-9527-5587ff7d36ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 76,
                "y": 19
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "ea0dc6d2-d402-4de0-928f-e1a6e425836f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 70,
                "y": 19
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1702b249-2251-4eb0-9c29-a02bf7c94618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 15,
                "offset": 2,
                "shift": 6,
                "w": 1,
                "x": 67,
                "y": 19
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e2131a98-000f-4fcb-a227-182d9839a4ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 61,
                "y": 19
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7c917cf5-ceef-4598-9744-37a4629b73aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 46,
                "y": 87
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "e721e226-71ad-4d7a-b34e-f399da4fcdaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 15,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 53,
                "y": 87
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "50a73b8e-f23f-4c5b-9e12-cf2068e56cc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "84dec063-3577-4c7a-b3da-dd4c3f45a274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 221
        },
        {
            "id": "ca2b6d9c-b520-4f0d-aca5-6c3126b6a904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 374
        },
        {
            "id": "3bfd1b97-3623-49da-809d-598dd6510b89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 376
        },
        {
            "id": "e3a936d0-ef26-4b4c-83ec-ab084957e1be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7922
        },
        {
            "id": "0b89683a-ec2a-434f-b3ce-3654b78b1682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7924
        },
        {
            "id": "265f9164-4c35-4c5c-ad6c-c0a99107b529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7926
        },
        {
            "id": "8fb4e01a-0b03-4464-b4eb-67e07bb9d296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7928
        },
        {
            "id": "de7fdb4a-2018-43e8-b1df-8ee4869592f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8216
        },
        {
            "id": "876e03b9-697a-4f75-bbf1-ff647d99498e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8220
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 9,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}