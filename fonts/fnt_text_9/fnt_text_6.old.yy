{
    "id": "5b09e272-e0d9-4f6c-9f92-2568593255e5",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_text_6",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Calibri",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7172d872-0421-4005-91f3-939da528e6a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9c6be2cb-f15a-4fd9-9150-91229fa8f338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 79,
                "y": 46
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ab60ce36-5835-4e1c-ad10-d9c54308de6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 72,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f7da3f59-95e4-4d69-ac21-c20eeb241c3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 62,
                "y": 46
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "78e401fe-bee6-4481-9865-0759a42084ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 53,
                "y": 46
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7a73d259-00dd-497d-93e1-fbadb35b37cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 40,
                "y": 46
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "245d5155-578d-42dd-9e8e-01a8dacb553a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 28,
                "y": 46
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "de435324-67ee-4f34-bb3b-e8b5bb285c31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 24,
                "y": 46
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e4b4c0cf-86b5-4a92-b2e8-42b95bcecde1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 18,
                "y": 46
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "145b3da8-73c1-495d-8f80-5b9d3b2b48a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 12,
                "y": 46
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d4c5d085-ac31-48f0-8841-c0b6746f41b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 83,
                "y": 46
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d68c9bd8-07e4-4bfa-8046-a63b29053436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8dcfadf0-cc8b-4320-ad74-68f245120f07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 232,
                "y": 24
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9395840b-6927-4452-a93f-594d5624bc40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 227,
                "y": 24
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0ccd4936-d24c-4fdc-be45-e1b699d91630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 223,
                "y": 24
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a0bae9e4-a34e-435a-82d1-a8b5f1a1764d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 214,
                "y": 24
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "80aee405-4512-467c-bcd1-e1e458e1fb8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 205,
                "y": 24
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "951cb98e-28d6-4ca3-a3c4-dbd66a7977ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 197,
                "y": 24
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "bedbd9af-84f8-4ea6-9cc7-7ca7db51f747",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 188,
                "y": 24
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4a46c45f-7ef1-4331-a84a-af3fb9e7539f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 179,
                "y": 24
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "cfc13458-bb65-44f9-aa4e-c09e644585e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 170,
                "y": 24
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "743f8968-db88-4ea3-a192-a825c9f8bf29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 237,
                "y": 24
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "42c59046-b6cd-4a5b-b5d6-45ff3caccbc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 90,
                "y": 46
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ec3ce58f-4d61-4202-89df-18b7ecee2a5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 99,
                "y": 46
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c2462d5e-54d7-4e8d-bf74-097dfc7fad6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 108,
                "y": 46
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c534b1f3-c793-4d05-b702-5e58c1100dd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 47,
                "y": 68
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e575a67e-70bb-480e-917e-863c7333abfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 43,
                "y": 68
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "906b29b3-da70-462b-a908-9bb393ffeea9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 38,
                "y": 68
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d1d80b6c-dcc9-4bef-b156-ad94444b583e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 68
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "8e4d24e9-adb9-4833-9ee2-3fdd5390fc83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 19,
                "y": 68
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f6e90b17-0419-40d7-a59e-463764626d3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 10,
                "y": 68
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "0274297c-1df0-4439-88ae-ee585151ce15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "0d298302-9043-4706-927e-e16efb82abc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 235,
                "y": 46
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d09e5625-5f59-4fbe-a7b7-d724a2b740ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 223,
                "y": 46
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3b31b8de-a844-4571-a9bf-ce54ddfb42de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 214,
                "y": 46
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ceb78eb2-d59b-441a-a53a-b7bc3d8b1b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 205,
                "y": 46
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "efcb8dfc-e43d-4db8-897d-6b18cdc57621",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 195,
                "y": 46
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6411bb8d-8659-4de9-8229-dff181f409fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 187,
                "y": 46
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ef4f5f78-045c-443d-be7c-e1b8a9a82dfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 179,
                "y": 46
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "03aa0065-887d-4798-a08a-8c3f490f0499",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 169,
                "y": 46
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4a7a1ace-0c29-429b-8651-4907a7c0a30b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 159,
                "y": 46
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "30a3a233-755a-47f8-99ca-b1827cb5b73c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 155,
                "y": 46
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d36083d5-61ec-4965-8e51-ba9b75337b49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 149,
                "y": 46
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5bb30b28-2ba7-4d1a-9873-8ece8b51e691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 139,
                "y": 46
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0c71d744-29f1-4b03-8b2b-1b4626cacdda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 131,
                "y": 46
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b5a597ef-d157-49c8-87a5-f8f6d05a2554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 20,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 117,
                "y": 46
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3e3d4d20-97cb-4b13-923e-d37aeba3726c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 159,
                "y": 24
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d0d7ade7-45ce-4267-aa3f-b3e482e32a46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 148,
                "y": 24
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1be01bf7-3fdc-4ce4-afd8-2d99f990caeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 139,
                "y": 24
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a864b1be-2c97-4903-87e2-17c64d5e9fe2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "edae13bb-1756-474b-89ce-74b8e0afaba9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "7af7cad5-3aac-4072-ad77-14d6eb041044",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "471e09a9-ba3d-4cf2-b963-e7d06e7e8d50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e6d6d4ca-fe2a-450b-b2ec-99d12b36f10a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f3556082-bb65-470e-8322-f25be3b6f605",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "0c22ad92-0c02-4737-a082-bf5f87c7cda0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 20,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "510079d1-9160-4433-a0e2-4a2c584d54d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a7500558-29f8-444d-a2b6-7d91a345ba49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7f198b52-68e5-4c0b-9bec-b37c18c28e53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "e4e3d25c-c07a-490f-bfd1-ebaf12dd038b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f6b8052e-2e06-4b7d-b583-0db7b79481c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3677b80c-c9d6-4220-a5cd-04f647cb11e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f5ff983a-b1ba-4b1e-96f5-48e67ab4d4e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "83baf8c4-6b94-44ef-9bdc-9b2a6a657a66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "342ee5d1-40af-4dca-a48f-a2a9bef78955",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c6006d03-0e72-4e1a-a35a-42ef6ba2a13b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b59586ea-8422-40d4-aa78-cb25f64aac23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ed80d467-6b59-4b92-bdbd-d60b2281a73a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d47b0cd4-c03a-4c15-8d04-4fb8f1e6b99b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "992c718a-6d8b-4699-a890-1945a457585f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "07561254-0ed8-4b18-9c27-14f81ea103c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "64d623ad-cc71-4731-aad9-0732b567be70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "27e7244a-db6a-4118-b685-84fbbc384cd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 44,
                "y": 24
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "08c58ea7-de5c-4e4d-80d2-2958ec7433a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "c069afaf-959a-406c-b6ba-6e3202ca0989",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 128,
                "y": 24
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f80c1d7e-45f9-452b-8379-c41e0232bdcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 119,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "a9339b41-6ab3-4483-a14f-e033d80b9d9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 115,
                "y": 24
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a28b53be-aa97-420f-857a-8a20a1c44a41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 103,
                "y": 24
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "555f5cca-0ea8-4287-addc-34cf599a2546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 94,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ab9eedd7-f161-4641-a36c-93ffb17f6b85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 85,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e0b498af-e175-4431-89d6-e4a0c9f68de2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 76,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a29dbc1b-3598-4fcf-b265-63b94b5615d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 67,
                "y": 24
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7218f9f2-99b6-4e4d-a6bb-6c51691f398b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 60,
                "y": 24
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "06ade4c3-5a43-486e-8ff6-942bcaeed030",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 133,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0cb36cf8-1fb6-4bac-8e66-438137093c30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 53,
                "y": 24
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f9cc7a53-d13d-4382-83f7-2da90655a40c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 35,
                "y": 24
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "70b30c48-1c3d-446e-8ec5-ce3695d42a3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 25,
                "y": 24
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4e3db5ee-f20e-4084-9d75-f1584dd6f31b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 11,
                "y": 24
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "58485f1e-282b-4a96-8a55-b822651bcf29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "11329951-90bf-4e44-8fb9-534bafff8035",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "10feb070-970e-4471-b7ca-b4b70b4da6fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3e0f243c-13af-4f04-9aae-5f8b80a0c511",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f5581fc6-14b5-4efa-a0f0-c4a3e32a4906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 20,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "cf1f3457-3fe5-4139-9735-06e0a1f4ee61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0bde55bc-9d34-470a-95cd-8b77f8089382",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 68
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "7c5b7319-6498-4403-9449-2225ebd0ef8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 65,
                "y": 68
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "c8e267f9-85a8-4d78-9f1b-f42d0bdd5ef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "098beb30-d063-45f0-98cd-92580b7c9f75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "626df6ce-f616-480b-803b-ccd64bc91c91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "eebef24c-42a5-4cf7-a19f-d0832a3fe7bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 221
        },
        {
            "id": "142e1eb2-2246-4db8-8ce8-c5afd91d3e60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 356
        },
        {
            "id": "395f0aa3-61f9-4472-b33a-32660b267093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 374
        },
        {
            "id": "d012a0fe-ca13-423f-a4e0-5d19a18200a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 376
        },
        {
            "id": "d04b12f8-bc13-4c82-858b-eabce0a6ba2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 538
        },
        {
            "id": "29aa38f0-f6ea-4b50-a471-6dc1b709ab21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7922
        },
        {
            "id": "9342b498-c6c2-47fd-946a-4037652b0fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7924
        },
        {
            "id": "0133ce4c-ba72-419b-afdf-bcc3e5633669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7926
        },
        {
            "id": "3ff52a72-ff36-4efe-91c6-c647a4f9844b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7928
        },
        {
            "id": "b96d8d06-6cc4-4f2d-86de-bdf774adf429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8216
        },
        {
            "id": "0ad10b81-7913-4bdd-a13a-871659c4c027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8220
        },
        {
            "id": "b18fe8de-5fdb-4751-a137-6ba1ecffb543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "3fcd8d2b-e309-4ed2-975c-3002dab17683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "50979aab-0afa-4d65-a3f4-a30982ffd4e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "9f6eebdc-ab7d-46e3-b299-b23e198e833d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "076dd9c7-f52e-4a65-a1da-10ddae426e75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "511e0f5e-1eac-4539-b92a-eb947eaf7bca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "933ba425-48f1-4fb7-a59b-9632d877a6fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "36981d2c-b8b5-43f6-88f2-35768aceefc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "411bb2b1-76cc-49ff-8fca-96a9fc3bc180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "64a81575-b840-458e-b928-e8724a5a2e38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "fc67284a-a4dc-48b4-889d-7ad0b4aa2694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "e7fb4a3c-165b-4313-8ef3-9f88894c8814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "65235099-7674-4fd7-8816-a2e03c614252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "e14570db-4f19-449d-949c-bd07048f3f5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "acbb6ae3-c810-4f20-aac0-96809ae2337e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 308
        },
        {
            "id": "f983ecc2-55fa-46b7-b0c6-a5a0c2013a3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 506
        },
        {
            "id": "e2143d9e-e87e-42d3-bc98-3fb7e2a5035a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7840
        },
        {
            "id": "d57ca8db-ac70-42e9-8bcc-cffbec7968d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7842
        },
        {
            "id": "1437e8b4-822f-4ef8-b44e-d2219e32d1d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7844
        },
        {
            "id": "4232a348-1cb3-4e2a-9efe-76aea049193e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7846
        },
        {
            "id": "5c1b3be6-6988-4f76-ae45-87f1f4fd9d1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7848
        },
        {
            "id": "fb966cdc-e4c5-4fdd-a512-bf21d638bf76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7850
        },
        {
            "id": "6cbc4ca9-60f1-4676-974c-630c61f4bd22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7852
        },
        {
            "id": "67b603f6-cf2c-4dd0-b09f-bab1cc80c6fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7854
        },
        {
            "id": "7821f682-27a2-4d91-90a4-c7d8dd9118f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7856
        },
        {
            "id": "56612607-87a9-4621-8f21-131deeac5564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7858
        },
        {
            "id": "79b597dc-3d5a-4029-a1cc-10d39c594980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7860
        },
        {
            "id": "a2584cf3-4692-4c38-ab28-ad5253bbdae6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7862
        },
        {
            "id": "50eecbf6-3d72-4955-b32d-af2ce7780e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "365f05e7-1813-4169-a92e-c4cfdb46b4f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "c7a71357-8ce3-469f-b1d5-e08cef4350b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "84272a55-c186-4627-9cfa-1d6e9655343b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "d9459973-128f-4656-8b65-44c7c14534de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 221
        },
        {
            "id": "4753d926-f029-4d2d-9331-091ae6710da0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 356
        },
        {
            "id": "eb6a5188-27c8-4d9e-b970-d07d62d0807a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "206d10ca-cd34-4086-810b-f509fca3629b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 374
        },
        {
            "id": "8389e04b-b4b8-4ecd-b3fa-af4cefa06cae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 376
        },
        {
            "id": "e54dd3f8-a089-4f7e-a54c-4dc79f1a3c0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 538
        },
        {
            "id": "b883d00e-3c87-4d75-946d-5163a1f90c80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "d85be038-1727-4cbb-b1d1-7ad6a81cd4ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "63c69d6a-ac88-4f8d-b266-f9f3ce44fc18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "95dee8a9-c099-4685-874e-1c91cf056094",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7922
        },
        {
            "id": "6db33740-912b-4559-ae19-5de7208dd763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7924
        },
        {
            "id": "56ebfd4c-c873-4afc-8a84-380c7741be42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7926
        },
        {
            "id": "65956778-1671-4aae-8288-134b8963e1bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7928
        },
        {
            "id": "99004d0e-0ee3-41b5-9528-c188e7095d76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8216
        },
        {
            "id": "ada1b425-b36c-49dd-923c-d151cba447fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "0ec6084b-2bb2-45dd-aceb-07bb3b3c47a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8220
        },
        {
            "id": "e1b28a91-9938-4134-b38c-cad2cae79296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}