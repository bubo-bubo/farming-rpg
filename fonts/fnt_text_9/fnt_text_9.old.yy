{
    "id": "5b09e272-e0d9-4f6c-9f92-2568593255e5",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_text_9",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Calibri",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "cb043cc5-b5c1-4f5d-9002-41a56ee1ddfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "dae584ae-3965-4f2e-8e8b-e62dc3a8916d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 10,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bc87c9ba-d307-4375-86ba-78e8766bd798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 122,
                "y": 26
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "93d50072-56e2-4204-bc89-52917b1bd257",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 116,
                "y": 26
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "fc6b1441-e006-45de-aadf-6d0c9d9fadda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 110,
                "y": 26
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "6cc4672d-0828-4343-9d9e-bf43c8e50952",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 102,
                "y": 26
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c226d396-a625-4228-a267-2e127c4b94b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 95,
                "y": 26
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "eaccd86b-cddf-4c19-bad6-ff0ada4cb486",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 91,
                "y": 26
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "73ac31a4-d181-4344-8ce3-9fd71968f985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 87,
                "y": 26
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6ae15e28-3205-41d7-ae66-55e1cbe917fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 83,
                "y": 26
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9286b32e-1494-4591-9959-0dc8ce9f3e6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 10,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 5,
                "y": 38
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "31ca47b5-d077-4922-960e-a36279725127",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 77,
                "y": 26
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c30fa3b2-e7f2-4f58-a946-e0459b802dd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 67,
                "y": 26
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "61c94d98-25e1-4e27-88ab-c12709b35ec8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 63,
                "y": 26
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2ebd018d-83f5-48c7-a9a0-58336b945a9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 59,
                "y": 26
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "fb68b00c-2cec-4e3c-abbe-11f4f4e9b63e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 54,
                "y": 26
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "040d487a-26dd-44ca-8d6b-3e3972adb5af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 48,
                "y": 26
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "785aa28b-26fa-49e1-944f-51e693bcce76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 10,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 43,
                "y": 26
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "0428ea53-331a-4854-8f7f-b5de5c53ec17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 37,
                "y": 26
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "fd01b744-7f37-4939-a83a-ffa2aca44883",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 31,
                "y": 26
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6ae4e632-5c87-455a-8e5e-67f75f8a9bb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 25,
                "y": 26
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b65d7693-4d7e-438d-8d5c-048e75bcd615",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 71,
                "y": 26
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a32da6fb-ca09-4629-a4fb-473a24924e6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 9,
                "y": 38
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c3e166c2-545d-421f-bb76-31ae51ba4fee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 15,
                "y": 38
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "64a8705a-e3ee-4ed2-8aad-64838db8a08f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 21,
                "y": 38
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "84b52c62-3d4f-4078-bb4d-c08807a003a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 15,
                "y": 50
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ddaa2d31-eb9b-49f9-a457-2c272525cf09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 10,
                "offset": 1,
                "shift": 2,
                "w": 1,
                "x": 12,
                "y": 50
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "905ac994-e764-4dd6-a608-bcc819be0d85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 8,
                "y": 50
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b830eadc-c456-4dc0-a70e-415c03144eeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "cbec77a2-14d3-413d-80b4-dfa36c36914a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 116,
                "y": 38
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ae536ee0-ade0-4778-bf14-fc01932eff18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 110,
                "y": 38
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "944d8c14-9265-4b3c-a392-505885063c13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 105,
                "y": 38
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "640861c3-4d99-41a9-aab7-35b9f156079b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 96,
                "y": 38
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "8be5c8b0-0507-4295-b3b8-18a91981bb25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 89,
                "y": 38
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "48d11bf0-8617-4f8c-9e32-3d8ba8e128e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 10,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 84,
                "y": 38
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "19a3adbb-66dc-4bc7-9b31-841ef4964a3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 78,
                "y": 38
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "0ff61ea7-f8d9-486a-8f96-83fdca52db50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 10,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 72,
                "y": 38
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b652a89f-985a-4338-8854-221e5cbf1d14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 10,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 67,
                "y": 38
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "62d11717-da4d-40c2-9261-a063282fdce0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 10,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 63,
                "y": 38
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "355c10b9-fc57-4b63-8bec-34ced02e826a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 56,
                "y": 38
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8b8d266e-705e-4699-b1bd-0a9b984aa54c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 10,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 50,
                "y": 38
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a7cb427a-20ee-48cb-936c-13f9116857e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 10,
                "offset": 1,
                "shift": 2,
                "w": 1,
                "x": 47,
                "y": 38
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "aad2fabe-0c73-4bdc-9f83-a9a3704703ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 43,
                "y": 38
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6ec755a7-7446-4a38-9bdd-9586272fb07b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 10,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 38,
                "y": 38
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1c0455f2-b6d7-40a0-8f5d-6bd1a19a7559",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 10,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 34,
                "y": 38
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5e9930d4-d43a-4e44-acad-570ab3b26184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 10,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 27,
                "y": 38
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "45d27f86-1856-423f-a8ca-27d9b2f22d78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 10,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 19,
                "y": 26
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "cdde7a84-6b54-421f-ab25-4a15d2e79f2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 12,
                "y": 26
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "681e9bb0-199c-49db-be93-7acb34d4d0d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 10,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 7,
                "y": 26
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "87dde3aa-c1f3-404b-a528-f63ef23b4265",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 2,
                "y": 14
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8a0c4f33-a837-4bc2-a7bb-22e09673b208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 10,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "aaab4a5a-e6e1-4a48-8634-957fec83a7a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "29372430-2b22-49e3-9255-1c59f5ca7b9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c97b321d-c519-43d2-ac57-fa5b21145021",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 10,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a50b8b46-ec40-43c4-a341-531da2ea6518",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "325672d6-9afb-4e85-a3d8-68bed6a72916",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c0dd9c51-6439-4059-825b-93540faa0578",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0f1853c6-d9ae-4f29-8b40-b723d2c54ebe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6e6f42dd-58cc-4e01-9e4c-e6b20d74ec99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "fe8db82f-1e18-4beb-a115-f36b3a4d893b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 10,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b2885976-7d4e-46c6-ac7c-0a51115f0fe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "28d69b83-999a-4b16-b9a7-84659e9ecd2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d1dc7a51-51db-4709-bd5c-34260b9c443b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "02d973b7-176e-4136-a0fe-93a9eb88c65c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "cfaad3d3-f81b-47a2-b25d-47d927ce6b80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "12606087-734c-4d70-9ee8-de906440e632",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e9220ab7-fd39-4c22-ae8f-d9806a60a16c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 10,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "12fa7115-56fa-4802-815d-417b8e272f42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "cc3da8f7-bb1a-4462-9b1f-cfb081bb9204",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a629a0ec-c46d-480d-8d2d-2fe734c49deb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 6,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "45b828be-7b46-46a3-afff-c69c55bb8390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "bc8b1d41-a3f3-46aa-92e5-022b77534a77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 10,
                "y": 14
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "179717eb-4892-4d9b-8644-b7c09a036912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 10,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 70,
                "y": 14
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "feeb5d5d-8f86-4de7-bf6a-21beeace41af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 16,
                "y": 14
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "6d104f67-e4d7-4e6a-bb77-f39156c800b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 122,
                "y": 14
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "178bb4da-c549-4db6-ac1b-c32fc03b02d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 10,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 117,
                "y": 14
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "79c50ed0-de07-4b06-9c38-1d7e7d36c70e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 113,
                "y": 14
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7d6f3e1c-18b0-42fd-a84e-72a7e7493dcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 10,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 106,
                "y": 14
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "cee63fa0-989a-4fda-b456-834502e4db98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 10,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 101,
                "y": 14
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "58b85389-9d40-41a0-835b-262e2435c40a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 95,
                "y": 14
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "418d2593-eb79-4372-9d75-21b4433b3aed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 10,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 90,
                "y": 14
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "94c1ab04-f94d-4a0b-9887-a3f570ca8be3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 84,
                "y": 14
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4cfc7d46-7d88-4ff1-b6bf-c81abb46642e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 10,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 80,
                "y": 14
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4206081b-6420-42b4-a115-cacf0aee6fa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "906788c2-922a-40b6-8f03-9165ab6d5770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 75,
                "y": 14
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2d9dd3bb-dc42-4b7b-8e85-35020ab32c5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 64,
                "y": 14
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d1f38677-ec6b-46dd-9ed3-9d3f56859cbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 58,
                "y": 14
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a0bdd4e1-9241-418e-ada4-69340df075e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 14
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a3016a0c-c862-4821-9b4f-a12644d9d04a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 44,
                "y": 14
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4518f822-92b4-4951-9dcd-dc32a3ac39ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 38,
                "y": 14
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4644b060-debf-4f00-804a-606f4ffd98ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 33,
                "y": 14
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d59d6e97-262a-4934-805a-053f4d452dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 29,
                "y": 14
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "48d65413-a139-43e1-b590-6e0adedc515c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 10,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 25,
                "y": 14
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "271bff5a-1d23-44e3-bc5f-5bbbaaa2510d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 20,
                "y": 14
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "2e704bf4-208b-4947-99a7-f1aa72ac870f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 21,
                "y": 50
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "1122727c-dddf-4713-b1e9-693c90df6d78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 10,
                "offset": 2,
                "shift": 5,
                "w": 1,
                "x": 27,
                "y": 50
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 6,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}