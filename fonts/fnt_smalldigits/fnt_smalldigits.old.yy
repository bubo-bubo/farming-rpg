{
    "id": "476e323a-d3ac-448b-a2dc-b9ac066e393c",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_smalldigits",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Courier New",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b5a1e0dd-0485-4131-b5f4-12d6797818e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "fe85d913-7784-4a06-91b5-01255b4526d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 71,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e4872748-b82b-4992-9b95-9f6a59184b38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 78,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "15696642-2397-47ee-a042-3d046cc8e418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 85,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1f73742e-1df2-46a3-b5e6-adb9f83b9ff4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 93,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f09a7c0f-aae4-4ad9-83e5-e226a743520e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 102,
                "y": 40
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "2b36ee4a-81c5-402c-b0b8-ecb1c831636e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 109,
                "y": 40
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "2764aaa7-bb42-4657-82b3-75a80ec66a67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 118,
                "y": 40
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "04b0af2d-4501-489c-a05b-a76661372c05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "edab18f1-2037-4a15-adcf-d3bf72e4c85b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 9,
                "y": 59
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b9d9f4d5-c809-4465-a52b-475b762da2e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 16,
                "y": 59
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "045d0c49-0439-4c06-b693-433f9e1bd29a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 59
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ac401b9e-51a7-46e3-b778-08a4527bef86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 35,
                "y": 59
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "caed8aad-be72-4ed1-86a7-7d9aae456a88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 44,
                "y": 59
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a78a0621-6de9-4625-9331-fa6c75ab660f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 62,
                "y": 59
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "794b63bc-5ea2-4fda-a96c-47ae5fb7bd5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 69,
                "y": 78
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c42b1385-c95b-494f-b82e-99158b32326c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 71,
                "y": 59
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "64f2ee12-6751-4abd-bc70-8611dbe8ed4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 80,
                "y": 59
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "232b262e-6751-45e3-80be-7a89c9cac7f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 89,
                "y": 59
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c0b24a19-1c0c-4354-81d8-acabaa5285cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 98,
                "y": 59
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "cf24732a-46dc-4034-9928-feabb61431de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 105,
                "y": 59
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "59a3c4d0-3a9c-4d25-ac92-e2b2ffea254f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 115,
                "y": 59
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "96b16df8-da55-44e5-8dd1-f4bda51b1ba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d12e6a20-a2c5-4af9-b8d7-e9f5cd8b5243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 11,
                "y": 78
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "34afb855-357f-4120-a36d-8164652b74ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 22,
                "y": 78
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "54801030-af03-4e25-84b7-da79128f9cc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 32,
                "y": 78
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "597222ad-1ed5-464a-b236-d0bb40d40e49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 41,
                "y": 78
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "503d3184-c37d-4f0c-b419-4c92469fda6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 50,
                "y": 78
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cb41b160-0209-40c2-ac32-9f3a134e4e44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 59,
                "y": 78
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "5bcbc140-df24-40ad-8f11-a68b79f0cc8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 62,
                "y": 40
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "bffa01ec-5053-44c8-9d67-a1160201193d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 53,
                "y": 59
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "9122a9aa-6f07-4482-8314-472324459f58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 53,
                "y": 40
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "dd01a2b1-a6f1-40f1-95a0-691d9f71f8e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 11,
                "y": 21
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "414c4903-5561-4d55-befa-a6e654dbcfd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "12ac88bc-d928-46c3-947d-9c23c22dcb85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "cde25f63-48fa-4357-9c77-d65764979453",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "33e86625-1e34-4cd0-b030-0045efe52e8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6dfc01d5-c729-49b9-ba3f-05cff134e3d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "11e61dce-1ace-4c68-8c42-eec6045f95d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5e092716-2a80-476f-b786-e4ea13b604bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2f13e734-e36d-429e-b81c-015ce22c0722",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "23c095d5-5ba7-4098-9611-9ba29b03d614",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0583495c-6626-44d4-93a2-6b5a58247c97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4d3f0a36-437f-4711-91a4-dfc971b19287",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b4c743e5-a311-450a-999f-9b6f4a533cc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d4f4e382-9237-42ba-9a2c-0f0f55405b5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "cb3e5217-2998-4397-a264-8dd6ee00e309",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 22,
                "y": 21
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e74a1840-f772-42ec-964b-d820d4e1469d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 33,
                "y": 40
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "83bb0ebf-b3cc-42a5-9d55-5416d938e3d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 21
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8ec23484-5684-4ec4-954a-f5e98ab7c924",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 38,
                "y": 21
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b41f3d8d-3643-4a11-b3fd-f5cefe9c1777",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 49,
                "y": 21
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "df2fdf97-0186-4fd3-b664-e5deda511176",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 58,
                "y": 21
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ce14924b-0bfb-40c4-9b41-cc0a3f8eb064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 67,
                "y": 21
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c055cb3d-ccc1-4791-904a-d03c82cb5afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 77,
                "y": 21
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "08324bea-614c-442b-b544-4bf95694150b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 87,
                "y": 21
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c033ac47-1cff-4521-b1de-214bdca13b22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 96,
                "y": 21
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c8d14443-2b81-4055-927c-14b4484b28a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 103,
                "y": 21
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "30044ac0-6362-47d5-a17d-953449b8b8f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 112,
                "y": 21
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3265a687-c7b4-4eed-ae1d-77c60d884300",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "461c27d6-633f-4d0a-b3f7-36518db2408e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 40
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "835d0c90-8b79-42d0-b5b0-c81ca444a298",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 24,
                "y": 40
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1c666d49-8b43-46c2-a604-624f61aba1cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 43,
                "y": 40
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "20cd4f57-e091-49ca-b400-011155d700b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 78,
                "y": 78
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 48,
            "y": 57
        },
        {
            "x": 65,
            "y": 90
        },
        {
            "x": 97,
            "y": 122
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 11,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}