{
    "id": "476e323a-d3ac-448b-a2dc-b9ac066e393c",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_smalldigits",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Courier New",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "9fabdf66-6ce4-4d6d-90ea-0af47e7c0fc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3af55ad4-da47-417d-bd78-5fa7c86f1db1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 106,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ae7fd121-5555-4e87-ae4a-2912cf061494",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 115,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "4fcf00f4-7820-4946-a510-bb8775b882ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "85486fe5-b98d-464a-aceb-33968cbcefbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 59
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "4fbf1002-f1ab-4e1d-8833-ca06367e68a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 59
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "fa2f87d4-b591-4eda-9a59-5c1039f56cfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 59
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9c3acbad-1b96-4f9c-9a35-a5a3269b48ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 38,
                "y": 59
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "3e32fafe-5c08-4d03-a95e-05591e9ec2a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 46,
                "y": 59
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8d737d19-e53e-4ccf-a87e-8898fd971f43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 55,
                "y": 59
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "da926f7e-d811-465b-97c4-0352e4a684c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 64,
                "y": 59
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f7a7eab4-e1a0-4f48-85eb-e0bf79cd8337",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 72,
                "y": 59
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "49db74bc-9783-402f-b22a-2ac380a9b0fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 83,
                "y": 59
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2c977384-4fb4-4abc-994e-c6d73538ab7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 94,
                "y": 59
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c4cf917c-b450-40d4-b852-a2d5a74e00af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 114,
                "y": 59
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "27ff4791-1429-4692-a24f-78a40d649eac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 24,
                "y": 97
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b437c54e-e597-40aa-9c02-6475173db0b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "afeb5ae8-8ece-4ef2-ad89-b67bc6756077",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 12,
                "y": 78
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e3ad61aa-a6b8-4f36-8d0b-d64300de7838",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 23,
                "y": 78
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d9e0be22-98a5-45db-98ec-642bbabc95d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 34,
                "y": 78
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "19dfcdef-37cb-4572-ada4-945d28c88e0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 43,
                "y": 78
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "22f0e79c-4efb-402d-956a-9e2e383a0098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 53,
                "y": 78
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2fec86d0-c94b-4d5d-ae94-38684b0fe59b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 64,
                "y": 78
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "77a3c0f1-d835-4d98-aaa8-8a82a45adeed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 74,
                "y": 78
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "be46ed0a-d236-438e-acae-ec250c9d46f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 85,
                "y": 78
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "611e754f-2966-4e89-a882-9a24fed8e7ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 96,
                "y": 78
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d9cf9d86-a0a7-477c-b5f0-1e805b511094",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 107,
                "y": 78
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "80ea3bf2-855a-4235-85a2-55fad711346b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 97
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8df86fb8-0976-41ad-a795-aa13a7f6524c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 97
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ccbec0c7-8f31-4547-8a6f-bcd18b34aa5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 97,
                "y": 40
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b463fafa-4e26-48aa-999a-8e304ee8bfa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 105,
                "y": 59
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "cceba747-67cd-4c5b-8429-481004cbd360",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 86,
                "y": 40
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "dafac02b-b04d-4e00-ae2b-6d005a14b4cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 33,
                "y": 21
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e9ca07dc-f84d-493f-9716-d136fa40338e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "6f3fb564-115b-429b-a55a-fc7259e3521b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c55a8a7a-1b74-4229-b7f8-f4755c5ee953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "751982cb-5e15-4e35-8e38-8c5db3b0e58c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f5873fcd-fefa-4d64-877f-cf8a8239a0d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "fdb497ae-a567-4ce7-9069-cf30cda7a60b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4dd96c9e-699a-4602-b591-1bf2eca6a5be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4ef568a6-729c-4297-9e87-a52c4d66ce1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e126072f-255d-47a5-8aaf-8566a3a4e6c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "2fd7a340-029a-4aa1-9167-35b6f9aa72ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "8817a840-c6f2-42a6-8b06-0204bf8020b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e32997d6-a84d-4fe6-a178-58b5bdb3c49e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 21
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "73a44888-f16e-4543-a6b1-69aef834fe0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 24,
                "y": 21
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ccf6101e-8f1b-4820-924b-2f2ccb487466",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 44,
                "y": 21
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "4933b552-c7be-4625-bc2c-3a96709401b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 65,
                "y": 40
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2eac7dbb-97e2-4982-85c9-19ed83b7b045",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 52,
                "y": 21
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d6fcdd52-876e-49ec-a7cb-4bb84836c67d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 61,
                "y": 21
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b7470b35-f108-49a7-96a4-29fe81cb365e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 72,
                "y": 21
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e97e7e33-4fc0-4377-a037-f826bd5c6803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 83,
                "y": 21
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "59882433-8cb2-4897-a43c-c22138a82ad8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 92,
                "y": 21
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b093177e-0be4-4baa-ba04-b1c0bd2ee9bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 103,
                "y": 21
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "26987468-dcbe-49ea-bbdb-943c51a86ff1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 113,
                "y": 21
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "712fe6ee-7ef5-4619-a312-bb19bfd8d149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a71f2c0e-b1ca-40bf-8561-7e1adcfc1fd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 11,
                "y": 40
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d33401b3-aa54-4e34-9fd0-09bbf1b46022",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 21,
                "y": 40
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d93b1c8f-eb2d-492e-b04b-818e1dc35c00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 32,
                "y": 40
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "53375fce-b836-48ef-b027-3ba052cdc34e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 43,
                "y": 40
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "44095f48-3921-4a28-a8f4-01a77d1310ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 54,
                "y": 40
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ede3d761-2205-4bb0-927a-713711b60484",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 75,
                "y": 40
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "23a065c8-5cd2-47dc-a3e5-db4a4f85fe41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 34,
                "y": 97
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 48,
            "y": 57
        },
        {
            "x": 65,
            "y": 90
        },
        {
            "x": 97,
            "y": 122
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 11,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}