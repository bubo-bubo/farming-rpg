{
    "id": "688b272e-15d6-47ed-9e50-94f84441dae0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crops_picked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01771421-12ef-4db4-b9da-116e5408d3a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "688b272e-15d6-47ed-9e50-94f84441dae0",
            "compositeImage": {
                "id": "294ba48f-6a6a-4329-b3f6-7d02745c19c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01771421-12ef-4db4-b9da-116e5408d3a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "769b04b1-a909-458e-b938-b59ce8a5f9e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01771421-12ef-4db4-b9da-116e5408d3a0",
                    "LayerId": "aeb5587c-55f8-4933-9651-01647c133700"
                }
            ]
        },
        {
            "id": "45a69e51-8a37-4ac4-b2c0-d45396579d03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "688b272e-15d6-47ed-9e50-94f84441dae0",
            "compositeImage": {
                "id": "6540f94c-917f-4646-b239-bced4d460ed8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45a69e51-8a37-4ac4-b2c0-d45396579d03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a031ee3-0d60-46c0-851c-a23fa0ca16ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45a69e51-8a37-4ac4-b2c0-d45396579d03",
                    "LayerId": "aeb5587c-55f8-4933-9651-01647c133700"
                }
            ]
        },
        {
            "id": "9b5d2999-5376-420d-a7ce-ffda4d8525a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "688b272e-15d6-47ed-9e50-94f84441dae0",
            "compositeImage": {
                "id": "ffadcb2e-59ad-43e2-bed6-b28ede8b11ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b5d2999-5376-420d-a7ce-ffda4d8525a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8941fd0d-83b5-4c14-ab06-7cc8c2b77b0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b5d2999-5376-420d-a7ce-ffda4d8525a1",
                    "LayerId": "aeb5587c-55f8-4933-9651-01647c133700"
                }
            ]
        },
        {
            "id": "386adf94-4667-4f48-bbd3-c1022d536541",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "688b272e-15d6-47ed-9e50-94f84441dae0",
            "compositeImage": {
                "id": "55abf383-7535-4710-af41-0004db0b842b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "386adf94-4667-4f48-bbd3-c1022d536541",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cd44e96-94c7-4e90-8497-8f661b9ef223",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "386adf94-4667-4f48-bbd3-c1022d536541",
                    "LayerId": "aeb5587c-55f8-4933-9651-01647c133700"
                }
            ]
        },
        {
            "id": "cec00fc8-e150-4d98-93fa-e40ff72046b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "688b272e-15d6-47ed-9e50-94f84441dae0",
            "compositeImage": {
                "id": "df1d24c3-8591-4317-9c4d-a47fdbedbfca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cec00fc8-e150-4d98-93fa-e40ff72046b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8602026a-9a70-4a76-b93b-9f6d7dafc6cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cec00fc8-e150-4d98-93fa-e40ff72046b1",
                    "LayerId": "aeb5587c-55f8-4933-9651-01647c133700"
                }
            ]
        },
        {
            "id": "a50f5638-50e3-4a6a-82a6-cc9b10918a47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "688b272e-15d6-47ed-9e50-94f84441dae0",
            "compositeImage": {
                "id": "88559018-9d59-45ea-86f0-2dc6e4c86e6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a50f5638-50e3-4a6a-82a6-cc9b10918a47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c349a6c1-9632-4d45-82ef-8bf9b0d664dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a50f5638-50e3-4a6a-82a6-cc9b10918a47",
                    "LayerId": "aeb5587c-55f8-4933-9651-01647c133700"
                }
            ]
        },
        {
            "id": "8531eced-2285-45c8-977a-aa4c32a2d3e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "688b272e-15d6-47ed-9e50-94f84441dae0",
            "compositeImage": {
                "id": "eb20aa22-b25c-495f-a208-91ceb8e13ac2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8531eced-2285-45c8-977a-aa4c32a2d3e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc0cb039-c9bd-4e31-8175-ae0c77ae5196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8531eced-2285-45c8-977a-aa4c32a2d3e0",
                    "LayerId": "aeb5587c-55f8-4933-9651-01647c133700"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "aeb5587c-55f8-4933-9651-01647c133700",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "688b272e-15d6-47ed-9e50-94f84441dae0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}