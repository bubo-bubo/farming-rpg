{
    "id": "182bd657-eb35-43e2-a605-948e62bf6e46",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_male_bedhead_pink",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1343,
    "bbox_left": 18,
    "bbox_right": 811,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "207d338c-163d-4f67-af36-b11c07be47ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "182bd657-eb35-43e2-a605-948e62bf6e46",
            "compositeImage": {
                "id": "0d605f3c-d2d6-40fb-b77d-736e798ccd6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "207d338c-163d-4f67-af36-b11c07be47ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7b5f65a-3836-4e51-bb8b-c54d2e4a3a21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "207d338c-163d-4f67-af36-b11c07be47ad",
                    "LayerId": "2d78672b-c14f-4946-b173-d266d00c071e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "2d78672b-c14f-4946-b173-d266d00c071e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "182bd657-eb35-43e2-a605-948e62bf6e46",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}