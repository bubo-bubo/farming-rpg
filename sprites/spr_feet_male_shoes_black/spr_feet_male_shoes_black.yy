{
    "id": "19a867f3-fd28-4ebf-8286-4b352b346ca2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_feet_male_shoes_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1341,
    "bbox_left": 19,
    "bbox_right": 810,
    "bbox_top": 54,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2468a11f-51f8-4490-829c-820992bd5c37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19a867f3-fd28-4ebf-8286-4b352b346ca2",
            "compositeImage": {
                "id": "9a935185-adbe-4587-824f-0ffa35b5d536",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2468a11f-51f8-4490-829c-820992bd5c37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "507266df-6e9c-47e9-824c-976478a341c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2468a11f-51f8-4490-829c-820992bd5c37",
                    "LayerId": "fc66128e-8c73-431b-8e54-9abfc3db3dc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "fc66128e-8c73-431b-8e54-9abfc3db3dc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19a867f3-fd28-4ebf-8286-4b352b346ca2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}