{
    "id": "a8b0c2b5-cf4e-4fe3-9370-1e3e5c795b22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_namebox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c01dd0d5-fed0-4d04-b35a-b9937e0bc50e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8b0c2b5-cf4e-4fe3-9370-1e3e5c795b22",
            "compositeImage": {
                "id": "484d6936-6b46-4211-9dc0-0899c097f2a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c01dd0d5-fed0-4d04-b35a-b9937e0bc50e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e29fb76-f180-4549-bc28-7f94172a3636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c01dd0d5-fed0-4d04-b35a-b9937e0bc50e",
                    "LayerId": "81770ef3-f56c-4df4-ab3b-54b6dee54360"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "81770ef3-f56c-4df4-ab3b-54b6dee54360",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8b0c2b5-cf4e-4fe3-9370-1e3e5c795b22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}