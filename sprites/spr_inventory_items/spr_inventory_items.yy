{
    "id": "97e271eb-9922-4cac-ac34-b8fb2c9b16e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_items",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 2,
    "bbox_right": 509,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7787a29-a282-4715-a74c-134e1f7f5740",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97e271eb-9922-4cac-ac34-b8fb2c9b16e9",
            "compositeImage": {
                "id": "99bf3eaf-9842-400a-befe-d2d79ff44b5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7787a29-a282-4715-a74c-134e1f7f5740",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d703b12-9679-45af-8aef-8167cdf3901b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7787a29-a282-4715-a74c-134e1f7f5740",
                    "LayerId": "82b127a5-77f4-456f-a11f-0379ceeb9c59"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 256,
    "layers": [
        {
            "id": "82b127a5-77f4-456f-a11f-0379ceeb9c59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97e271eb-9922-4cac-ac34-b8fb2c9b16e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}