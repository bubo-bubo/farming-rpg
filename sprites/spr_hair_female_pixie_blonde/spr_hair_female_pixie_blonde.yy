{
    "id": "fccad421-bf33-490c-b091-b92a425b5220",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_female_pixie_blonde",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1343,
    "bbox_left": 19,
    "bbox_right": 809,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12525c31-1a77-4612-8442-72f88c7803ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fccad421-bf33-490c-b091-b92a425b5220",
            "compositeImage": {
                "id": "fbe1be38-377b-42cb-a831-dd956bb69012",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12525c31-1a77-4612-8442-72f88c7803ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f34202e5-cc30-4b7f-9233-4bd2c842d181",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12525c31-1a77-4612-8442-72f88c7803ac",
                    "LayerId": "0c2b4f4c-84f2-4217-8911-6d3dc8b748a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "0c2b4f4c-84f2-4217-8911-6d3dc8b748a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fccad421-bf33-490c-b091-b92a425b5220",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}