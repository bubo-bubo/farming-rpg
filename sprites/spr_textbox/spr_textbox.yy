{
    "id": "61499f08-7ee6-4d63-bf16-517f7a57fa4e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_textbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e298a3b-32cf-474d-814e-2ad217fe9bb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61499f08-7ee6-4d63-bf16-517f7a57fa4e",
            "compositeImage": {
                "id": "a08b3169-b052-4946-938a-1454e3b2e905",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e298a3b-32cf-474d-814e-2ad217fe9bb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9f6020a-e51a-4791-8901-9b6e9fc24742",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e298a3b-32cf-474d-814e-2ad217fe9bb6",
                    "LayerId": "cd4ea8b0-10e6-455a-be43-d0b2a39911f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "cd4ea8b0-10e6-455a-be43-d0b2a39911f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61499f08-7ee6-4d63-bf16-517f7a57fa4e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}