{
    "id": "80851966-66e5-400a-b201-56117bcf0bde",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_torso_female_sleeveless_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1336,
    "bbox_left": 23,
    "bbox_right": 807,
    "bbox_top": 34,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a25415c0-ae51-46b8-a654-d503c594cad1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80851966-66e5-400a-b201-56117bcf0bde",
            "compositeImage": {
                "id": "09771d45-1bdf-40d7-8e05-9911422431f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a25415c0-ae51-46b8-a654-d503c594cad1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9c9ddb7-c18e-4708-b38f-289f4fd53b1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a25415c0-ae51-46b8-a654-d503c594cad1",
                    "LayerId": "919df2e7-3b6d-4836-bc66-e691e5eb97cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "919df2e7-3b6d-4836-bc66-e691e5eb97cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "80851966-66e5-400a-b201-56117bcf0bde",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}