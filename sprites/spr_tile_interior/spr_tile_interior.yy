{
    "id": "cb2af0ae-7ca5-41f4-ad3c-9bb022d15ab2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_interior",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 703,
    "bbox_left": 96,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8647a7cd-cc39-4d78-b306-c4685545acc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb2af0ae-7ca5-41f4-ad3c-9bb022d15ab2",
            "compositeImage": {
                "id": "72388b24-ffef-420e-ac9e-bae633216808",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8647a7cd-cc39-4d78-b306-c4685545acc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "450db447-850c-4be3-bacd-cccddb824d39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8647a7cd-cc39-4d78-b306-c4685545acc6",
                    "LayerId": "b44cba8c-04d9-453c-8309-ba35962f9b65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "b44cba8c-04d9-453c-8309-ba35962f9b65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb2af0ae-7ca5-41f4-ad3c-9bb022d15ab2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}