{
    "id": "92fa317f-6358-4048-8fe4-940e29b4f62f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a7af6ac-9672-425e-b529-e7c62920b30e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92fa317f-6358-4048-8fe4-940e29b4f62f",
            "compositeImage": {
                "id": "7dda8173-0132-4462-92d7-27b4ef9f18f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a7af6ac-9672-425e-b529-e7c62920b30e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0f2c50e-63a6-49f9-a05c-77e801a93699",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a7af6ac-9672-425e-b529-e7c62920b30e",
                    "LayerId": "3c0e17a7-b50e-4dd5-b2c2-6c37f937218b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3c0e17a7-b50e-4dd5-b2c2-6c37f937218b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92fa317f-6358-4048-8fe4-940e29b4f62f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}