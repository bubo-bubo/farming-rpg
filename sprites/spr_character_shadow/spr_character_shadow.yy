{
    "id": "ca40efea-ad79-466e-8cc1-b9e8e5323431",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_character_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "773f0e76-053a-4833-9f44-8f383f418c3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca40efea-ad79-466e-8cc1-b9e8e5323431",
            "compositeImage": {
                "id": "1fca0529-9666-42f1-80ef-610ffacab3c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "773f0e76-053a-4833-9f44-8f383f418c3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "090a5c7a-93f4-4392-b62c-9b8a979b2030",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "773f0e76-053a-4833-9f44-8f383f418c3d",
                    "LayerId": "21466e1a-863f-4ee3-9512-e339cc4a5270"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "21466e1a-863f-4ee3-9512-e339cc4a5270",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca40efea-ad79-466e-8cc1-b9e8e5323431",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 13,
    "yorig": 5
}