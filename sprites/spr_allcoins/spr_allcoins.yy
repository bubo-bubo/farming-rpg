{
    "id": "0b0c110f-52bb-4bb2-82b8-a18fcb920dd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_allcoins",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5fff013c-1be2-4d79-92a2-1c31a66fdfc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b0c110f-52bb-4bb2-82b8-a18fcb920dd3",
            "compositeImage": {
                "id": "bcff11e2-92b2-4dc9-8e48-7d3b683648f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fff013c-1be2-4d79-92a2-1c31a66fdfc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaf55568-c4e5-440b-9cba-c2bdc98e7d6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fff013c-1be2-4d79-92a2-1c31a66fdfc2",
                    "LayerId": "05abd9a9-38e1-47e0-91c3-9dad446033bf"
                }
            ]
        },
        {
            "id": "eb8d245c-9b63-49ed-ab3d-8568e47d5d4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b0c110f-52bb-4bb2-82b8-a18fcb920dd3",
            "compositeImage": {
                "id": "2e4b0845-f79d-4ac1-a1fb-070db8690d46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb8d245c-9b63-49ed-ab3d-8568e47d5d4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5abe952f-82a5-400f-b562-ca3de8f1bbda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb8d245c-9b63-49ed-ab3d-8568e47d5d4e",
                    "LayerId": "05abd9a9-38e1-47e0-91c3-9dad446033bf"
                }
            ]
        },
        {
            "id": "53f4187c-b0e0-45f6-ad85-507c70c479bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b0c110f-52bb-4bb2-82b8-a18fcb920dd3",
            "compositeImage": {
                "id": "85593fb3-619a-4969-8468-28be78d74483",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53f4187c-b0e0-45f6-ad85-507c70c479bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8759348-b61b-4109-b9d4-b376e65a3519",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53f4187c-b0e0-45f6-ad85-507c70c479bd",
                    "LayerId": "05abd9a9-38e1-47e0-91c3-9dad446033bf"
                }
            ]
        },
        {
            "id": "237824fe-ac97-4696-b8ad-12f3c91aac13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b0c110f-52bb-4bb2-82b8-a18fcb920dd3",
            "compositeImage": {
                "id": "55793015-039b-434d-8fe0-5cb1284ee594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "237824fe-ac97-4696-b8ad-12f3c91aac13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3a48568-4d83-4e59-944f-0f1716f67835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "237824fe-ac97-4696-b8ad-12f3c91aac13",
                    "LayerId": "05abd9a9-38e1-47e0-91c3-9dad446033bf"
                }
            ]
        },
        {
            "id": "cafd4264-dee0-4cd3-88b9-c45bf90a30fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b0c110f-52bb-4bb2-82b8-a18fcb920dd3",
            "compositeImage": {
                "id": "42d76572-bc50-4660-a082-07b0d032e94f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cafd4264-dee0-4cd3-88b9-c45bf90a30fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "594e24a3-c90c-45b0-abf7-51bca57f9806",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cafd4264-dee0-4cd3-88b9-c45bf90a30fd",
                    "LayerId": "05abd9a9-38e1-47e0-91c3-9dad446033bf"
                }
            ]
        },
        {
            "id": "99c710db-aa16-4ffc-a265-cebc22e87e85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b0c110f-52bb-4bb2-82b8-a18fcb920dd3",
            "compositeImage": {
                "id": "70628013-2374-4bd1-a477-163d0e83f4f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99c710db-aa16-4ffc-a265-cebc22e87e85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "249ad223-748d-41e0-bee1-85e2cf53a01d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99c710db-aa16-4ffc-a265-cebc22e87e85",
                    "LayerId": "05abd9a9-38e1-47e0-91c3-9dad446033bf"
                }
            ]
        },
        {
            "id": "bd248653-ac39-4a2a-b2ed-c806a6556325",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b0c110f-52bb-4bb2-82b8-a18fcb920dd3",
            "compositeImage": {
                "id": "c4a7296a-7882-4833-9d80-bb72c728cfbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd248653-ac39-4a2a-b2ed-c806a6556325",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c1c9079-b47f-4d9e-9704-b1a4d9d5acef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd248653-ac39-4a2a-b2ed-c806a6556325",
                    "LayerId": "05abd9a9-38e1-47e0-91c3-9dad446033bf"
                }
            ]
        },
        {
            "id": "a03b05c7-8a18-4862-b1c4-583b475bc781",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b0c110f-52bb-4bb2-82b8-a18fcb920dd3",
            "compositeImage": {
                "id": "66170b5a-c1d3-4d75-80f8-fbe6dcdb6745",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a03b05c7-8a18-4862-b1c4-583b475bc781",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea6f93cc-ce99-45c1-a2e0-e30f733c97ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a03b05c7-8a18-4862-b1c4-583b475bc781",
                    "LayerId": "05abd9a9-38e1-47e0-91c3-9dad446033bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "05abd9a9-38e1-47e0-91c3-9dad446033bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b0c110f-52bb-4bb2-82b8-a18fcb920dd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 0,
    "yorig": 0
}