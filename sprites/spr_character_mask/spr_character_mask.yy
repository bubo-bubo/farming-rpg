{
    "id": "a8ef778c-e1a3-4369-83cf-e6046e3ab6a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_character_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 19,
    "bbox_right": 43,
    "bbox_top": 54,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f4e448f-c699-441f-a671-f91141544f10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ef778c-e1a3-4369-83cf-e6046e3ab6a0",
            "compositeImage": {
                "id": "f2449ea4-bcc5-40fa-91ea-8908fd609177",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f4e448f-c699-441f-a671-f91141544f10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86a63af4-3769-4f12-a7f7-56910b0b55a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f4e448f-c699-441f-a671-f91141544f10",
                    "LayerId": "3fa54726-5cb8-4ec8-9095-267782e58e90"
                }
            ]
        }
    ],
    "gridX": 64,
    "gridY": 64,
    "height": 64,
    "layers": [
        {
            "id": "3fa54726-5cb8-4ec8-9095-267782e58e90",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8ef778c-e1a3-4369-83cf-e6046e3ab6a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 59
}