{
    "id": "fb975b94-64b3-468e-b865-ade8e5cebb26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_collision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45706884-208f-4e92-8bac-a31aa14db3f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb975b94-64b3-468e-b865-ade8e5cebb26",
            "compositeImage": {
                "id": "0d362f3d-f1cc-4db8-845c-b96c553654fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45706884-208f-4e92-8bac-a31aa14db3f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16e4236e-523f-4971-99c5-1f63a999c3b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45706884-208f-4e92-8bac-a31aa14db3f2",
                    "LayerId": "9c3585ec-1b12-4dea-a6fd-f32030d86089"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9c3585ec-1b12-4dea-a6fd-f32030d86089",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb975b94-64b3-468e-b865-ade8e5cebb26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}