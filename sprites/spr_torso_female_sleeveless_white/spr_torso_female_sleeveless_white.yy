{
    "id": "60f45030-241d-40ce-a643-0fc327624190",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_torso_female_sleeveless_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1336,
    "bbox_left": 23,
    "bbox_right": 807,
    "bbox_top": 34,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60174c15-18b7-48d9-a21a-71a279c5b34c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60f45030-241d-40ce-a643-0fc327624190",
            "compositeImage": {
                "id": "2eaef297-1851-4636-9793-df63a41072ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60174c15-18b7-48d9-a21a-71a279c5b34c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0912815-81a5-44a3-8e68-a71b0808988c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60174c15-18b7-48d9-a21a-71a279c5b34c",
                    "LayerId": "7311e6aa-f68f-4f08-845b-6e44e328c429"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "7311e6aa-f68f-4f08-845b-6e44e328c429",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60f45030-241d-40ce-a643-0fc327624190",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}