{
    "id": "aa1986e8-409f-4511-8902-a66245f3462c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_male_messy_raven",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1343,
    "bbox_left": 18,
    "bbox_right": 812,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0261289d-515a-43ac-b505-eaaae669f3a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa1986e8-409f-4511-8902-a66245f3462c",
            "compositeImage": {
                "id": "bd672178-f017-4eff-94ff-381308a0d174",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0261289d-515a-43ac-b505-eaaae669f3a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84a2e726-636e-46a4-9288-bd8e6f1c03c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0261289d-515a-43ac-b505-eaaae669f3a5",
                    "LayerId": "8e901ca9-c3ed-4bc2-96a5-de91efe4a004"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "8e901ca9-c3ed-4bc2-96a5-de91efe4a004",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa1986e8-409f-4511-8902-a66245f3462c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}