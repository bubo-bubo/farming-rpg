{
    "id": "b3ec13ec-9558-4c01-96b1-9de7e5bb0725",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 1,
    "bbox_right": 123,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f22449c1-f49c-48cc-b5a0-a4a6f4ec1b5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ec13ec-9558-4c01-96b1-9de7e5bb0725",
            "compositeImage": {
                "id": "e8d2055d-31e9-417f-a6ed-359f180d68d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f22449c1-f49c-48cc-b5a0-a4a6f4ec1b5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "234f134e-a092-4592-a3d0-5c61a62a5cee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f22449c1-f49c-48cc-b5a0-a4a6f4ec1b5e",
                    "LayerId": "d61a18c4-7db8-4dd9-a253-223810ed6e99"
                }
            ]
        },
        {
            "id": "61f32b63-d2ef-40d2-83fa-ca5e50ed2bc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ec13ec-9558-4c01-96b1-9de7e5bb0725",
            "compositeImage": {
                "id": "b29b364b-93cf-4a5f-bb90-e8a7b9eb42a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61f32b63-d2ef-40d2-83fa-ca5e50ed2bc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b973ce72-1836-4410-9eb7-b71e630117e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61f32b63-d2ef-40d2-83fa-ca5e50ed2bc8",
                    "LayerId": "d61a18c4-7db8-4dd9-a253-223810ed6e99"
                }
            ]
        },
        {
            "id": "a54a6b03-95ee-4738-83cf-e5e3abf5330e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ec13ec-9558-4c01-96b1-9de7e5bb0725",
            "compositeImage": {
                "id": "96e5b2a2-1f44-459d-8169-cd8ea4accabf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a54a6b03-95ee-4738-83cf-e5e3abf5330e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d0f66b3-5a9a-466e-9720-d42d285c40df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a54a6b03-95ee-4738-83cf-e5e3abf5330e",
                    "LayerId": "d61a18c4-7db8-4dd9-a253-223810ed6e99"
                }
            ]
        },
        {
            "id": "43f9f75d-d55c-4cd3-957b-4f273537676c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ec13ec-9558-4c01-96b1-9de7e5bb0725",
            "compositeImage": {
                "id": "e948dc27-2e45-4cd6-9ebe-c32342392c38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43f9f75d-d55c-4cd3-957b-4f273537676c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "633cae39-a131-468a-b0be-d2d1c64a5f73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43f9f75d-d55c-4cd3-957b-4f273537676c",
                    "LayerId": "d61a18c4-7db8-4dd9-a253-223810ed6e99"
                }
            ]
        },
        {
            "id": "04a81113-8b8d-4dc3-9906-847dc0bcd5a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ec13ec-9558-4c01-96b1-9de7e5bb0725",
            "compositeImage": {
                "id": "2d44a12d-fb32-4580-b3c8-91e68671b860",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04a81113-8b8d-4dc3-9906-847dc0bcd5a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee72be9f-4c78-46c4-aa63-7c3713234e4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04a81113-8b8d-4dc3-9906-847dc0bcd5a1",
                    "LayerId": "d61a18c4-7db8-4dd9-a253-223810ed6e99"
                }
            ]
        },
        {
            "id": "4bfd45af-a210-4d49-b8f7-e18fab097a1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ec13ec-9558-4c01-96b1-9de7e5bb0725",
            "compositeImage": {
                "id": "d3ac1aba-1a7b-4066-aa73-54268e664f57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bfd45af-a210-4d49-b8f7-e18fab097a1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eae51c7e-e8e0-4b7a-bbc6-9e77d909d008",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bfd45af-a210-4d49-b8f7-e18fab097a1e",
                    "LayerId": "d61a18c4-7db8-4dd9-a253-223810ed6e99"
                }
            ]
        },
        {
            "id": "a617e18f-6b1f-4acd-83b7-03be3bb0cb2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ec13ec-9558-4c01-96b1-9de7e5bb0725",
            "compositeImage": {
                "id": "b2c4ce01-bc57-4b61-bd00-b8637d6ee175",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a617e18f-6b1f-4acd-83b7-03be3bb0cb2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09bd29d0-60c1-4dd3-9695-8afa2ccd0040",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a617e18f-6b1f-4acd-83b7-03be3bb0cb2e",
                    "LayerId": "d61a18c4-7db8-4dd9-a253-223810ed6e99"
                }
            ]
        },
        {
            "id": "0676fa32-573a-4fc4-bbda-f6ac3d648509",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ec13ec-9558-4c01-96b1-9de7e5bb0725",
            "compositeImage": {
                "id": "986b2d10-4b18-4295-bfbc-3df5b504d0db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0676fa32-573a-4fc4-bbda-f6ac3d648509",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d356626b-dac5-49d7-b9ba-835a94833694",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0676fa32-573a-4fc4-bbda-f6ac3d648509",
                    "LayerId": "d61a18c4-7db8-4dd9-a253-223810ed6e99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d61a18c4-7db8-4dd9-a253-223810ed6e99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3ec13ec-9558-4c01-96b1-9de7e5bb0725",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}