{
    "id": "b6d499b7-f9af-4d8c-ac3b-8a5cb46891a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_legs_female_pants_teal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1341,
    "bbox_left": 24,
    "bbox_right": 807,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "668a4494-bef4-4506-9ff4-d8ee4b61a4f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6d499b7-f9af-4d8c-ac3b-8a5cb46891a1",
            "compositeImage": {
                "id": "f794ba27-7bc9-43ff-996c-bf3da11d7159",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "668a4494-bef4-4506-9ff4-d8ee4b61a4f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "841bc97d-7c4b-4084-94c0-4bd35bee9076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "668a4494-bef4-4506-9ff4-d8ee4b61a4f8",
                    "LayerId": "26475d1b-d6ce-41db-89e3-9f2bba1b83da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "26475d1b-d6ce-41db-89e3-9f2bba1b83da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6d499b7-f9af-4d8c-ac3b-8a5cb46891a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}