{
    "id": "43f35106-210d-4c01-b123-84a81d177a4a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crops",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 446,
    "bbox_left": 3,
    "bbox_right": 159,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46a994db-5007-49bf-bf0a-1dbfe019a183",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43f35106-210d-4c01-b123-84a81d177a4a",
            "compositeImage": {
                "id": "a33b7d2a-3409-4763-95bd-7c4aa5205a54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46a994db-5007-49bf-bf0a-1dbfe019a183",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35d43742-5934-4f26-8c3e-22c0f944b833",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46a994db-5007-49bf-bf0a-1dbfe019a183",
                    "LayerId": "3a46aab1-6599-4907-b814-a2f0d9eb5801"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 448,
    "layers": [
        {
            "id": "3a46aab1-6599-4907-b814-a2f0d9eb5801",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43f35106-210d-4c01-b123-84a81d177a4a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}