{
    "id": "6b9e7b0f-f548-4e37-bf50-e275b0c746be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_legs_female_pants_magenta",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1341,
    "bbox_left": 24,
    "bbox_right": 807,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22c3e7a3-4f65-47c5-a53e-8b770327bc03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b9e7b0f-f548-4e37-bf50-e275b0c746be",
            "compositeImage": {
                "id": "9870b195-11d7-423f-ac20-7796378b8ec7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22c3e7a3-4f65-47c5-a53e-8b770327bc03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c8493e1-4133-4b69-9997-98657fda5597",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22c3e7a3-4f65-47c5-a53e-8b770327bc03",
                    "LayerId": "64b84073-4cfe-4f31-8e7a-fca5c128bbf2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "64b84073-4cfe-4f31-8e7a-fca5c128bbf2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b9e7b0f-f548-4e37-bf50-e275b0c746be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}