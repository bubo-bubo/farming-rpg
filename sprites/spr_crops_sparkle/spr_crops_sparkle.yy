{
    "id": "549537f3-642e-4bc4-9774-1fd10594f14c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crops_sparkle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "523adf1c-4677-4f30-8fd2-40c79747c180",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "549537f3-642e-4bc4-9774-1fd10594f14c",
            "compositeImage": {
                "id": "873b5021-24b0-484c-bd0a-a57c5dacb429",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "523adf1c-4677-4f30-8fd2-40c79747c180",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffaef242-72f3-4b20-9b88-e93dc419806e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "523adf1c-4677-4f30-8fd2-40c79747c180",
                    "LayerId": "dea5fa41-e1df-4162-bbe9-ea01059db892"
                }
            ]
        },
        {
            "id": "e5b3170b-5278-40fb-ba41-09510f8863a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "549537f3-642e-4bc4-9774-1fd10594f14c",
            "compositeImage": {
                "id": "1031af84-4358-460e-bb52-a3e5708ee845",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5b3170b-5278-40fb-ba41-09510f8863a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b69bee1a-515c-4f10-9f04-d1ecdc05597b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5b3170b-5278-40fb-ba41-09510f8863a5",
                    "LayerId": "dea5fa41-e1df-4162-bbe9-ea01059db892"
                }
            ]
        },
        {
            "id": "670495fb-6032-4152-962a-804798d2a6f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "549537f3-642e-4bc4-9774-1fd10594f14c",
            "compositeImage": {
                "id": "abf05aac-b4ee-4c16-ae2b-eb788181c2bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "670495fb-6032-4152-962a-804798d2a6f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6912c2a-72a8-43c5-9261-f4bd4d3a2dd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "670495fb-6032-4152-962a-804798d2a6f6",
                    "LayerId": "dea5fa41-e1df-4162-bbe9-ea01059db892"
                }
            ]
        },
        {
            "id": "21383a6b-3dd8-487e-901b-41e85057f954",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "549537f3-642e-4bc4-9774-1fd10594f14c",
            "compositeImage": {
                "id": "25ea902d-d4ca-4ddb-9a3d-2a97dc5851c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21383a6b-3dd8-487e-901b-41e85057f954",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a60b895-e312-444b-9897-323d85755661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21383a6b-3dd8-487e-901b-41e85057f954",
                    "LayerId": "dea5fa41-e1df-4162-bbe9-ea01059db892"
                }
            ]
        },
        {
            "id": "4a5ae167-24aa-4322-9c27-09d902185d78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "549537f3-642e-4bc4-9774-1fd10594f14c",
            "compositeImage": {
                "id": "8abb9005-5fa8-475b-9017-fc79fc410b01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a5ae167-24aa-4322-9c27-09d902185d78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "008c9295-834e-44ac-be33-200c2f9d9d30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a5ae167-24aa-4322-9c27-09d902185d78",
                    "LayerId": "dea5fa41-e1df-4162-bbe9-ea01059db892"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "dea5fa41-e1df-4162-bbe9-ea01059db892",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "549537f3-642e-4bc4-9774-1fd10594f14c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}