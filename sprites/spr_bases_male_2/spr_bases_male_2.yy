{
    "id": "504f6dc8-aa10-43fa-a834-9c4458080fed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bases_male_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 16,
    "bbox_right": 818,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40b8b319-0a0f-417e-b79c-a10bd75ac33e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "504f6dc8-aa10-43fa-a834-9c4458080fed",
            "compositeImage": {
                "id": "cf4d9fc0-82c7-4513-9e2a-3cf7d48d0d46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40b8b319-0a0f-417e-b79c-a10bd75ac33e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1662dad-1965-4519-a816-5c9b9aca564c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40b8b319-0a0f-417e-b79c-a10bd75ac33e",
                    "LayerId": "a719e44e-512d-48d9-afbf-dea60b08c9a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "a719e44e-512d-48d9-afbf-dea60b08c9a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "504f6dc8-aa10-43fa-a834-9c4458080fed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}