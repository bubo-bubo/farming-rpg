{
    "id": "65847a36-8b09-4f7c-98b7-db701553da4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_torso_male_shirt_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1336,
    "bbox_left": 17,
    "bbox_right": 558,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e828a96a-2f54-44d8-996b-6de315e186f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65847a36-8b09-4f7c-98b7-db701553da4d",
            "compositeImage": {
                "id": "093859c2-3a46-4c9a-a4e5-7ebe5d96bc5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e828a96a-2f54-44d8-996b-6de315e186f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56a8712d-77a2-488c-9d12-26a5dc565df8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e828a96a-2f54-44d8-996b-6de315e186f4",
                    "LayerId": "27dcc520-3be5-4ef7-8094-7803650f3ac3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "27dcc520-3be5-4ef7-8094-7803650f3ac3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65847a36-8b09-4f7c-98b7-db701553da4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}