{
    "id": "40e8425f-dce1-465f-9d2e-458622db992b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_feet_female_boots_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 23,
    "bbox_right": 809,
    "bbox_top": 54,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5880d92-910c-4470-8ffa-b3154e9926e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40e8425f-dce1-465f-9d2e-458622db992b",
            "compositeImage": {
                "id": "eba70eb7-5877-44dc-bd3b-b488f94b9231",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5880d92-910c-4470-8ffa-b3154e9926e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61286e0d-b2cf-4ef8-a910-c650d7f6a3a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5880d92-910c-4470-8ffa-b3154e9926e8",
                    "LayerId": "8d0f85a7-38aa-45e0-97ee-200c10520731"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "8d0f85a7-38aa-45e0-97ee-200c10520731",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40e8425f-dce1-465f-9d2e-458622db992b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}