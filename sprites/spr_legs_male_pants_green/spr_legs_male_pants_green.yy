{
    "id": "051afd3e-c212-4c4a-bd88-52108778080e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_legs_male_pants_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1340,
    "bbox_left": 22,
    "bbox_right": 552,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ac80dca-d8a2-44e3-a76e-f7f86a43e815",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "051afd3e-c212-4c4a-bd88-52108778080e",
            "compositeImage": {
                "id": "1c5ef181-901a-4bae-bd3a-7c8a98197e6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ac80dca-d8a2-44e3-a76e-f7f86a43e815",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0efc0ff1-002f-47a9-bcde-9ce898d50029",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ac80dca-d8a2-44e3-a76e-f7f86a43e815",
                    "LayerId": "2b0e0198-dc99-4179-9d92-32424c5b6ab2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "2b0e0198-dc99-4179-9d92-32424c5b6ab2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "051afd3e-c212-4c4a-bd88-52108778080e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}