{
    "id": "d8a76664-198d-4c42-a810-81b946ae9677",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_frame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca3bfa31-4e56-40d4-b202-7e3195eab2ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a76664-198d-4c42-a810-81b946ae9677",
            "compositeImage": {
                "id": "a6ad18d7-ad51-49b9-ab55-eabc736f2f08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca3bfa31-4e56-40d4-b202-7e3195eab2ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d20d0a05-11a4-4656-b39e-a108b56a9e50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca3bfa31-4e56-40d4-b202-7e3195eab2ce",
                    "LayerId": "0e3f189a-6d3a-4a09-8265-70052367bca5"
                }
            ]
        },
        {
            "id": "c6ad88fa-3a6c-4547-aa2a-3859eeeb8989",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a76664-198d-4c42-a810-81b946ae9677",
            "compositeImage": {
                "id": "ccf37f2d-65cc-4ad1-9764-e26da87e40ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6ad88fa-3a6c-4547-aa2a-3859eeeb8989",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65c84de8-ea12-4949-b8a0-9af2abb3081b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6ad88fa-3a6c-4547-aa2a-3859eeeb8989",
                    "LayerId": "0e3f189a-6d3a-4a09-8265-70052367bca5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0e3f189a-6d3a-4a09-8265-70052367bca5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8a76664-198d-4c42-a810-81b946ae9677",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}