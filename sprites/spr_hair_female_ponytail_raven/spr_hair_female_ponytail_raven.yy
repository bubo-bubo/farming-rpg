{
    "id": "106a8197-84d0-4b1e-82fe-135ac715a9c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_female_ponytail_raven",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1343,
    "bbox_left": 18,
    "bbox_right": 812,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d574b17-a665-41bf-9539-296bb0e2d612",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "106a8197-84d0-4b1e-82fe-135ac715a9c5",
            "compositeImage": {
                "id": "c0d5a5a0-e669-46f1-91ca-436a7ca06a38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d574b17-a665-41bf-9539-296bb0e2d612",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c37397bb-8f66-4149-a856-a50505dea125",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d574b17-a665-41bf-9539-296bb0e2d612",
                    "LayerId": "e551e111-4716-4615-8b40-95db7b53e19d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "e551e111-4716-4615-8b40-95db7b53e19d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "106a8197-84d0-4b1e-82fe-135ac715a9c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}