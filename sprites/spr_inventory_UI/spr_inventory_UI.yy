{
    "id": "17f7a4d6-ff95-4ea3-9fd6-04fc39172c69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_UI",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81a7107c-183a-4c9f-b7af-2b9ffc42e6f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17f7a4d6-ff95-4ea3-9fd6-04fc39172c69",
            "compositeImage": {
                "id": "ec3c8a98-5247-440c-9b9d-a19875e465ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81a7107c-183a-4c9f-b7af-2b9ffc42e6f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5f77cf7-cd4c-4bd6-accb-4c698fa415eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81a7107c-183a-4c9f-b7af-2b9ffc42e6f2",
                    "LayerId": "6bc90b69-71c5-4ce3-8abc-bce11461ff50"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 192,
    "layers": [
        {
            "id": "6bc90b69-71c5-4ce3-8abc-bce11461ff50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17f7a4d6-ff95-4ea3-9fd6-04fc39172c69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}