{
    "id": "e2feb27c-1cf1-41f0-8557-28878551a1c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bases_female_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 18,
    "bbox_right": 818,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbba0e3b-fdf2-4eda-878c-b5ec9707fd35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2feb27c-1cf1-41f0-8557-28878551a1c0",
            "compositeImage": {
                "id": "950841dd-c800-4a3d-86f0-d0fcb5f154b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbba0e3b-fdf2-4eda-878c-b5ec9707fd35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7560414e-1d87-4a7c-b6d2-3b9c8e94d56c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbba0e3b-fdf2-4eda-878c-b5ec9707fd35",
                    "LayerId": "7bd30738-ad42-46e2-af28-6dc6cd3f77ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "7bd30738-ad42-46e2-af28-6dc6cd3f77ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2feb27c-1cf1-41f0-8557-28878551a1c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}