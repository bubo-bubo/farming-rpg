{
    "id": "41cb6ee7-5f48-4d50-b8d9-47c9d130fbce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bases_female_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 18,
    "bbox_right": 818,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66f448cf-ff16-4e6e-b8af-e94bdf67b0a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41cb6ee7-5f48-4d50-b8d9-47c9d130fbce",
            "compositeImage": {
                "id": "a3ee11f8-6430-4102-9996-a8a9282f81b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66f448cf-ff16-4e6e-b8af-e94bdf67b0a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a9cd352-521e-4ac2-b66e-c2e3cb8041ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66f448cf-ff16-4e6e-b8af-e94bdf67b0a2",
                    "LayerId": "615a0d87-6b96-4958-a26f-45e479693bed"
                }
            ]
        }
    ],
    "gridX": 64,
    "gridY": 64,
    "height": 1344,
    "layers": [
        {
            "id": "615a0d87-6b96-4958-a26f-45e479693bed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41cb6ee7-5f48-4d50-b8d9-47c9d130fbce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}