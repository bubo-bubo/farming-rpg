{
    "id": "7f798507-717e-44c8-aa59-30e6f70423c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_feet_female_longboots_brown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 23,
    "bbox_right": 809,
    "bbox_top": 50,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b4e9031-2953-4cdd-81eb-958a16fdc782",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f798507-717e-44c8-aa59-30e6f70423c5",
            "compositeImage": {
                "id": "d5f5d560-9035-4b90-9bd2-1817dac21f92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b4e9031-2953-4cdd-81eb-958a16fdc782",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd6b4852-7575-40dd-98d6-5b948b853d25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b4e9031-2953-4cdd-81eb-958a16fdc782",
                    "LayerId": "f9976805-6e17-46cf-8df1-1795cf24ce48"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "f9976805-6e17-46cf-8df1-1795cf24ce48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f798507-717e-44c8-aa59-30e6f70423c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}