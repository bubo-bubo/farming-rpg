{
    "id": "1b7afdb3-9925-4b25-beec-0df727eec333",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bases_male_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 16,
    "bbox_right": 818,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9dd1965-db03-462b-8820-6108696ca8ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b7afdb3-9925-4b25-beec-0df727eec333",
            "compositeImage": {
                "id": "5cb5fa9c-dc3b-45e1-a27f-478fe7a3e30f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9dd1965-db03-462b-8820-6108696ca8ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cae00cf-c0f8-4b08-abca-8d6382fd9b90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9dd1965-db03-462b-8820-6108696ca8ad",
                    "LayerId": "2e5d9a56-f4e3-4d73-9618-d9a77d6b5c29"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "2e5d9a56-f4e3-4d73-9618-d9a77d6b5c29",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b7afdb3-9925-4b25-beec-0df727eec333",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}