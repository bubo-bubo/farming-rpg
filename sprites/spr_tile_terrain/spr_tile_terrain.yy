{
    "id": "1a18572d-c19d-40d3-b445-c18b2e716f82",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_terrain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28b9d7d1-cde3-4a34-bd17-9efb055ab7b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a18572d-c19d-40d3-b445-c18b2e716f82",
            "compositeImage": {
                "id": "8f7373c1-6c3b-49a8-92b0-db8862fea2c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28b9d7d1-cde3-4a34-bd17-9efb055ab7b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b299519-9eef-48f4-9858-f1203398471c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28b9d7d1-cde3-4a34-bd17-9efb055ab7b7",
                    "LayerId": "97408b99-3194-462c-8f05-65f6cb5bfff7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "97408b99-3194-462c-8f05-65f6cb5bfff7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a18572d-c19d-40d3-b445-c18b2e716f82",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": -226,
    "yorig": 284
}