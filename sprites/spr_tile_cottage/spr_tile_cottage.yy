{
    "id": "cbed9b0f-6a41-4069-a6b4-987dc6ac5f2e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_cottage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "289c5479-d191-45cb-a7e7-976270564363",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbed9b0f-6a41-4069-a6b4-987dc6ac5f2e",
            "compositeImage": {
                "id": "1600f71d-2166-421e-9339-cefdd73463f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "289c5479-d191-45cb-a7e7-976270564363",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b312f4f-c12e-4149-9a41-2c981298ac27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "289c5479-d191-45cb-a7e7-976270564363",
                    "LayerId": "49501ef9-532d-4755-8a93-a57ee58ed211"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "49501ef9-532d-4755-8a93-a57ee58ed211",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbed9b0f-6a41-4069-a6b4-987dc6ac5f2e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}