///@description instance_creat_crop
///@arg grid_x
///@arg grid_y
///@arg crop_type
///@arg days_old


var i_grid = crops.ds_crops_instances;
cs = crops.cellSize
var xx = argument0 * cs;
var yy = argument1 * cs;

//create the instance
var inst = instance_create_layer(cs/2+xx, cs/2+yy, "Instances",obj_crop);

//store the instance ID in ds_cops_instances
i_grid[# argument0, argument1] = inst;

show_debug_message("Respawned a" + ds_crops_types[# 2, argument2]);

//give the crop its characteristics
with(inst){
	cropType = argument2;
	growthStageDuration = crops.ds_crops_types[# 0,cropType];
	daysOld = argument3;
}
return inst;